\svnInfo $Id: selfcal.tex 12951 2014-03-08 18:40:31Z pizzo $

\section[Automated Self-Calibration]{Automated Self-Calibration}
\label{selfcal}

%\footnote{This section is maintained by Nicolas Vilchez ({\tt vilchez[at]astron[dot]nl}) and George Heald ({\tt heald[at]astron[dot]nl}).}

% rewritten by George Heald, 28 March 2014

The standard technique of self-calibration (iterative updates of both sky model and direction-independent instrumental gains) is available in the form of a python script that automatically utilizes {\tt BBS}, {\tt DPPP}, {\tt awimager}, and {\tt PyBDSM} in a pre-defined loop algorithm. Further development of the python script into an operational form that can be run by the Radio Observatory is underway. It is assumed that before attempting to perform self-calibration, the LOFAR data set has been flux calibrated (e.g. by the Radio Observatory using the Standard Imaging Pipeline). 

This section provides a brief guide to using {\tt selfcal.py} on a LOFAR dataset, and details the internal procedures. The script is written to be completed without user interaction and in a consistent manner; therefore, there are very few options that can be set at runtime. Additional details are available at:\\ 

\url{http://www.lofar.org/operations/doku.php?id=public:user\_software:documentation:selfcal}




%-----------------------------------------------------------
\subsection{Overview}
\label{selfcal:overview}

The goal of the self-calibration (selfcal) process is to improve the quality of the final image, through an iterative process. At each iteration, a calibration step is performed\footnote{In the current version, the automatic script performs phase calibration only.} to update the instrumental gain table. An image of the full field of view is then created at a particular angular resolution, and the source finder is used to update the sky model that is then used for the next calibration round. In early iterations, the data are imaged at low angular resolution. In each iteration, the image resolution is improved such that successive models have an increasing level of detail, and the calibration of the remote stations progressively improves. The iterative process is continued until the image resolution obtained is the best resolution possible with the data (taking into account the observing frequency and longest available baseline). In figure~\ref{fig:global-selfcal}, the highlighted segments of the pipeline correspond to the ones used within the selfcal process (including the loop itself). 


\begin{figure}[ht!]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{selfcal-pipeline.png}
    \caption[selfcal-diagram.]{Illustration of the selfcal process in the Standard Imaging Pipeline.}
    \label{fig:global-selfcal}
  \end{center}
\end{figure}


%-----------------------------------------------------------
\subsection{Availability}
\label{selfcal:imagingpipeline}

Work is in progress to implement the selfcal procedure within the Standard Imaging Pipeline. At present, the stand-alone selfcal version is available on CEP2, CEP3 and on Flits (ASTRON Astronomy Group cluster).
On CEP2 and CEP3, the stand-alone {\tt selfcal.py} is included in the daily build. On Flits, the most recent version of the stand-alone {\tt selfcal.py} is maintained by N.~Vilchez. 

To utilize the stand-alone {\tt selfcal.py} at your own facility, the LOFAR trunk must be installed ({\tt selfcal.py} is included in the LOFAR trunk available through {\tt svn}, not in releases yet). To obtain the LOFAR trunk software, LOFAR {\tt svn} access is required. Installation details are available at: \\

\url{http://www.lofar.org/operations/doku.php?id=public:user\_software:documentation:lofar-cmake} 

%----------------------------------------------------------------------------------------------------------------------------------------------------------------
\subsection{Selfcal: the stand-alone version}
\label{selfcal:standalone}


\subsubsection{Usage}
On CEP2 and CEP3 the latest version of {\tt selfcal.py} is installed, but you need to setup your environment by typing\footnote{On Flits, {\tt selfcal.py} is directly installed, no use command is required.}:\\\,\\
\texttt{> module load lofar}\\\,\\
in order to make use of the daily build from the LOFAR trunk. Alternatively, you may add {\tt lofar} to the {\tt .mypackages} file in your home directory.

Note that the \texttt{LofIm} package conflicts with the \texttt{LUS} package (especially for \texttt{awimager}). To use  {\tt selfcal.py}, you must disable \texttt{LUS} from your environment (i.e. erase \texttt{LUS} from the {\tt .mypackages} file in your {\tt \$HOME} directory).

The self-calibration procedure is initiated by typing \texttt{selfcal.py} at the command prompt. To access usage information, type \texttt{selfcal.py -h} and the following will appear:\\\,\\
\texttt{Usage: selfcal.py PATH/parsetFile \\
OR \\
Usage: selfcal.py --obsDir= --outputDir= [Options: --skyModel=\\ --nbCycle= --outerfovclean= --VLSSuse= --annulusRadius= --startResolution=\\ --endResolution= --resolutionVector= --mask= --UVmin= --startingFactor= --FOV=\\ --nofPixelPerBeam= --robust= --maskDilation=]} \\

and a detailed list of parameters.

Only ``obsDir'' and ``outputDir'' (which specify the input and the output directories) are required for a successful {\tt selfcal.py} run. All other parameters have sensible default values as determined through commissioning.

The {\tt selfcal.py} parameters are:
\begin{itemize}

\item \textbf{Required Parameters:} 

	\begin{enumerate}

	\item \texttt{obsDir} (string): This is the observation directory which contains data (or frequency merged data generated by {\tt mergeSB.py}; see \S\,\ref{selfcal:dataformat}) \\
	Data must contain the same number of subbands, have the same frequency range and the same pointing centre. Checks are done internally. The {\tt obsDir} must contain ONLY data, no additional files. Due to this, if the {\tt outputDir} is in the {\tt obsDir} tree, the code will crash.\\
	The full path must be provided.
               
	\item \texttt{outputDir} (string): This is the output directory where the selfcal process will be executed. If it doesn't exist, it will be created automatically. \\
	The full path must be provided.

	\end{enumerate}

\item \textbf{Optional Parameters:}

	\begin{enumerate}
		\item \texttt{skyModel} (string, default:`none'): This option allows the user to provide a skymodel instead of using the {\tt VLSSuse} option. The skymodel must be BBS compatible. The full path of the file must be provided.\\
NB: If the {\tt skyModel} option is used, the {\tt VLSSuse} parameter must be not set. 
			\begin{enumerate}
               		\item Case 1: {\tt VLSSuse=yes}: GSM sky model (VLSS) is used for initial calibration 
               		\item Case 2: {\tt VLSSuse=no}:  low resolution (90 arcsec) skymodel is used for initial calibration (generated with first-pass image from the input data) 
               		\item Case 3: {\tt skyModel} is specified (and {\tt VLSSuse=no}): your own skymodel is used for initial calibration
	       		\end{enumerate}
               
		\item \texttt{nbCycle} (int, default:10): This is the number of selfcal cycles to perform. It must be between 5 and 20. The selfcal process will start at 15 times the best possible resolution and decrease the resolution at each cycle in linear steps.
 

		\item \texttt{outerfovclean} (string, default:`no'): accepts `yes' or `no'. If this option is activated, a preprocessing cleaning to subtract sources from outside the field of view will be applied before the selfcal process.\\
		A first low resolution image will be generated (using core stations only) to obtain a sky model. This preliminary image has 30arcsec pixel size and a larger field of view (fov={\tt FOV}+2$\times${\tt annulusRadius} degrees). A skymodel is extracted and separated into an `annulus' skymodel (containing sources with angular distance $>${\tt FOV}$/2$ deg from the target) and a `centre' skymodel (containing sources with angular distance $<${\tt FOV}$/2$ deg from the target). The `annulus' skymodel is then subtracted from the visibilities. This cleaning is an iterative process and will continue until there are less than 10\% of sources from the original annulus skymodel (typically 2-3 iterations are needed).
                               
		\item \texttt{VLSSuse} (string, default:yes): accepts `yes' or `no': Use the VLSS-based skymodel for the first iteration of selfcal (as generated by {\tt gsm.py}).\\
		If the user does not want to use VLSS skymodel ({\tt VLSSuse=no}), the code will create a preliminary image (see the {\tt outerfovclean} parameter described above), and use the `center' skymodel as the input skymodel.

		\item \texttt{annulusRadius} (float, default:1): In degrees; if {\tt outerfovclean=yes}, you can select the radius of the outer annulus from which sources will be subtracted. By default, the value is 1~deg, i.e. the field of view will be $5+2\times1=7$~deg, and all sources which are less than 1 degree of the edge of the field of view will be subtracted.

		\item \texttt{startResolution} (float, default:$15\times$ the best resolution available from the data): The resolution to use in the first selfcal cycle. If not set, the starting resolution is automatically computed in arcseconds, using\\
		Starting Resolution $=15\times\lambda/\mathrm{max(baseline)}$. \\
 		NB: the {\tt startResolution} option conflicts with {\tt resolutionVector} and {\tt startingFactor} parameters. \\
 		NB: if {\tt startResolution} is provided, {\tt endResolution} must also be provided. 


		\item \texttt{endResolution} (float, default:best resolution available in the data): The resolution to use in the final selfcal cycle. If not set, the best resolution available is calculated using:\\
		Best Resolution $=\lambda/\mathrm{max(baseline)}$ \\
		NB:  the {\tt endResolution} option conflicts with {\tt resolutionVector} parameter, but not with {\tt startingFactor} \\
		NB:  if {\tt endResolution} is provided, {\tt startResolution} does not need to be provided.
 
		\item \texttt{resolutionVector} (list of float: e.g. {\tt resolutionVector=x1, x2, x3} (do not use parentheses), default:[none]): Select the resolution for each cycle. Format: list of float in arcseconds. \\
		NB: the {\tt resolutionVector} option conflicts with {\tt startResolution}, {\tt endResolution} and {\tt startingFactor} parameters. 


		\item \texttt{mask} (string, default:`yes'): accepts `yes' or `no':  Use clean masks when imaging.

		\item \texttt{maskDilation} (integer, default:0): If mask is used (generated a from PyBDSM-extracted sky model), it is possible to dilate it. 0 means no dilation, 1 means extend the mask regions by 1 pixel in all directions, etc. \\
		For more details see the PyBDSM {\tt export\_image} documentation.

		\item \texttt{UVmin} (float, default:0 or 0.1 in klambda): Set the {\tt awimager} parameter {\tt UVmin} for each imaging run. If unset, {\tt UVmin=0.1} will be used for observations with declinations below 35 degrees, otherwise {\tt UVmin=0}. It is recommended to use {\tt UVmin=0} if the user is interested in imaging extended or diffuse emission.


		\item \texttt{startingFactor} (int, default:15): The resolution of the initial selfcal cycle is set by {\tt startingFactor} $\times$ Best Resolution, or {\tt startingFactor} $\times$ {\tt endResolution}. \\
		NB:  the {\tt startingFactor} option conflicts with {\tt startResolution} and {\tt resolutionVector} parameters, but not with {\tt endResolution}  

		\item \texttt{FOV} (float, default:5): Select the image size (in degrees).

		\item \texttt{nofPixelPerBeam} (float, default:4): Number of pixels per synthesized beam.

		\item \texttt{robust} (float, default:none): If the {\tt robust} parameter is specified, it will be kept constant for all selfcal cycles. If unset, the robust parameter varies iteratively from 1 to -2. Must be in the interval: [2;-2] 

	\end{enumerate}


\item \textbf{Future Parameters:}

	\begin{enumerate}

		\item \texttt{catalogType} (string, default:`none'): accepts `GSM' or `PYBDSM': {\tt catalogType} is used together with the {\tt peeling} and {\tt DDC} options. It is required to be able to select in the initial catalog the brightest source (or sources) to peel.

		\item \texttt{peeling} (string, default:`no'): accepts `yes' or `no': If {\tt peeling=yes}, the brightest source (or sources) in the initial catalog will be peeled.\\
 		NB: the {\tt peeling} and {\tt DDC} options conflict. Do not use both at the same time.

		\item \texttt{DDC} (string, default:`no'): accepts `yes' or `no': Same as {\tt peeling} option, except that the subtraction is not applied, just the Direction Dependent Calibration.

		\item \texttt{nofDirections} (int, default:1): Number of directions (i.e. brightest sources) required to peel or to apply the Direction Dependent Calibration.

	\end{enumerate}

\end{itemize}


%\end{itemize}


The output directory will be automatically created if it does not exist. If the output directory already exists, it is advisable to delete the output of any previous runs to avoid conflicts. Note that if the selfcal process crashes, it is not possible to resume from the point where the crash occurred; a new run (starting from scratch) would be required. 



%----------------------------------------------------------------------------------------------------------------------------------------------------------------
\subsubsection{Required Data Format}
\label{selfcal:dataformat}

To process visibility data with the {\tt selfcal.py} script, flux-calibrated data must be written in the {\tt CORRECTED\_DATA} column of your Measurement Sets. Selfcal currently only performs phase calibration. Flux calibration is assumed to have been completed (e.g. by the Observatory using the Standard Imaging Pipeline) before running {\tt selfcal.py}; additional amplitude calibration is not performed (yet) by the self-calibration procedure.

The self-calibration routine assumes that the input visibility data have been grouped in sets of contiguous subbands, where the sets typically are made of 10-20 subbands. To facilitate the grouping of subbands and to ensure that the input visibility data are formatted in the way expected by {\tt selfcal.py}, a helper function is provided and should be used before running self-calibration.

The script, called {\tt mergeSB.py}, accepts intermediate or final visibility (MS) data from the Radio Observatory pipeline. These datasets are named following a specific convention. Therefore the names of the MS visibility datasets follow a specific pattern:
\begin{itemize}
\item Intermediate data: {\tt L123456\_SAP123\_SB123\_uv.MS.dppp}
\item Final data: {\tt L123456\_SB123\_uv.dppp.MS}
\end{itemize}
These visibility datasets are equivalent and may be used interchangeably for {\tt mergeSB.py} and\\ {\tt selfcal.py}. 

If the names of your visibility datasets are different, but you still feel that they are prepared properly for use in the self-calibration routine, then you will have to manually change the name of the Measurement Sets to conform with one of the two naming conventions provided above. In addition, please be aware that the Measurement Sets will be grouped alphabetically (using the {\tt ls} order); therefore be sure that the subband numbers are given as {\tt 001}, {\tt 002}, etc.

For the input to {\tt selfcal.py}, the names of the frequency merged Measurement Sets do not need to follow any convention (except that the chronological order of a set of snapshots must somehow be reflected in the alphabetical order of the MS names). The procedures in {\tt selfcal.py} will always work on the {\tt CORRECTED\_DATA} column, which is created by {\tt mergeSB.py}. To avoid confusion it is highly recommended to use the two scripts together, unless you want to work on only one subband. 

To combine subbands, type \texttt{mergeSB.py} at the command prompt. If you provide no arguments (or type \texttt{mergeSB.py -h}) then the required parameters are listed:\\\,\\
\texttt{MISSING Parameters}\\
\texttt{Usage: mergeSB.py --obsDir= --outputDir= --column2Merge= Optional:\\ --nofChannelPerSubband=  --checkNames]}\\\,\\
If {\tt nofChannel2Merge} is not provided, the default behavior is to merge the data to a frequency resolution corresponding to 1 channel per subband. The default value for {\tt checkNames} is yes.\\

\begin{itemize}

\item \textbf{Required Parameters:} 

	\begin{enumerate}

		\item \texttt{obsDir} (string): This parameter specifies a directory containing only a set of single-subband Measurement Sets (MS) that should be merged (per time chunk). This directory should contain the Measurement Sets for all available time chunks, and all subbands per time chunk, that the user wants to merge. For example if you want to use {\tt selfcal.py} on a dataset that consists of 10 time chunks and one group of 5 contiguous subbands, then the {\tt obsDir} must contain $10\times5=50$ MSs. It is often the case that one or more Measurement Sets are missing. This is acceptable, because {\tt DPPP} (which is used to merge the subbands) will fill missing frequency values with flagged data, corresponding to missing Measurement Sets. This is true as long as the first 			and the last subband are present for all time chunks.\\
		The path given for {\tt --obsDir} must be a full path.
               
		\item \texttt{outputDir} (string): This parameter specifies the output directory for merged Measurement Sets. In this directory, two subdirectories will be created. The first one, named {\tt Merged-Data}, will contain the merged time chunks. This subdirectory should be used afterwards as the {\tt --obsDir} parameter for {\tt selfcal.py}. The second subdirectory, named {\tt DPPP\_Parset}, will contain the parsets that are used for merging the subbands. The user can inspect these parsets to find out how the subbands have been merged by {\tt mergeSB.py}.\\
		The path given for {\tt --outputDir} must be a full path.

		\item \texttt{column2Merge} (string): This is the name of the column to merge. Usually {\tt CORRECTED\_DATA} is used ({\tt awimager} will only use the {\tt CORRECTED\_DATA} column).\\
		Usually, this should be set to either {\tt DATA} or {\tt CORRECTED\_DATA}.

	\end{enumerate}


\item \textbf{Optional parameters:}

	\begin{enumerate}

		\item \texttt{nofChannelPerSubband} (int, default:1): This is the number of channels per subband required after the subband concatenation. By default, {\tt mergeSB.py} averages to 1 channel per subband.
 
		\item \texttt{checkNames} (string, default:`yes'): [OPTION NOT ACTIVATED AT THE MOMENT] {\tt mergeSB.py} checks the names of the MSs (to verify that they are either intermediate or final pipeline data products, see documentation). 
		If {\tt checkNames} is set to `no', a warning appears if the input MS names do not respect the expected nomenclature, but {\tt mergeSB.py} will continue without exiting.

	\end{enumerate}

\end{itemize}


   
%----------------------------------------------------------------------------------------------------------------------------------------------------------------
\subsubsection{Selfcal implementation details}

The self-calibration process consists of a set of individual tasks. These are run in a sequence which is looped, and at each cycle (iteration) the image resolution is increased to improve the sky model, allowing a better phase calibration to be performed at the next step.

The individual tasks that are combined within a single iteration are:
\begin{enumerate}
\item Calibration with a skymodel using {\tt calibrate-stand-alone} (BBS)
\item Flagging using {\tt DPPP}
\item Imaging using {\tt awimager}
\item Sky model extraction using the {\tt PyBDSM} source finder.\footnote{The PyBDSM manual is available at {\tt http://www.astron.nl/citt/pybdsm/}}
\end{enumerate}

\begin{figure}[!ht]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{SelfCalibration-Scheme.png}
    \caption[selfcal-diagram.]{Illustration of the iterations performed by the selfcal process.}
    \label{fig:global-selfcal}
  \end{center}
\end{figure}

\begin{figure}
  \begin{center}
    \includegraphics[width=1\textwidth]{selfcal-results.png}
    \caption[HBA selfcal example]{Results of selfcal process with an example HBA dataset at approximately 140 MHz. There are clear improvements in the image noise after performing self-calibration. The noise levels in the three images are 30, 10, and 2 mJy/beam for the cases of: no phase calibration ({\it left panel}), standard GSM-based phase calibration ({\it middle}), and self-calibration ({\it right panel}). The thermal noise is estimated to be approximately 0.7 mJy/beam for this dataset.}
    \label{fig:example-selfcal-hba}
  \end{center}
\end{figure}


The diagram in Figure \ref{fig:global-selfcal} shows an example of the self-calibration process. Within a cycle, each time chunk is calibrated separately using {\tt BBS}, then each of them is flagged using {\tt DPPP}. It is necessary to calibrate and flag them separately because it is not possible (at the moment) to calibrate non-continuous time-concatenated MSs using BBS. After this step (which is parallelized on 8 cores), the MSs are concatenated in time using the {\tt msconcat} command from the {\tt pyrap.tables} python module. Next, {\tt awimager} images the time concatenated MS with specific parameters. To finish, a sky model is extracted using {\tt pybdsm} on the newly created image.

The algorithm is designed to improve the angular resolution in each cycle. This means that {\tt selfcal} must include longer baselines at each iteration. Moreover, the robust parameter is also changed in each iteration to give additional weight to longer baselines. The default behavior is to start at {\tt robust=1} (nearly natural weighting) at low resolution (15 times the best resolution) and end at {\tt robust=-2} (uniform weighting) when including all available baselines. As described in \S\,\ref{selfcal:standalone}, the progression of imaging parameters from one iteration to the next can be modified by the user.


%----------------------------------------------------------------------------------------------------------------------------------------------------------------
\subsubsection{Selfcal examples}




\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.9\textwidth]{LBA-screen.png}
    \caption[LBA selfcal example]{Results of selfcal process with LBA ($\sim$60 MHz). The noise levels in the image is 22 mJy/beam. The thermal noise is estimated to be approximately 11 mJy/beam for this dataset.}
    \label{fig:example-selfcal-lba}
  \end{center}
\end{figure}

In Figure \ref{fig:example-selfcal-hba}, an example of the self-calibration process is shown for HBA-low. The example dataset had the following properties:
\begin{itemize}
\item 31 time chunks
\item 10 contiguous subbands
\item Best beam resolution $=5^{\prime\prime}$
\end{itemize}


An example of using self-calibration on an example LBA dataset is shown in Figure \ref{fig:example-selfcal-lba}. This LBA dataset contains 10 subbands and the total duration of the observation was 4.5~h. Ionospheric effects become very strong on baselines longer than about 20 km (in this particular example), so the direction independent strategy does not improve the data quality for angular resolution less than about $50^{\prime\prime}$ in this particular case. If this example is representative, then {\tt selfcal} can be expected to improve calibration on baselines out to approximately 20~km, depending on ionospheric conditions at the time. Further improvements will require direction dependent calibration.
At present, with both HBA and LBA datasets, the selfcal process typically manages to obtain a final image noise about 2-3 times the theoretical thermal noise. 













