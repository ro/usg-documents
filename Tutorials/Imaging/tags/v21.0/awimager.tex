\svnInfo $Id: awimager.tex 9414 2012-01-18 08:54:02Z shulevski $

\section[The AW Imager]{The AW imager\footnote{This Chapter is maintained by Bas van der Tol ({\tt tol[at]astron[dot]nl}).}}
\label{sec:awimager}

%------------------------------------------------------------------------------------------

\subsection{Introduction}
\label{introduction}

In this section we will describe the necessary steps needed to perform
successful imaging of LOFAR data using the AWimager\footnote{Cyril Tasse, 
Bas van der Tol, Joris van Zwieten, Ger van Diepen, Sanjay Bhatnagar 2013, 
\emph{Applying full polarization A-Projection to very wide field of view 
instruments: An imager for LOFAR}. A\&A, 553, A105}.

Note that the AWimager is
still in the development phase, therefore this documentation is very dynamic
and it is currently meant to provide the basic instructions on how to use the code. 

%In particular, a new version of the AWImager is scheduled for mid 2015. This new
%version will have more features, like multi-frequency clean. The options for
%AWImager2 are different (more intuitive) than that of AWImager. It will be 
%documented in the next release of the Cookbook.

%------------------------------------------------------------------------------------------

\subsection{Background}
\label{background}

The AWimager is specially adapted to image wide fields of view, and
imaging data produced by non-coplanar arrays, where the W term in the
measured visibilities is not negligible. Furthermore, AWimager
corrects for direction dependent effects (LOFAR beam and ionosphere)
varying in time and frequency. The used algorithm is A-projection.

The algorithm is implemented using some CASA libraries.

%------------------------------------------------------------------------------------------

\subsection{Usage}
\label{usage}

To run AWimager, you first need to setup your environment using:
\begin{verbatim}
use Lofar
\end{verbatim}

Before running AWimager, it is necessary to calibrate the dataset and correct
the visibilities towards the phase center of the observation. This can now be
done by not specifying any direction in the {\it correct} step of BBS.

\begin{verbatim}
Step.correct.Model.Sources = []
\end{verbatim}

AWimager can run in a parallel fashion. The number of processing cores
({\tt n}) to be used during imaging can be specified:

\begin{verbatim}
export OMP_NUM_THREADS=n
\end{verbatim}

If not specified, all cores will be used.
\\AWimager is quite memory hungry, so the number of cores should be
limited in case it fails due a \texttt{'bad alloc'} error.

\subsection{Output files}
AWimager creates several image output files. Note that in the
following list {\tt <image>} is the image name given using the
{\tt image} parameter. 
\begin{itemize}
  \item {\tt <image>.model}
is the uncorrected dirty image.
  \item {\tt <image>.model.corr}
is the dirty image corrected for the average primary beam.
  \item {\tt <image>.restored} and {\tt <image>.restored.corr} 
are the restored images.
  \item {\tt <image>.residual} and {\tt <image>.residual.corr} 
are the residual images.
  \item {\tt <image>.psf}
is the point spread function.
  \item {\tt <image>0.avgpb}
is the average primary beam.
\end{itemize}
Furthermore, a few other files might be created for AWimager's
internal use.

\subsection{Parameters}
An extensive list of the parameters that can be used by the AWimager
can be obtained by typing:

\begin{verbatim}
awimager -h
\end{verbatim}

Eventually, to run the imager, you can type:

\begin{verbatim}
awimager ms=test.MS image=test.img weight=natural wprojplanes=64 npix=512 
cellsize=60arcsec data=CORRECTED_DATA padding=1. niter=2000 
timewindow=300 stokes=IQUV threshold=0.1Jy operation=csclean
\end{verbatim}

which is one command spread over multiple lines.

It is also possible to specify these parameters in a parset and run it
like:
\begin{verbatim}
awimager parsetname
\end{verbatim}

Many parameters can be set for the AWimager. Several of
them are currently being tested by the commissioners. The most
important parameters are listed below.

\subsubsection{Data selection}
These parameters specify the input data.
\begin{itemize}
\item {\tt ms}
\\The name of the input MeasurementSet.
\item {\tt data}
\\The name of the data column to use in the MeasurementSet. The default is DATA.
\item {\tt antenna}
\\Baseline selection following the CASA baseline selection syntax
\item {\tt wmax}
\\Ignore baselines whose w-value exceeds wmax (in meters).
\item {\tt uvdist}
\\Ignore baselines whose length (in wavelengths) exceed uvdist.
\item {\tt select}
\\Only use data matching this TaQL selection string. For example,
{\tt sumsqr(UVW[:2])<1e8} selects baselines with length $<$10km.
\end{itemize}

\subsubsection{Image properties}
These parameters define the properties of the output image.
\begin{itemize}
\item {\tt image}
\\The name of the image.
\item {\tt npix}
\\The number of pixels in the RA and DEC direction
\item {\tt cellsize}
\\The size of each pixel. An unit can be given at the end. E.g.  {\tt 30arcsec}
\item {\tt padding}
\\The padding factor to use when imaging. It can be used to get rid
of effects at the edges of the image. If, say, 1.5 is given, the number
of pixels used internally is 50\% more.
\item {\tt stokes}
\\The Stokes parameters for which an image is made. If A-projection
is used, it must be IQUV.
\end{itemize}

\subsubsection{weighting}
These parameters select the weighting scheme to be used.
\begin{itemize}
\item {\tt weight}
\\Weighting scheme (uniform, superuniform, natural, briggs (robust), briggsabs, or radial)
\item {\tt robust}
\\Robust weighting parameter.
\end{itemize}

\subsubsection{Operation}
This parameter selects the operation to be performed by awimager.
\begin{itemize}
\item {\tt operation}
\\The operation to be performed by the AWimager.
  \begin{itemize}
    \item csclean = make an image and clean it (using Cotton-Schwab).
    \item multiscale = use multiscale cleaning
    \item image = make dirty image only.
    \item predict = fill the data column in the MeasurementSet by
      predicting the data from the image.
    \item empty = make an empty image. This can be useful if only 
      image coordinate info is needed.
  \end{itemize}
\end{itemize}

\subsubsection{Deconvolution}  
These parameters control the deconvolution algorithm. Only those
parameters that are applicable to the selected operation will be
used.
\begin{itemize}
\item {\tt niter}
\\The number of clean iterations to be done. The default is 1000.
\item {\tt gain}
\\The loop gain for cleaning. The default is 0.1.
\item {\tt threshold}
\\The flux level at which to stop cleaning. The default is 0Jy.
\item {\tt uservector}
\\Comma separated list of scales (in pixels) to be used by
multiscale deconvolution
\end{itemize}

\subsubsection{Gridding}
These parameters control the AW-projection algorithm.
\begin{itemize}
\item {\tt wprojplanes}
\\The number of W projection planes to use.
\item {\tt maxsupport}
\\The maximum of W convolution functions. The default is 1024.
\item {\tt oversample}
\\The oversampling to use for the convolution functions. The default is 8.
\item {\tt timewindow}
\\The width of the time window (in sec) where the AW-term is assumed
to be constant. Default is 300 sec. The wider the window, the faster
the imager will be.
\item {\tt splitbeam}
\\Evaluate station beam and element beam separately? The default is true.
AWimager will work much faster if the correction for the station and
element beam can be applied separately. This should only be done if
the element beam is the same for all stations used.
\end{itemize}

For more details, the user can refer to the Busy Wednesday \href{http://www.lofar.org/operations/doku.php?id=commissioning:busy_wednesdays}{comissioning reports}\footnote{\url{http://www.lofar.org/operations/doku.php?id=commissioning:busy_wednesdays}} (specifically those from September 28 and October 26 2011).

