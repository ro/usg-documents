\svnInfo $Id: examples.tex 4940 2010-05-25 12:00:09Z pizzo $

\section{Practical examples}
\label{practicalexamples}

\subsection{Calibration of 3C66 data obtained with LOFAR}
\label{sec:3c66}

In this section we will describe a way to get started calibrating the field centered on the radio source 3C66 observed by LOFAR. The data reduction is specifically written with dataset ID {\tt L2010\_07096} in mind. With the exception of the CASA imager described at the end, everything is done using LOFAR software tools.  The people who have contributed to this data reduction are George Heald, Ger de Bruyn, Bj{\"o}rn Adebahr, and Monica Trasatti. The final image is reported in Fig.~\ref{3C66}.


\begin{figure*}[!hb]
\begin{center}
    \includegraphics[scale=0.6]{3c66ghbb.pdf}
  \caption{The field of 3C66 observed by LOFAR. The image was formed from a single sub band at 138~MHz, and has a dynamic range of 560. The ring-shaped artifacts are caused by 3C65, which is located just beyond the southern edge of the image.}
  \label{3C66}
  \end{center}
\end{figure*}



%---------------------------------------------------------------------------------------------------------------

\vspace{1cm}
{\large{\bf Data description}}

The data are in the high band and cover approximately the frequency range 125--174 MHz. The system was set up in its {\tt HBA\_DUAL} mode so that all HBA ears in the core are correlated separately. Thus, although only 16 stations (11 core + 5 remote) were used for the observation, there appear to be 27 stations in the Measurement Sets. The duration of the observation is 6 hours, and the integration time is 3 seconds. Each of the 248 sub bands contains 256 channels and spans 0.2 MHz. All four polarizations are recorded.


%---------------------------------------------------------------------------------------------------------------

\vspace{1cm}
{\large{\bf Flagging and data averaging}}

DPPP can be used to flag and average the data. The following settings are suggested for this data reduction. It should be noted that we flag baselines shorter than 400 meters, and this step may be unnecessary.

\begin{verbatim}

##########################################
#
#     DPPP.parset
#
##########################################

msin = /net/sub4/lse010/data2/L2010_07096/SB63.MS
msin.startchan = 8
msin.nchan = 240
msin.datacolumn = DATA     # is the default

msout = "SB63_DPPP.MS"
msout.datacolumn = DATA    # is the default

steps = [uvwflag,count,flag1,count,avg1,flag2,count]

uvwflag.type=uvwflagger
uvwflag.uvmmin=400

flag1.type=madflagger
flag1.threshold=3
flag1.freqwindow=31
flag1.timewindow=5
flag1.correlations=[0,3]   # only flag on XX and YY

avg1.type = squash
avg1.freqstep = 16
avg1.timestep = 1          # is the default

flag2.type=madflagger
flag2.threshold=2
flag2.timewindow=51
\end{verbatim}


Note that the entry {\tt flag2.timewindow=51} can be replaced by larger values up to the total duration of the observation, depending on the source structure in the field. This can help in removing RFI deriving from sunrise/sunset. In some cases (for instance those with a bright dominant point source), the amplitude is not expected to vary very much with time. By not increasing the {\tt timewindow} parameter, data associated with sunrise/sunset may not be flagged because the associated rise and fall of the visibility amplitudes are too slow.\\
%
%Pre-DPPP'd data (using these settings or ones that are very similar) have been produced by Roberto Pizzo and all sub bands are available in George Heald's scratch area on lce033:\\
%\begin{verbatim}
%/data/scratch/heald/L2010_07096_all/goodSB/
%\end{verbatim}
%The ones in the {\tt badSB} directory at the same location are also DPPP'd, but probably suffer from a problem that makes their visibility amplitudes much lower than they should be. There's probably no need to spend time on these sub bands.
%
%Even though the shortcut of having pre-DPPP'd data around is available, note that 
%
Ger de Bruyn recommends averaging of a factor of 16 in frequency.  By discarding the first and last 8 channels (as recommended above), 15 channels would be present in the output sub bands. With this set up, it should be possible to subtract 3C65\footnote{We do not describe here how to go about subtracting 3C65, but future versions of the cookbook will cover this topic.} more completely than we were able to manage (it is several degrees away and we induced significant bandwidth smearing by averaging so many channels). \\
%The only relevant settings change should be to modify line 21 above to read:
%
%\begin{verbatim}
%avg1.freqstep = 16
%\end{verbatim}
%
%The data has not been run through DPPP with these settings yet, therefore you will probably have to do some of the sub bands yourself. \\ 
%

It is worth noting that the AOFlagger (see Sect.~\ref{aoflagger}) has recently proved to give much better flagging results than DPPP. Therefore, you could alternatively use this flagger and then DPPP only for the data averaging. A few sub bands processed in this way are available on lce029 at the location {\tt /data/safe/3C66data}.

After running AOFlagger and/or DPPP, you may want to load CASA and use the command {\tt clearcal}, which will prevent the CASA imager from overwriting the gain-corrected data column ({\tt CORRECTED\_\\DATA}) produced by the calibration\footnote{Strictly speaking, this is no longer necessary if you use {\tt calready=False} in the CASA imager settings, but if you forget and leave {\tt calready=True}, then CASA will overwrite your {\tt CORRECTED\_DATA} column unless you first run {\tt clearcal}.}:

\begin{verbatim}
taskname           = "clearcal"
vis                =  "SB63_DPPP.MS"
field              =  ""
spw                =  ""
\end{verbatim}



%------------------------------------------------------------------------------

\vspace{0.7cm}
{\large{\bf Preliminary model and calibration}}

The preliminary model does not have to be as perfect as it had to be when LOFAR consisted of about 5--7 stations. Then it was essential to provide BBS with a first calibration model that was extremely close to reality. 

The model that we have used to get started is the following:

\begin{verbatim}

# (Name, Type, Ra, Dec, I, Q, U, V, ReferenceFrequency='60e6', \
     SpectralIndexDegree='0', SpectralIndex:0='0.0', MajorAxis, \
     MinorAxis, Orientation) = format
# The above line defines the field order and is required.

3C66A, POINT, 02:22:39.6115, +43.02.07.799, 18.46, 0.0, 0.0, 0.0, '138e6'
3C66B, GAUSSIAN, 02:23:18.4, +42.59.37.2, 37.0, 0.0, 0.0, 0.0, '138e6',\
                '0', '0.0', 200.0, 112.5, 96.3

\end{verbatim}

Single lines are spread over multiple lines separated by backslash.
The flux of 3C66A should be more or less correct. The settings for the Gaussian description of 3C66B were obtained by calculating a power-law interpolation between the WENSS and NVSS images of the source, and subsequently doing a by-eye estimation of the axis ratio and position angle. We estimated that the total flux of the source was approximately twice the peak flux of 3C66A, which was sufficient to produce a reasonable first image.

In general, for faint sources it is best to use reference values from \href{http://nedwww.ipac.caltech.edu/}{NED}\footnote{{\tt http://nedwww.ipac.caltech.edu/}}. Extended structures like 3C66B seem to be better represented by Gaussians. A good way to proceed is to try to find an image of the target field in NED and apply a Gaussian to it, simulating the effect of the LOFAR station beam. This will help you to estimate the source flux values.

It should be noted that for more than one strong source in the field, the relative flux between the sources is a critically important factor. Moreover, for the first self-calibration it seems that that it is better to start with a model containing about half of the flux in the field and include only the strongest sources in it. Then, when proceeding in self calibration iterations, more flux and more sources can be added.

The calibration has been performed by using the following BBS parset file:


\begin{verbatim}

################################################
#
#   BBS.parset
#
################################################

Strategy.ChunkSize = 0
Strategy.Steps = [solve, correct]

Step.solve.Operation = SOLVE
Step.solve.Model.Sources = []
Step.solve.Model.Gain.Enable = T
Step.solve.Model.Cache.Enable = T
Step.solve.Solve.Parms = ["Gain:0:0:*","Gain:1:1:*"]
Step.solve.Solve.CellSize.Freq = 0
Step.solve.Solve.CellSize.Time = 1
Step.solve.Solve.CellChunkSize = 10
Step.solve.Solve.Options.MaxIter = 20
Step.solve.Solve.Options.EpsValue = 1e-9
Step.solve.Solve.Options.EpsDerivative = 1e-9
Step.solve.Solve.Options.ColFactor = 1e-9
Step.solve.Solve.Options.LMFactor = 1.0
Step.solve.Solve.Options.BalancedEqs = F
Step.solve.Solve.Options.UseSVD = T

Step.correct.Operation = CORRECT
Step.correct.Model.Sources = []
Step.correct.Model.Gain.Enable = T
Step.correct.Output.Column = CORRECTED_DATA


\end{verbatim}

Note that {\tt Step.solve.Solve.CellSize.Time = 1} represents the size of the solution interval for calibration in units of integration time. It may be useful to start with a high solution interval (like 300) and lower it to 1 during the the self-calibration iterations. For example, you could successively use values of 300, 100, 50, 10, and 1. This seems to be an important point, as for a complex field the first model used for calibration is not that accurate.

After running BBS, you should inspect the solutions using {\tt parmdbplot.py}, and the {\tt CORRECTED\_DATA} column using {\tt uvplot.py} (see Sect.~\ref{parmdbplot} and \ref{quickbaselinebasedvisibilityinspection}, respectively).  Inevitable bad solutions producing corrected data with very high amplitude will appear. To flag the data suffering from bad solution, you should run DPPP, preferably with the following settings:

\begin{verbatim}
msin = SB62_DPPP.MS
msin.datacolumn = CORRECTED_DATA  
msout=

steps = [flag1]

flag1.type=madflagger
flag1.threshold=3
flag1.timewindow=501
\end{verbatim}


%-------------------------------------------------------------------------------------------------------

\vspace{1cm}
{\large{\bf Imaging}}

To image the field, you can load CASA again and use the task {\tt clean}. This performs also good wide field imaging, as well as deconvolution. The task settings that we used are:

\begin{verbatim}
 
taskname            =         "clean"
vis                 =         'SB63_DPPP.MS'        
imagename           =         ['iter09']       
outlierfile         =           ''       
field               =           ''       
spw                 =           ''       
selectdata          =          False     
mode                =          'mfs'    
nterms              =            1        
reffreq             =           ''                                                                                
gridmode            =         'widefield'      
    wprojplanes     =          256       
      facets        =           1      
niter               =          2000       
gain                =          0.1      
threshold           =         '0.0mJy'       
psfmode             =         'clark'        
imagermode          =          ''      
multiscale          =           []       
interactive         =           True       
mask                =           []   
imsize              =         [2048, 2048]
cell                =     ['15.0arcsec', '15.0arcsec']
phasecenter         =           ''        
restfreq            =           ''        
stokes              =          'I'       
weighting           =       'natural'      
uvtaper             =        False       
modelimage          =           ''        
restoringbeam       =         ['']       
pbcor               =        False       
minpb               =          0.2       
calready            =         True        
async               =         False        

\end{verbatim}







%taskname           = "widefield"
%vis                =  "SB63_DPPP.MS"
%imagename          =  "SB63_DPPP.MS"
%outlierfile        =  ""
%field              =  ""
%spw                =  ""
%selectdata         =  False
%timerange          =  ""
%uvrange            =  ""
%antenna            =  ""
%scan               =  ""
%mode               =  "mfs"
%niter              =  2000
%gain               =  0.1
%threshold          =  "0.0mJy"
%psfmode            =  "clark"
%ftmachine          =  "wproject"
%facets             =  1
%wprojplanes        =  256
%multiscale         =  []
%negcomponent       =  -1
%interactive        =  True
%mask               =  "iter05.mask"
%nchan              =  1
%start              =  0
%width              =  1
%imsize             =  [2048, 2048]
%cell               =  ['15.0arcsec', '15.0arcsec']
%phasecenter        =  ""
%restfreq           =  ""
%stokes             =  "I"
%weighting          =  "natural"
%robust             =  0.0
%npixels            =  0
%noise              =  "1.0Jy"
%cyclefactor        =  1.5
%cyclespeedup       =  -1
%npercycle          =  100
%uvtaper            =  False
%outertaper         =  ['', '', '0deg']
%innertaper         =  ['1.0']
%restoringbeam      =  ['']
%calready           =  False
%\end{verbatim}

You will notice that the {\tt interactive} parameter is set to {\tt True}, which allows you to fiddle around with the mask. In this mode you can also modify the number of CLEAN components and iterations on-the-fly.


%----------------------------------------------------------------------------------

\vspace{1.5cm}
{\large{\bf Calibration cycle}}

In order to self calibrate, you should now take the clean components that you just obtained and feed them back in to BBS. 
This can be achieved by using a program provided by Joris van Zwieten. It is available at %In order to make up for an unfortunate factor-of-two difference between Stokes I in CASA-speak and Stokes I in LOFAR-speak, I had to modify this program slightly. It's available at\\
{\tt $\sim$heald/bin/casapy2bbs} and can be used through the following command:

\begin{verbatim}
> ~heald/bin/casapy2bbs iter09 iter09.catalog
\end{verbatim}

Note that the name of the clean component image produced by {\tt widefield} is just the string given in parameter {\tt imagename} and all other images produced by {\tt widefield} are called {\tt <imagename>.*}. The output of the script, in this case {\tt iter09.catalog}, can then be used as a new  skymodel for BBS. 

Following a new calibration, you can make a new image and loop as many times as you like around the major cycle: convert clean component lists to a new BBS skymodel, run BBS again, and produce a new image.

If you want to look at your image in kvis, you will have to first make a fits file:

\begin{verbatim}
image2fits in=iter09.image out=iter09.image.fits
\end{verbatim}

To increase the sensitivity of the final image, you can combine several sub bands. Currently, this can be done in the image plane by using the scripts {\tt average.py} or {\tt average\_weights.py}\footnote{Available in {\tt ~/pizzo/EXAMPLES/Scripts}}. However, a better final result is obtained by combining the sub bands in the uv domain. This can be done by using Oleg Smironov's script, which can be obtained by typing from your shell:

\begin{verbatim}
> svn co svn://lofar9.astron.nl/var/svn/repos/trunk/Owlcat
\end{verbatim}

After this command has completed, you can run:

\begin{verbatim}
> Owlcat/merge-ms.py -s output.MS SB*.MS
\end{verbatim}

%Remember to specify the sub band names in the script {\tt merge-ms.py}. Moreover, note that this script handles single columns datasets, i.e. datasets not containing a  {\tt CORRECTED\_DATA} column.
%By using the CASA task {\tt SPLIT}, you will be able to apply the gain tables to the data, thus writing a new dataset containing the {\tt CORRECTED\_DATA} column only.
