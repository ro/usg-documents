\svnInfo $Id$

\section[Source detection and sky model manipulation: PyBDSM and LSMTool]{Source detection and sky model manipulation: PyBDSM and LSMTool\footnote{This section is maintained by David Rafferty ({\tt drafferty[at]hs[dot]uni-hamburg[dot]de}).}}
\label{PyBDSM}
%----------------------------------------------------------------------------------------------------------------------------------------------------

\subsection{Source detection: PyBDSM}
\subsubsection{Introduction}

PyBDSM ({\bf Py}thon {\bf B}lob {\bf D}etection and {\bf S}ource {\bf M}easurement) is a Python source-finding software package written by Niruj Mohan, Alexander Usov, and David Rafferty. PyBDSM can process FITS and CASA images and can output source lists in a variety of formats, including \texttt{makesourcedb} (BBS), FITS and ASCII formats. It can be used interactively in a casapy-like shell or in Python scripts. The full PyBDSM manual is located at \href{http://tinyurl.com/PyBDSM-doc}{{\tt  http://tinyurl.com/PyBDSM-doc}}.


%----------------------------------------------------------------------------------------------------------------------------------------------------

\subsubsection{Recent Changes}
\label{pybdsm:recentchanges}

Changes to PyBDSM since the last version of the cookbook include:
\begin{itemize}

\item New output option ({\tt bbs\_patches = 'mask'}) to allow patches in an output BBS sky model to be defined using a mask image (set with the {\tt bbs\_patches\_mask} option).

\item Many minor bug fixes (use {\tt help changelog} for details).
\end{itemize}

%----------------------------------------------------------------------------------------------------------------------------------------------------

\subsubsection{Setup}

The latest version of PyBDSM is installed on the CEP clusters. To initialize your environment for PyBDSM, run:
\begin{verbatim}
> use Lofar
\end{verbatim}

After initialization, the interactive PyBDSM shell can be started with the command {\tt pybdsm} and PyBDSM can be imported into Python scripts with the command {\tt from lofar import bdsm}.

%----------------------------------------------------------------------------------------------------------------------------------------------------

\subsubsection{Usage}

The following describes how to run an analysis using the PyBDSM interactive interface. For details on using PyBDSM directly in Python scripts, see Section~\ref{S:python}.

After initialization (see above), the PyBDSM interactive shell is started from the prompt with the command {\tt pybdsm}. Upon startup, the version number and a brief overview of the available commands and tasks are shown:
\begin{verbatim}
> pybdsm
PyBDSM version 1.8.3 (LOFAR revision 30100)
========================================================================
PyBDSM commands
  inp task ............ : Set current task and list parameters
  par = val ........... : Set a parameter (par = '' sets it to default)
                          Autocomplete (with TAB) works for par and val
  go .................. : Run the current task
  default ............. : Set current task parameters to default values
  tput ................ : Save parameter values
  tget ................ : Load parameter values
PyBDSM tasks
  process_image ....... : Process an image: find sources, etc.
  show_fit ............ : Show the results of a fit
  write_catalog ....... : Write out list of sources to a file
  export_image ........ : Write residual/model/rms/mean image to a file
PyBDSM help
  help command/task ... : Get help on a command or task
                          (e.g., help process_image)
  help 'par' .......... : Get help on a parameter (e.g., help 'rms_box')
  help changelog ...... : See list of recent changes
________________________________________________________________________
\end{verbatim}

A standard analysis is performed using the {\tt process\_image} task. This task reads in the input image, calculates background rms and mean images, finds islands of emission, fits Gaussians to the islands, and groups the Gaussians into sources. Use {\tt inp process\_image} to list the parameters:
\begin{verbatim}
BDSM [1]: inp process_image
--------> inp(process_image)
PROCESS_IMAGE: Find and measure sources in an image.
================================================================================
filename ................. '': Input image file name
adaptive_rms_box ..... False : Use adaptive rms_box when determining rms and
                               mean maps
advanced_opts ........ False : Show advanced options
atrous_do ............ False : Decompose Gaussian residual image into multiple
                               scales
beam .................. None : FWHM of restoring beam. Specify as (maj, min, pos
                               ang E of N) in degrees. E.g., beam = (0.06, 0.02,
                               13.3). None => get from header
flagging_opts ........ False : Show options for Gaussian flagging
frequency ............. None : Frequency in Hz of input image. E.g., frequency =
                               74e6. None => get from header. For more than one
                               channel, use the frequency_sp parameter.
interactive .......... False : Use interactive mode
mean_map .......... 'default': Background mean map: 'default' => calc whether to
                               use or not, 'zero' => 0, 'const' => clipped mean,
                               'map' => use 2-D map.
multichan_opts ....... False : Show options for multi-channel images
output_opts .......... False : Show output options
polarisation_do ...... False : Find polarisation properties
psf_vary_do .......... False : Calculate PSF variation across image
rms_box ............... None : Box size, step size for rms/mean map calculation.
                               Specify as (box, step) in pixels. E.g., rms_box =
                               (40, 10) => box of 40x40 pixels, step of 10
                               pixels. None => calculate inside program
rms_map ............... None : Background rms map: True => use 2-D rms map;
                               False => use constant rms; None => calculate
                               inside program
shapelet_do .......... False : Decompose islands into shapelets
spectralindex_do ..... False : Calculate spectral indices (for multi-channel
                               image)
thresh ................ None : Type of thresholding: None => calculate inside
                               program, 'fdr' => use false detection rate
                               algorithm, 'hard' => use sigma clipping
thresh_isl ............. 3.0 : Threshold for the island boundary in number of
                               sigma above the mean. Determines extent of
                               island used for fitting
thresh_pix ............. 5.0 : Source detection threshold: threshold for the
                               island peak in number of sigma above the mean. If
                               false detection rate thresholding is used, this
                               value is ignored and thresh_pix is calculated
                               inside the program
\end{verbatim}

Standard autocompletion of command, task, and parameter names and values is available with the {\tt<TAB>} key. As in casapy, {\tt inp} prints the parameter names and their current values, as well as a short description of each parameter. Full information about a parameter is available with the help command (e.g., {\tt help 'mean\_map'}). Once parameters are set, the analysis can be run with the command {\tt go}. The progress of the fit is printed to the screen, and a log file (named as the input image name with ``.pybdsm.log'' appended) with additional information about the fit will be saved in the current directory.

Once processing has finished, the results of the fit may be inspected using the {\tt show\_fit} task:
\begin{verbatim}
BDSM [1]: inp show_fit
--------> inp(show_fit)
SHOW_FIT: Show results of fit.
================================================================================
broadcast ............ False : Broadcast Gaussian and source IDs and coordinates
                               to SAMP hub when a Gaussian is clicked?
ch0_flagged .......... False : Show the ch0 image with flagged Gaussians (if
                               any) overplotted
ch0_image ............. True : Show the ch0 image. This is the image used for
                               source detection
ch0_islands ........... True : Show the ch0 image with islands and Gaussians (if
                               any) overplotted
gmodel_image .......... True : Show the Gaussian model image
gresid_image .......... True : Show the Gaussian residual image
mean_image ............ True : Show the background mean image
pi_image ............. False : Show the polarized intensity image
psf_major ............ False : Show the PSF major axis variation
psf_minor ............ False : Show the PSF minor axis variation
psf_pa ............... False : Show the PSF position angle variation
pyramid_srcs ......... False : Plot the wavelet pyramidal sources
rms_image ............. True : Show the background rms image
smodel_image ......... False : Show the shapelet model image
source_seds .......... False : Plot the source SEDs and best-fit spectral
                               indices (if image was processed with
                               spectralindex_do = True). Sources may be chosen
                               by ID with the 'c' key or, if ch0_islands = True,
                               by picking a source with the mouse
sresid_image ......... False : Show the shapelet residual image
\end{verbatim}

Internally derived images (e.g, the Gaussian model image) can be exported to FITS files using the {\tt export\_image} task:
\begin{verbatim}
BDSM [1]: inp export_image
--------> inp(export_image)
EXPORT_IMAGE: Write one or more images to a file.
================================================================================
outfile ............... None : Output file name. None => file is named
                               automatically; 'SAMP' => send to SAMP hub (e.g.,
                               to TOPCAT, ds9, or Aladin)
clobber .............. False : Overwrite existing file?
img_format ........... 'fits': Format of output image: 'fits' or 'casa'
img_type ....... 'gaus_resid': Type of image to export: 'gaus_resid',
                               'shap_resid', 'rms', 'mean', 'gaus_model',
                               'shap_model', 'ch0', 'pi', 'psf_major',
                               'psf_minor', 'psf_pa', 'psf_ratio',
                               'psf_ratio_aper', 'island_mask'
mask_dilation ............ 0 : Number of iterations to use for island-mask
                               dilation. 0 => no dilation
pad_image ............ False : Pad image (with zeros) to original size
\end{verbatim}

Lastly, the positions, fluxes, etc.\ of the fitted Gaussians may be written in a number of formats to a file using the {\tt write\_catalog} task:
\begin{verbatim}
BDSM [1]: inp write_catalog
--------> inp(write_catalog)
WRITE_CATALOG: Write the Gaussian, source, or shapelet list to a file.
================================================================================
outfile ............... None : Output file name. None => file is named
                               automatically; 'SAMP' => send to SAMP hub (e.g.,
                               to TOPCAT, ds9, or Aladin)
bbs_patches ........... None : For BBS format, type of patch to use: None => no
                               patches. 'single' => all Gaussians in one patch.
                               'gaussian' => each Gaussian gets its own patch.
                               'source' => all Gaussians belonging to a single
                               source are grouped into one patch. 'mask' => use
                               mask file specified by bbs_patches_mask
bbs_patches_mask ...... None : Name of the mask file (of same size as input
                               image) that defines the patches if bbs_patches =
                               'mask'
catalog_type .......... 'srl': Type of catalog to write:  'gaul' - Gaussian
                               list, 'srl' - source list (formed by grouping
                               Gaussians), 'shap' - shapelet list (FITS format
                               only)
clobber .............. False : Overwrite existing file?
correct_proj .......... True : Correct source parameters for image projection
                               (BBS format only)?
format ............... 'fits': Format of output catalog: 'bbs', 'ds9', 'fits',
                               'star', 'kvis', 'ascii', 'csv', 'casabox', or
                               'sagecal'
incl_chan ............ False : Include flux densities from each channel (if
                               any)?
incl_empty ........... False : Include islands without any valid Gaussians
                               (source list only)?
srcroot ............... None : Root name for entries in the output catalog (BBS
                               format only). None => use image file name
\end{verbatim}
The information included in the output varies depending on the format used: with the ASCII and FITS formats, all Gaussian or source parameters are included; with the other formats, only a subset of parameters are included. See the PyBDSM manual (\href{http://tinyurl.com/PyBDSM-doc}{{\tt  http://tinyurl.com/PyBDSM-doc}}) for details. Additionally, in the BBS output file, sources with sizes smaller than the beam are denoted as point sources.

Upon the successful completion of any task, all parameters are saved to the file ``pybdsm.last'' in the current directory and may be reloaded using the command {\tt tget}. The current parameters may also be saved to ``pybdsm.last'' at any time using {\tt tput}. If a file name is given to the {\tt tput} or {\tt tget} commands (e.g., {\tt tput '3C196.sav'}), the parameters are saved to or loaded from the given file.

%----------------------------------------------------------------------------------------------------------------------------------------------------
\subsubsection{Examples}

This section gives examples of using PyBDSM on the following: an image that contains only fairly simple sources and no strong artifacts, an image with strong artifacts around bright sources, and an image with complex diffuse emission. It is recommended that interactive mode (enabled with {\tt interactive=True}) be used for initial runs on a new image, as this allows the user to check the background mean and rms images and the islands found by PyBDSM before proceeding to fitting. Also, if a very large image is being fit, it is often helpful to run on a smaller (but still representative) portion of the image (defined using the {\tt trim\_box} parameter) to verify that the chosen parameters are appropriate before fitting the entire image.

\paragraph{Simple Example}

A simple example of using PyBDSM on a LOFAR image (an HBA image of 3C61.1) is shown below. In this case, default values are used for all parameters. Generally, the default values work well on images that contain relatively simple sources with no strong artifacts.
\begin{verbatim}
BDSM [1]: inp process_image
BDSM [2]: filename = 'sb48.fits'
BDSM [3]: go
--------> go()
--> Opened 'sb48.fits'
Image size .............................. : (256, 256) pixels
Number of channels ...................... : 1
Beam shape (major, minor, pos angle) .... : (0.002916, 0.002654, -173.36) degrees
Frequency of averaged image ............. : 146.497 MHz
Blank pixels in the image ............... : 0 (0.0%)
Flux from sum of (non-blank) pixels ..... : 29.565 Jy
Derived rms_box (box size, step size) ... : (61, 20) pixels
--> Variation in rms image significant
--> Using 2D map for background rms
--> Variation in mean image significant
--> Using 2D map for background mean
Min/max values of background rms map .... : (0.05358, 0.25376) Jy/beam
Min/max values of background mean map ... : (-0.03656, 0.06190) Jy/beam
--> Expected 5-sigma-clipped false detection rate < fdr_ratio
--> Using sigma-clipping thresholding
Number of islands found ................. : 4
Fitting islands with Gaussians .......... : [====] 4/4
Total number of Gaussians fit to image .. : 12
Total flux in model ..................... : 27.336 Jy
Number of sources formed from Gaussians   : 6

BDSM [4]: show_fit
--------> show_fit()
========================================================================
NOTE -- With the mouse pointer in plot window:
  Press "i" ........ : Get integrated fluxes and mean rms values
                       for the visible portion of the image
  Press "m" ........ : Change min and max scaling values
  Press "0" ........ : Reset scaling to default
  Click Gaussian ... : Print Gaussian and source IDs (zoom_rect mode,
                       toggled with the "zoom" button and indicated in
                       the lower right corner, must be off)
________________________________________________________________________

\end{verbatim}
The figure made by {\tt show\_fit} is shown in Figure~\ref{F:example}. In the plot window, one can zoom in, save the plot to a file, etc. The list of best-fit Gaussians found by PyBDSM may be written to a file for use in other programs, such as TOPCAT or BBS, as follows:
\begin{verbatim}
BDSM [5]: write_catalog
--------> write_catalog()
--> Wrote FITS file 'sb48.pybdsm.srl.fits'
\end{verbatim}
The output Gaussian or source list contains source positions, fluxes, etc. BBS patches are also supported.

\begin{figure}
\begin{center}
 \hspace{-1.5cm}\includegraphics[scale=0.6]{example.pdf}
 \caption{Output of {\tt show\_fit}, showing the original image with and without sources, the model image, and the residual (original minus model) image. Boundaries of the islands of emission found by PyBDSM are shown in light blue. The fitted Gaussians are shown for each island as ellipses (the sizes of which correspond to the FWHMs of the Gaussians). Gaussians that have been grouped together into a source are shown with the same color. For example, the two red Gaussians of island \#1 have been grouped together into one source, and the nine Gaussians of island \#0 have been grouped into 4 separate sources. The user can obtain information about a Gaussian by clicking on it. Additionally, with the mouse inside the plot window, the display scaling can be modified by pressing the ``m'' key, and information about the image flux, model flux, and rms can be obtained by pressing the ``i'' key.\label{F:example}}
\end{center}
\end{figure}

%----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

\paragraph{Image with Artifacts}

Occasionally, an analysis run with the default parameters does not produce good results. For example, if there are significant deconvolution artifacts in the image, the {\tt thresh\_isl}, {\tt thresh\_pix}, or {\tt rms\_box} parameters might need to be changed to prevent PyBDSM from fitting Gaussians to such artifacts. An example of running PyBDSM with the default parameters on such an image is shown in Figure~\ref{F:art_def}. It is clear that a number of spurious sources are being detected. Simply raising the threshold for island detection (using the {\tt thresh\_pix} parameter) would remove these sources but would also remove many real but faint sources in regions of low rms. Instead, by setting the {\tt rms\_box} parameter to better match the typical scale over which the artifacts vary significantly, one obtains much better results. In this example, the scale of the regions affected by artifacts is approximately 20 pixels, whereas PyBDSM used a {\tt rms\_box} of 63 pixels when run with the default parameters, resulting in an rms map that is over-smoothed. Therefore, one should set {\tt rms\_box=(20,10)} so that the rms map is computed using a box of 20 pixels in size with a step size of 10 pixels (i.e., the box is moved across the image in 10-pixel steps). See Figure~\ref{F:art_alt} for a summary of the results of this call.

\begin{figure}
 \begin{center}
 \begin{tabular}{l}
  \includegraphics[scale=0.5]{art_fit_def.pdf} \\
  \includegraphics[scale=0.73]{art_rms_def.pdf} \\
\end{tabular}
 \caption{Example fit with default parameters of an image with strong artifacts around bright sources. A number of artifacts near the bright sources are picked up as sources. The background rms map for the same region (produced using {\tt show\_fit}) is shown in the lower panel: the rms varies fairly slowly across the image, whereas ideally it would increase more strongly near the bright sources (reflecting the increased rms in those regions due to the artifacts).\label{F:art_def}}
 \end{center}
\end{figure}

\begin{figure}
 \begin{center}
 \begin{tabular}{l}
  \includegraphics[scale=0.5]{art_fit_alt.pdf} \\
  \includegraphics[scale=0.73]{art_rms_alt.pdf} \\
\end{tabular}
 \caption{The same as Figure~\ref{F:art_def}, but with {\tt rms\_box=(20,10)}. The rms map now varies on scales similar to that of the regions affected by the artifacts, and both bright and faint sources are recovered properly.\label{F:art_alt}}
 \end{center}
\end{figure}

%---------------------------------------------------------------------------------------------------------------------------------------------

\paragraph{Image with Extended Emission}

If there is extended emission that fills a significant portion of the image, the background rms map will likely be biased high in regions where extended emission is present, affecting the island determination (this can be checked during a run by setting {\tt interactive=True}). Setting {\tt rms\_map=False} and {\tt mean\_map='const'} or {\tt 'zero'} will force PyBDSM to use a constant mean and rms value across the whole image. Additionally, setting {\tt atrous\_do=True} will fit Gaussians of various scales to the residual image to recover extended emission missed in the standard fitting. Depending on the source structure, the {\tt thresh\_isl} and {\tt thresh\_pix} parameters may also have to be adjusted as well to ensure that PyBDSM finds and fits islands of emission properly. An example analysis of an image with significant extended emission is shown in Figure~\ref{F:a2256}.

\begin{figure}
\begin{center}
 \hspace{-1.5cm}\includegraphics[scale=0.4]{HydraA_74MHz_fit.pdf}
 \caption{Example fit of an image of Hydra A with {\tt rms\_map=False}, {\tt mean\_map='zero'}, and {\tt atrous\_do=True}. The values of {\tt thresh\_isl} and {\tt thresh\_pix} were adjusted before fitting (by setting {\tt interactive=True}) to obtain an island that enclosed all significant emission.\label{F:a2256}}
\end{center}
\end{figure}

%---------------------------------------------------------------------------------------------------------------------------------------------

\subsubsection{Usage in Python scripts}\label{S:python}

PyBDSM may also be used non-interactively in Python scripts (for example, to automate source detection in a large number of images for which the optimal analysis parameters are known). To use PyBDSM in a Python script, import it by calling {\tt from lofar import bdsm} inside your script. Processing may then be done using {\tt bdsm.process\_image()} as follows:
\begin{verbatim}
img = bdsm.process_image(filename, <args>)
\end{verbatim}
where {\tt filename} is the name of the image (in FITS or CASA format) or PyBDSM parameter save file and {\tt <args>} is a comma-separated list of arguments defined as in the interactive environment (e.g., {\tt beam = (0.033, 0.033, 0.0), rms\_map=False}). If the fit is successful, PyBDSM will return an ``Image'' object (in this example named ``img'') which contains the results of the fit (among many other things). The same tasks used in the interactive PyBDSM shell are available for examining the fit and writing out the source list, residual image, etc. These tasks are methods of the Image object returned by {\tt bdsm.process\_image()} and are described below:
\begin{description}
\item[{\tt img.show\_fit()}] This method shows a quick summary of the fit by plotting the input image with the islands and Gaussians found, along with the model and residual images.
\item[{\tt img.export\_image()}] Write an internally derived image (e.g., the model image) to a FITS file.
\item[{\tt img.write\_catalog()}] This method writes the Gaussian or source list to a file.
\end{description}
The input parameters to each of these tasks are the same as those available in the interactive shell. See the PyBDSM documentation (\href{http://tinyurl.com/PyBDSM-doc}{{\tt  http://tinyurl.com/PyBDSM-doc}}) for more details and scripting examples.


\subsection{Sky model manipulation: LSMTool}
\subsubsection{Introduction}
LSMTool is a Python package which allows for the manipulation of sky models in the \texttt{makesourcedb} format (used by BBS). Such models  include those output by PyBDSM, those made by {\tt gsm.py}, and CASA clean-component models (after running {\tt casapy2bbs.py}).

\subsubsection{Setup}
To initialize your environment for LSMTool, users on CEP2 and CEP3 should run the following commands:
\begin{verbatim}
use LofIm
source ~rafferty/init_lsmtool
\end{verbatim}

%-----------------------------------------------------------
\subsection{Basic Usage}
\label{lsmtool:usage}

The command-line version of LSMTool can be run as follows:
\begin{verbatim}
Usage: lsmtool <skymodel> <parset> [<beam MS>]
Options:
  --version   show program's version number and exit
  -h, --help  show this help message and exit
  -q          Quiet
  -v          Verbose
\end{verbatim}
The parset specifies the operations to perform and their parameters. These are described in the next sections.

%-----------------------------------------------------------
\subsection{Operations}
\label{lsmtool:operations}

These are the operations that LSMTool can perform:
\begin{description}
 \item[SELECT]: Select sources by source or patch properties
 \item[REMOVE]: Remove sources by source or patch properties
 \item[TRANSFER]: Transfer a patch scheme from one sky model to another
 \item[GROUP]: Group sources into patches
 \item[UNGROUP]: Remove patches
 \item[MOVE]: Move a source or patch position
 \item[MERGE]: Merge two or more patches into one
 \item[CONCATENATE]: Concatenate two sky models
 \item[ADD]: Add a source
 \item[SETPATCHPOSITIONS]: Calculate and set patch positions
 \item[PLOT]: Plot a simple representation of the sky model
 \item[COMPARE]: Compare source fluxes and positions of two sky models
\end{description}


\subsection{The Parset File}
\label{lsmtool:parset}
As with NDPPP and LoSoTo, LSMTool can be run with a parset that specifies the operations to perform and their parameters.

Below is an example parset that filters on the flux, adds a source, and then groups the sources into patches:
\begin{verbatim}
LSMTool.Steps = [selectbright, addsrc, grp, setpos]

# Select only sources above 1 mJy
LSMTool.Steps.selectbright.Operation = SELECT
LSMTool.Steps.selectbright.FilterExpression = I > 1.0 mJy

# Add a source
LSMTool.Steps.addsrc.Operation = ADD
LSMTool.Steps.addsrc.Name = new_source
LSMTool.Steps.addsrc.Type = POINT
LSMTool.Steps.addsrc.Ra = 277.4232
LSMTool.Steps.addsrc.Dec = 48.3689
LSMTool.Steps.addsrc.I = 0.69

# Group using tessellation to a target flux of 50 Jy
LSMTool.Steps.grp.Operation = GROUP
LSMTool.Steps.grp.Algorithm = tessellate
LSMTool.Steps.grp.TargetFlux = 50.0 Jy
LSMTool.Steps.grp.Method = mid

# Set the patch positions to their midpoint and write final skymodel
LSMTool.Steps.setpos.Operation = SETPATCHPOSITIONS
LSMTool.Steps.setpos.Method = mid
LSMTool.Steps.setpos.OutFile = grouped.sky
\end{verbatim}

In the first line of this parset the step names are defined. Steps are applied sequentially, in the same order defined in the list of steps. A list of step-specific parameters is given in Table~\ref{lsmtool:tab:local_val}.

\begin{table}[!ht]
\centering
\tiny
\begin{tabular}{l l l l}
\hline
\hline
Var Name & Format & Example & Comment\\
\hline
Operation & string & SELECT & An operation among those defined in Sec.~\ref{lsmtool:operations}\\
OutFile & string & out\_sky\_model.sky & Name of output file \\
\hline
\multicolumn{4}{l}{\textbf{SELECT and REMOVE}}\\
FilterExpression & string & I $> 10.0$ Jy & Filter for selection\\
Aggregate & bool & False & Filter by aggregated patch property\\
ApplyBeam & bool & True & If true, apparent fluxes will be used \\
\hline
\multicolumn{4}{l}{\textbf{TRANSFER}}\\
PatchFile & string & sky\_model\_with\_patches.sky & File with patches that will be transferred\\
\hline
\multicolumn{4}{l}{\textbf{GROUP}}\\
Algorithm & string & tessellate & One of tessellate, cluster, single, every, or a CASA mask filename\\
TargetFlux & string & 10.0 Jy & Target total flux of patches (tessellate only)\\
NumClusters & int & 100 & Number of clusters (cluster only)\\
Threshold & float & 0.1 & Island threshold from 0 to 1 (threshold only)\\
FWHM & string & 60 arcsec & FWHM of Gaussian for smoothing before thresholding (threshold only)\\
ApplyBeam & bool & True & If true, apparent fluxes will be used \\
\hline
\multicolumn{4}{l}{\textbf{UNGROUP}}\\
\hline
\multicolumn{4}{l}{\textbf{MOVE}}\\
Name & string & src1 & Name of source or patch to move.\\
Position & list of floats & [12.3, 23.4] & RA and Dec in degrees to move to\\
Shift & list of floats & [0.001, 0.0] & RA and Dec in degrees to shift by\\
\hline
\multicolumn{4}{l}{\textbf{MERGE}}\\
Patches & list of strings & [bin1, bin2, bin3] & Patch names to merge\\
Name & string & merged\_patch & Name of new merged patch\\
\hline
\multicolumn{4}{l}{\textbf{SETPATCHPOSITIONS}}\\
Method & string & mid & Set patch positions to mid, mean, or wmean positions\\
\hline
\multicolumn{4}{l}{\textbf{CONCATENATE}}\\
Skymodel2 & string & in\_sky\_model2.sky & Name of second sky model to concatenate\\
MatchBy & string & position & Identify duplicates by position or name\\
Radius & string & 30 arcsec & Radius within which matches are identified\\
Keep & string & all & If two sources match, keep: all, from1, or from2\\
InheritPatches & bool & False & Matches inherit patches from parent sky model\\
\hline
\multicolumn{4}{l}{\textbf{ADD}}\\
Name & string & src1 & Name of source; required\\
Type & string & POINT & Type; required\\
Patch & string & new\_patch & Patch name; required if sky model has patches\\
RA & float or string & 12:45:30.4 & RA; required\\
Dec & float or string & +76.45.02.48 & Dec; required\\
I & float & 0.69 & Flux in Jy; required\\
AnyValidColumnName & & value & Any valid column name can be specified\\
\hline
\multicolumn{4}{l}{\textbf{PLOT}}\\
LabelBy & string & patch & Label points by: source or patch\\
\hline
\multicolumn{4}{l}{\textbf{COMPARE}}\\
OutDir & string & comparison\_plots/ & Output directory for plots\\
SkyModel2 & string & in\_sky\_model2.sky & Name of second sky model\\
Radius & string & 10 arcsec & Radius within which matches are identified\\
LabelBy & string & patch & Label plot points by source or patch\\
ExcludeMultiple & bool & True & Exclude sources with multiple matches\\
IgnoreSpec & float & -0.7 & Ignore any source in SkyModel2 with this spectral index\\
\end{tabular}
\caption{Definition of variables in the LSMTool parset. \label{lsmtool:tab:local_val}}
\end{table}

%-----------------------------------------------------------
\subsection{Interactive use and scripting}
\label{lsmtool:scripting}
LSMTool can also be used interactively (in IPython, for example) or in Python scripts without the need for a parset. To use LSMTool in a Python script or interpreter, import it as follows:
\begin{verbatim}
>>> import lsmtool
\end{verbatim}
A sky model can then be loaded with, e.g.:
\begin{verbatim}
>>> LSM = lsmtool.load('skymodel.sky')
\end{verbatim}
All of the operations described in Section~\ref{lsmtool:operations} are available as methods of the resulting sky model object (with the same name as the corresponding operation). For example, the following commands which duplicate the steps done in the example parset given in Section~\ref{lsmtool:parset}:
\begin{verbatim}
>>> LSM.select('I > 1.0 mJy')
>>> LSM.add({'Name':'new_source', 'Type':'POINT', 'Ra':277.4232, 'Dec':48.3689,
             'I':0.69})
>>> LSM.group(algorithm='tesselate', targetFlux='10.0 Jy')
>>> LSM.setPatchPositions(method='mid')
\end{verbatim}
In many cases, the methods accept parameters with the same names as those used in a parset (see the source-code documentation for details). The sky model can then written to a new file with:
\begin{verbatim}
>>> LSM.write('grouped.sky')
\end{verbatim}
Additionally, sky models can be written out as ds9 region files and kvis annotation files (as well as all the formats supported by the astropy.table package, such at VOTable, HDF5, and FITS):
\begin{verbatim}
>>> LSM.write('outskymodel.reg', format='ds9')
>>> LSM.write('outskymodel.ann', format='kvis')
>>> LSM.write('outskymodel.fits', format='fits')
>>> LSM.write('outskymodel.hdf5', format='hdf5')
>>> LSM.write('outskymodel.vo', format='votable')
\end{verbatim}

In addition to the operations described above, a number of other methods are available:
\begin{description}
 \item[LSM.copy()]: Return a copy of the sky model object
 \item[LSM.info()]: Print information about the sky model
 \item[LSM.more()]: Print the sky model to the screen, using more-like controls
 \item[LSM.broadcast()]: Send the sky model to other applications using SAMP
 \item[LSM.getColNames()]: Returns a list of the column names in the sky model
 \item[LSM.getColValues()]: Returns a numpy array of column values
 \item[LSM.getRowIndex()]: Returns the row index or indices for a source or patch
 \item[LSM.getRowValues()]: Returns a table or row for a source or patch
 \item[LSM.getPatchPositions()]: Returns patch RA and Dec values
 \item[LSM.getDefaultValues()]: Returns column default values
 \item[LSM.getPatchSizes()]: Returns an array of patch sizes
 \item[LSM.getDistance()]: Returns an array of angular distances to given position
 \item[LSM.setColValues()]: Sets column values
 \item[LSM.setRowValues()]: Sets row values
 \item[LSM.setDefaultValues()]: Sets default column values
\end{description}
For details on these methods, please see the source-code documentation.
