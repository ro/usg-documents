\svnInfo $Id$

\section[Imaging with MWImager]{Imaging with MWImager\footnote{This section is maintained by Bas van der Tol ({\tt vdtol[at]strw[dot]leidenuniv[dot]nl}) and Reinout van Weeren ({\tt rvweeren[at]strw[dot]leidenuniv[dot]nl}).}}

\label{sec:MWimager}

After calibration, an image can be created using {\tt MWimager}\footnote{Master-worker imager, where master refers to the front end, and worker to the compute nodes.}. It is worth noting that MWImager is still under development and the imaging of LOFAR data could also be done using  CASA (task CLEAN). However, we encourage the users to use MWimager and get in contact with the software developers\footnote{Ger van Diepen, diepen[at]astron[dot]nl} to help the improvement. 

MWimager is a wrapper around underlying programs doing the imaging. These are:
\begin{itemize}
	\item cimager: a new imager developed at ATNF for the ASKAP observations.
   	\item lwimager: a standalone version of the CASA imager.
\end{itemize}

MWimager forms an image cube from the visibility data in a LOFAR dataset. Similar to NDPPP and BBS, it operates in a distributed way using the so-called {\tt .gds} file.\\


\subsection{Running MWimager}

As mentioned before, MWimager acts as a front end to two imagers: by default, it uses the ASKAP imager (the actual program is \texttt{cimager}), developed by the ASKAP people for both LOFAR and ASKAP. This is the imager intended to be used in the LOFAR imaging pipeline, and should be tested most often. As a fallback option, there is the CASA imager (\texttt{lwimager}), which is similar to the interactive imager in casapy, but runs standalone. 


Both imagers are capable of the following:

\begin{itemize}

  \item  imaging from the DATA, MODEL\_DATA, or CORRECTED\_DATA column

  \item  imaging individual channels

  \item  multi-frequency synthesis (N visibility channels to M image channels)

  \item  imaging of all Stokes

  \item wide-field imaging using facets or W-projection (or combination)

  \item deconvolution (though not all clean algorithms are supported by both imagers).

\end{itemize}

Furthermore MWimager is capable of applying direction-dependent
corrections (beam, ionosphere).

MWimager requires the same cluster description file (e.g. {\tt cep1.clusterdesc}\footnote{It can be copied from {\tt /globaldata/COOKBOOK/Files}}) as BBS, a
parameter set file ({\tt .parset}) to tell it how to operate (see Sect.~\ref{mwimager:theparsetfile}).
Moreover, the file called \texttt{.casarc}\footnote{You can copy this file from \texttt{/home/rol/.casarc}} is also required in the home
directory. It points to various calibration tables, necessary
to run the imager.\\
Once you have copied and adjusted the parameters in your parset file {\tt MWimager.parset} (see section below), run the following commands to create an image:
\begin{verbatim}

> mwimager mwimager.parset imaging.clusterdesc
\end{verbatim}

If you want to run the CASA imager, use the \texttt{-casa} option:
\begin{verbatim}
> mwimager -casa mwimager.parset imaging.clusterdesc
\end{verbatim}

The imager can take 10 seconds to several hours, depending on the size
of the image you are trying to make. This is because it needs to
correct for the w-plane, which becomes increasingly more
computationally intensive for larger images. Therefore, always try and start
with a small image.

Some parameters to create a relatively quick, small, and crude image
(see e.g. Fig.~\ref{fig:image}) are given below.


%---------------------------------------------------------------------
\subsection{Parset files}
\label{mwimager:theparsetfile}

Below is an example parset for the MWimager\footnote{An example of this parset file can be found in  /globaldata/COOKBOOK/Parset}. 


\begin{verbatim}
############################################################
# mwimager.parset 1 

Images.stokes = [I]
Images.shape = [512, 512]
Images.cellSize = [20, 20]
Images.ra = 08h13m36.06
Images.dec = 48.13.02.25
Images.directionType = J2000
Solver.type = Dirty
Solver.algorithm = Hogbom
Solver.niter = 100
Solver.gain = 0.2
Solver.verbose = True
Solver.scales = [0, 3]
Gridder.type = WProject
Gridder.wmax = 10000
Gridder.nwplanes = 257
Gridder.oversample = 1
Gridder.maxsupport = 40
Gridder.limitsupport = 0
Gridder.cutoff = 0.001
Gridder.nfacets = 1
Gridder.nchan = 1
Gridder.padding = 1
dataset = <file name>.gds
datacolumn = CORRECTED_DATA
minUV = 1.0
ncycles = 0
restore = True
restore_beam = [0.003, 0.003, 0]
\end{verbatim}
This creates a Stokes I (total intensity) image, $512\times512$ pixel in size, with every pixel sampling 20~arcsec.  The image size needs to be kept
reasonably small in order for imaging to only take a short time.  The
location of the centre of the image needs setting specifically (to be
the position of your target source).  As the parameters are set above,
no cleaning is performed and clean components are therefore not
restored.  

If you are using international baselines, the increased baseline length
will require you to increase the values of {\tt wmax} and {\tt maxsupport} (and
the processing time will increase accordingly), or you'll get an error
when running the imager. Set these values to e.g. \texttt{wmax = 50000}
and \texttt{maxsupport = 1024}, and try again.

Some further notes:

\begin{itemize}

\item[nwplanes] The number of nwplanes has to be odd, While the ASKAP imager will work with an even number of images, the CASA imager requires an odd number of w-planes.

\item[restore] Set this to {\tt true}, even for a dirty image. This
  appears to be some oddity in the ASKAP imager: when \texttt{restore = False}, you will end up with an image with only zeros in it.

\item[ncycles] When trying to clean the image, do not forget to set this value to 1 or higher: this is the number of major clean cycles (compared to \texttt{niter} for the number of minor clean cycles.) Note that the CASA imager ignores \texttt{ncycles}, but the ASKAP imager does not.

\item[output files] Both the ASKAP and CASA imager will produce a number of output files (unfortunately with different naming conventions, though this will avoid overriding files). These files contain e.g. the clean model, the PSF model used in cleaning, a weights image, a dirty image, and a restored image.

\end{itemize}


Imaging can take up a lot of memory, especially when creating a large
image. It is therefore possible for the processing cluster to run out
of memory if multiple subbands are imaged at the same time, on the
same machine. Be sure to keep an eye out on this.  Either use
\texttt{top} to interactively trace the memory usage, or use a command
like \texttt{ps xo "\%cpu,\%mem,user,args" | grep -E
  '($\backslash$-$\backslash$-$\backslash$- lce|cimager
  $\backslash$-inputs)'} for a quick look.

If {\tt MWimager} is killed, {\tt cimager} could be still running on
one or more compute nodes, consuming memory and CPU power. Check if
this is the case and also kill any {\tt cimager} job before re-running
{\tt MWimager} again; e.g. \texttt{killall cimager}.

\begin{figure}[ht!]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{SB0_DirtyImage.pdf}
    \caption[3C196 Image.]{A simple image of 3C196 using the
    prescription above.}  
    \label{fig:image}
\end{center}
\end{figure}

\subsection{Cleaning}
\label{subsec:imaging-cleaning}

Deconvolution of an image has not yet been extensively tested. The essential parameters are the (obvious) following ones (others as above, or whatever preferred):

\begin{verbatim}
############################################################
# mwimager.parset 1 

Solver.type = Clean
Solver.algorithm = Hogbom
Solver.niter = 100
Solver.gain = 0.2
Solver.verbose = True
Solver.scales = [0, 3]
ncycles = 1
restore = True
restore_beam = [0.003, 0.003, 0]
\end{verbatim}

Algorithms that can be used are Hogbom and Multiscale (note: case-sensitive!). For the latter, you will need to set the scales using \texttt{Solver.scales}. The default is \texttt{[0, 3, 10]}.

The restoring beam has to be specified manually. The scale is in degrees (although one can append \texttt{arcsec} after the value), so the above example has a circular restoring beam of 10 arcseconds radius and a position angle of 0.

Do not forget to set \texttt{ncycles} to at least 1: this is the number of major cycles.

The cleaning will clean the entire image, or rather, the inner quarter. If you want to clean more, increase the image size or use the {\tt padding} parameter; you likely will have to increase the {\tt maxsupport} parameter (and memory and CPU usage will go up).


\subsection{Facetting}
\label{subsec:imaging-facetting}

When creating a large image, memory consumption may become an issue. To avoid this, one can use facetting. This will, however, increase the computation time by a large amount, so is only recommended when really needed.

To use facetting, set the image shape to the \emph{total} image size, and specify the number of facets: 


\begin{verbatim}
############################################################
# mwimager.parset 1 

Images.nfacets = 11
Images.shape = [2816, 2816]
\end{verbatim}

The above will produce an image using 11 by 11 facets (so 121 total),
each facet having a size of 256 pixels on a side (note, however, that a
2816 x 2816 pixel image can still be easily created using w-projection
without facetting).

If you are using facetting, you probably have to make sure the cleaning
process is used correctly: this communicates across
the facets, so sidelobes get cleaned. But since only the inner section
of a facet gets cleaned, setting the {\tt padding} parameter to 2 or more
will be essential.

Once you have started off the imager for your large image, have a check
that things do not run out of memory, and then you will probably want to
do something else: if it is a really big image (in pixel size), it may
take several hours (be sure to check occasionally if things did not
stall).

\subsection{Process information from the imager}

The cimager can produce a lot of output. You may not want to see all
of this on your screen, but still want to have a logging file which
has all the details, to browse through after the imaging has
finished. The cimager can therefore use a logging configuration file,
which sets up the level of output and where the output needs to go (e.g.
file and/or stdout). An example file,
\texttt{/home/rol/askap.log\_cfg}, can be used. This file will need to
reside in the directory where the cimager process is run, so copy it
to your working directory if you would like to use it. Modify it to your
needs (hopefully the syntax is relatively obvious).

% %-----------------------------------------------------------
% \subsection{Averaging subband images together}
% There is a script, {\tt average.py}, which permits you to average
% images from multiple subbands together.  You need to edit {\tt
% average.py} first, entering the names of the image files that you want
% to average (in either {\tt .fits} or {\tt .img} format).  The script
% produces error messages, but should work.
% 
% \begin{verbatim}
% > source /opt/scripts/doPyrap
% > python average.py
% \end{verbatim}
 
%-----------------------------------------------------------
\subsection{Displaying the output image}
\label{displayingtheoutputimage}

There are two simple ways to display the output image: (i) in CASA; (ii)
converting it to FITS and using the viewer of your choice (e.g. {\tt kvis} or {\tt ds9}, see below).

\begin{itemize}
\item To display the image in CASA, type:
\begin{verbatim}
> use Casa
> casaviewer
\end{verbatim}
Select the file, and click Raster Image (on the right-hand panel) to
display the image. An image can also be opened directly from the command line 
\begin{verbatim}
> casaviewer <imagename>
\end{verbatim}

\item To convert the image to FITS:
\begin{verbatim}
> image2fits in=<input image> out=<output image>
\end{verbatim}
where {\tt <input image>} is the output from {\tt MWimager}, likely to
be in the format {\tt image.i.<input MS name>\_Dirty}, and {\tt
  <output image>} is your desired FITS file name.

Once you have a FITS image, you can display it using the Karma\footnote{\href{http://www.atnf.csiro.au/computing/software/karma/}{\tt http://www.atnf.csiro.au/computing/software/karma/}} tool {\tt kvis}:

\begin{verbatim}
>use Karma
>kvis
\end{verbatim}

You could also use {\tt ds9} to view FITS images. This viewer is installed in the default path
so there is no need to first initialize a package:
\begin{verbatim}
>ds9 <imagename>
\end{verbatim}
\end{itemize}

%-----------------------------------------------------------
\subsection{Identifying background source positions}
\begin{figure}[ht!]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{17subband.pdf}
    \caption[3C196 Subtracted Image.]{This is a 17 subband, 5
    station image made of the 3C196 field (3C196 subtracted out)
    during Busy Week 1. Background sources are clearly
    apparent. Credit George Heald.}
    \label{fig:17subband}
  \end{center}
\end{figure}

The following steps allow you to compare the positions of background
sources in an image with their correct locations (taken from e.g.\
VLSS, WENSS).  To see the sources, either need cleaned image or you need to create an image with
the main target (in our case 3C196) already subtracted out (e.g.\
Fig.~\ref{fig:17subband}).
\begin{itemize}
\item Convert the image to {\tt .fits} format
\item Open it in {\tt ds9}
\item Analysis/Catalogs/Search for Catalogues
\item Enter the desired survey in the Name or Designation box and
 click Retrieve.
\item Select the survey from the list \& click Catalog.
\end{itemize}

Check carefully if the source positions agree. The software is still in development.
Previously a few bugs in the software have been found by inspection of source positions.

%-----------------------------------------------------------
\subsection{Running MWimager in a distributed fashion}
\label{mwimager:distributedfashion}

MWimager can be run in a distributed fashion on the cluster nodes in a
similar way to BBS. Use the {\tt .gds} file as the input in the
parset file.

%-----------------------------------------------------------
\subsection{Imager troubleshooting}
\label{sec:imager-troubleshooting}

(errors are broken across lines, indicated with a backslash)

\begin{itemize}

\item When troubleshooting the first step should be to check what version
of the software you are running. The command {\tt which} can be used to find out
which {\tt mwimager} you are using
\begin{verbatim}
>which mwimager
/opt/cep/LofIm/daily/Tue/lofar/install/gnu_opt/bin/mwimager
\end{verbatim}
In this example Tuesdays daily build was used.
If the {\tt which} command shows a path to some obsolete version of mwimager, you should initialize the
environment correctly with \texttt{use LofIm}.
When you are using the daily build and things that worked fine one day and seem suddenly to be broken the next day,
then you could try an earlier build. For example if you used the mwimager successfully with some parset on Monday  
and on Tuesday you use the same parset and mwimager fails, then you could initialize with \texttt{use LofIm Mon} and
try again.

\item An error along the lines of:
\begin{verbatim}
MeasIERS::fillMeas(MeasIERS::Files, Double) (file measures/Measures\
/MeasIERS.cc, line 309): Requested data table TAI_UTC cannot be found\
 in the searched directories:
./
./data/
/home/rol/aips++/data//ephemerides/
MeasTable::dUTC(Double) (file measures/Measures/MeasTable.cc,\
 line 6295): Cannot read leap second table TAI_UTC
Unused gridder
Unexpected exception in /opt/cep/LofIm/daily/Mon/askapsoft/bin/cimager:\
 2010-04-05 22:01:20	SEVERE	MeasTable::dUTC(Double) (file\
 measures/Measures/MeasTable.cc, line 6295)	Cannot read leap\
 second table TAI_UTC
\end{verbatim}

This indicates a missing \texttt{.casarc} file in your home directory. See instructions at the start of the imaging section.

\item
\begin{verbatim}
Askap error in /opt/cep/LofIm/daily/Mon/askapsoft/bin/cimager:\
 Overflowing convolution function for w-plane 0 - increase\
 maxSupport or decrease overSample; support=5 oversample=1 nx=10\
 ('support*itsOverSample<nx/2' failed) (thrown in gridding/\
WProjectVisGridder.cc:268) 
\end{verbatim}

  You will need to increase the maxsupport parameter, or decrease the
  over-sample parameter. Since this error shows up pretty early in the
  process, you can increase maxsupport by trial by
  steps of 2, not by 10 (the imager will run when increasing by a
  factor of 10, but may be much slower than a successful run after an
  increase of only 2).

\item
\begin{verbatim}
Askap error in /opt/cep/LofIm/daily/Mon/askapsoft/bin/cimager: W scaling\
 error: recommend allowing larger range of w, you have w=-21.1118\
 wavelengths ('result > -1' failed) (thrown in gridding/\
WDependentGridderBase.cc:88) 
\end{verbatim}

  This error probably occurs if you are using data with long baselines
  (e.g., international stations). If your default \texttt{wmax} is
  10000, try increasing to 50000.

\item 
\begin{verbatim}
> /opt/cep/casacore/bin/image2fits in=image out=out.fits
/opt/cep/casacore/bin/image2fits: error while loading shared libraries: 
libcasa_lattices.so: cannot open shared object file: No such file 
or directory
\end{verbatim}
Solution: your environment is incorrectly set. Use either \texttt{use LofIm} or use
the script at \texttt{/home/rol/sw/bin/image2fits} instead (note: the latter doesn't require the \texttt{in=} and \texttt{out=} keywords).

\item
\begin{verbatim}
> /opt/cep/casacore/bin/image2fits in= image-name out=out.fits
> Input::Put: parameter image.<image name>_Dirty is unknown
\end{verbatim}
Solution: This command is both space- and case-sensitive, check that
you have entered it correctly.

\item
\begin{verbatim}
> log4cplus:ERROR No appenders could be found for logger 
  (LCS.Common.EXCEPTION)
> log4cplus:ERROR Please initialize the log4cplus system properly
> Unable to open file /home/<user login>/MWimager.parset
\end{verbatim}
Solution: check that you have copied the {\tt mwimager.parset} file to
your working directory.
\end{itemize}
