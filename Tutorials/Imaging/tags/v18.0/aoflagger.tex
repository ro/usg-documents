\svnInfo $Id: dppp.tex 4940 2010-05-25 12:00:09Z pizzo $


\section[The AOFlagger - RFI Console]{The AOFlagger\footnote{The author of this Chapter is Andr\'e Offringa ({\tt andre[dot]offringa[at]anu[dot]edu[dot]au}).}}
\label{aoflagger}


The frequencies covered by LOFAR are considerably affected by RFI, both in the low and the high band (see Fig.~\ref{rfi-spectra-overview} and \ref{rfi-spectra-detail}).  An efficient cleaning of the data is essential to obtain high quality images. 

The AOFlagger (the algorithm executed by "RFI Console") is an independent flagger. It was 
originally written for the Epoch of Reionization key science project, 
which needed a flagger with better accuracy compared to the MADFlagger 
technique implemented in DPPP (Chapter~\ref{sec:DPPP}), but is since then optimized to be 
accurate for any observation and was therefore put inside the LofIm 
environment. Several comparisons have been made between the MADFlagger
and the AOFlagger, and the consensus is that the AOFlagger is more
accurate.


\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.55]{rfi-spectrum-all.pdf}
\caption{The RFI distribution in most of the observable frequencies of LOFAR. It combines data from four observations, whose RFI spectra have been given in more detail in figure~\ref{rfi-spectra-detail}. In most of the frequencies, the RFI situation is benign and cause only a few percent of data loss. Towards 30 MHz and lower, RFI become harder to deal with. The high end of the HBA contain a few strong and broadband digital broadcast transmitters.}
\label{rfi-spectra-overview}
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\subfigure[LBA (6 hour observation)]{ \includegraphics[scale=0.3]{rfi-spectrum-lba.pdf} } \hspace{-0.5cm}
\subfigure[HBA low (24 hour observation)]{ \includegraphics[scale=0.3]{rfi-spectrum-hba-low.pdf} } \hspace{-0.5cm}
\subfigure[HBA mid (1 hour observation)]{ \includegraphics[scale=0.3]{rfi-spectrum-hba-mid.pdf} } \hspace{-0.5cm}
\subfigure[HBA high (6 hour observation)]{ \includegraphics[scale=0.3]{rfi-spectrum-hba-high.pdf} }
\caption{The RFI in the LBA and HBA bands of LOFAR. The HBA has been split into three parts (HBA low, mid and high),  which were observed independently. Towards lower frequencies (below 30 MHz), the data are more affected by interferences. Parts of the HBA high range (around 225 MHz) are contaminated by several broadband digital broadcast transmitters. Please note that it is unknown whether these observations are representative of the RFI distribution in LOFAR bands. Also note that they were partly performed during daytime. }
\label{rfi-spectra-detail}
\end{center}
\end{figure}


Using the AOFlagger to flag the data and DPPP to average them afterwards seems a good strategy and it should provide data clean enough to perform the calibration and the imaging. Recently, the flagger has also been incorporated into DPPP (see Chapter~\ref{sec:DPPP}). If one wants to use the default flagging settings, it is recommended that DPPP is used directly with the AOFlagger option, because it is faster and uses less resources compared to executing the steps separately.  For details on how to run RFIconsole within DPPP, please see Chapter~\ref{sec:DPPP}. In the following sections, the details regarding the AOFlagger as independent routine are summarized.

\subsection{How to run the AOFlagger}
\label{howtoruntheaoflagger}

The AOFlagger runs on a measurement set and updates its flag table. This requires write access to the measurement set, thus it can not be directly run on the raw data on the storage 
nodes. Since the raw measurement sets are written using a special 
read-only storage manager (LofarStMan), the storage manager needs to be 
changed before running the flagger. This can be done by running the 
following command (e.g. for SB0.MS):

\begin{verbatim}
> makeFLAGwritable SB0.MS
\end{verbatim}
which gives the following output
\begin{verbatim}
Successful read/write open of default-locked table SB0.MS: 23 columns, 
91341 rows
Created new FLAG column; copying old values ...
FLAG column now stored with SSM to make it writable
\end{verbatim}

It might take a few moments to rewrite the flag column. After this, your 
measurement set is ready to be flagged by the AOFlagger. The fastest mode
of RFI Console, called the indirect read mode, will rewrite the data set
to a temporary location. This location will be the path from where you start
RFI Console. Therefore, you have to make sure you start RFI Console from a
scratch directory, for example:

\begin{verbatim}
 > mkdir /data/scratch/offringa/temp
 > cd /data/scratch/offringa/temp
\end{verbatim}

Now, you are ready to flag the set:

\begin{verbatim}
offringa@lce032:/data/scratch/offringa/temp$ rficonsole -indirect-read SB0.MS
\end{verbatim}

The program will now run the default algorithm on the measurement set 
and output status messages and progress to the console. Depending on the 
size of the observation, this might take up to several hours (it might 
therefore be appropriate to run it inside a 'screen', as described in Sect.~\ref{sec:gnuscreen}). In almost all cases, this should produce flags which will be good enough for further reduction.

Please note that the current working directory will be used as a temporary storage location! Thus by running RFI Console like above, temporary files will be created in {\tt /data/scratch/offringa/temp} that will take up the amount of space equal to the size of the sub-band (i.e. measurement set). So, do not run this in your home directory but always on the local hd's of the nodes.

Once the flagger has finished, it will print statistics of the flagged data per channel and polarization. E.g.:

\begin{verbatim}
Summary of RFI per channel: (166,700,363 Hz - 166,894,149 Hz)
Channel   1-  8:   7.8%   7.8%   7.7%   7.7%   7.7%   7.7%   7.6%   7.6%
Channel   9- 16:   7.6%   7.6%   7.6%   7.6%   7.6%   7.7%   8.3%   8.7%
[..]
Channel 241-248:   7.5%   7.5%   7.5%   7.5%   7.5%   7.5%   7.6%   7.6%
Channel 249-255:   7.6%   7.6%   7.6%   7.6%   7.6%   7.5%   7.3%
Polarization statistics: XX: 3.3%, XY: 3.3%, YX: 3.3%, YY: 3.4%
\end{verbatim}

After having ran RFI Console, the next step will normally be to use DPPP to average the data. When averaging a measurement set that has been flagged by 
the AOFlagger, your DPPP parset should not have any flagging steps in it.

\subsection{Advanced settings with RFI Console}
\label{advancedsettingswithrficonsole}

In this chapter, some techniques will be described which might be helpful when the default
strategy is not good enough or when you would like to analyze the RFI and/or
the flag results more closely.

\subsubsection{Visualizing RFI and flags}
A useful tool to analyze the RFI and flags in a measurement set is the ``RFI Gui''. To
start it, make sure you have your X forwarded and start it with:
\begin{verbatim}
> rfigui
\end{verbatim}

In the RFI Gui, there are options to alter flagging parameters, compare flags of
different strategies, image individual baselines and several plotting options.
For further explanation, refer to the RFI Gui tutorial and the references therein,
which can be found on the following address:
\begin{center}
\href{http://www.astro.rug.nl/rfi-software/gui-tutorial.html}{http://www.astro.rug.nl/rfi-software/gui-tutorial.html}
\end{center}

\subsubsection{Changing flagging parameters}
If you would like to change the flagger settings outside of the RFI Gui, it is 
possible to change RFI Console's strategy by creating a configuration file and 
change the flagger's settings in it. 

You can create such a file with:
\begin{verbatim}
> rfistrategy default mystrategy.rfis
\end{verbatim}

This will create a file named {\tt mystrategy.rfis}. Settings can be changed 
in two ways, either by:
\begin{itemize}
\item adding parameters to the {\tt rfistrategy} executable. Run {\tt rfistrategy} 
without parameters to get a list of options;
\item or manually changing the file and altering the parameters. The strategy file is 
an xml text file, and can be edited by hand with any text editor such as nano or emacs.
\end{itemize}

Once you have completed the alternative strategy, you can run it with:

\begin{verbatim}
> rficonsole -strategy mystrategy.rfis SB0.MS
\end{verbatim}

\subsubsection{RFI Console's parameters}
The RFI Console program can also take additional parameters. These can be 
retrieved by running the RFI Console program without commands. One useful 
option is to specify the number of threads to be used. This can be done through the {\tt -j} option:

\begin{verbatim}
> rficonsole -indirect-read -j 8 SB0.MS
\end{verbatim}

In this case, we would use 8 threads to flag the data instead of 4, which is the default. More threads will require more memory. More than 8 threads 
on the clusters slow down the process, therefore it is not recommended.

Another option is to flag based on data in the {\tt CORRECTED\_DATA} column, instead of
the default {\tt DATA} column. This can be done with the {\tt -column} option:

\begin{verbatim}
> rficonsole -column CORRECTED_DATA SB0.MS
\end{verbatim}

With this option, you can flag the corrected data created by BBS. While this fixes bad solutions, flagging corrected data (with any flagger) might not be a good approach, thus this option is BEING TESTED.

\subsubsection{Using the direct reading mode}

Since AOFlagger used to be limited by IO seeking and not by cpu performance, the indirect read approach was implemented in which a measurement set is written to a temporary location in a different order. The increase in speed on large sets is on the order of several factors, typically around 3 or 4 times. If you do not
want RFI Console to rewrite the set, you can leave out the ``indirect-read'' option.

%--------------------------------------------------------------------------------------------

\subsubsection{Flagging of bad baselines}
\label{flaggingofbadbaselines}

A new feature has been recently  implemented in RFI Console. At the end of each flagging run, it finds baselines which seems to behave abnormally.  The program tries to establish a smooth RFI vs. baseline-length curve as in Fig.~\ref{rfivsbaselinecurve}, estimates a standard deviation of the baselines to this curve, and clips baselines above some threshold, which is defaulted to 6 times the standard deviation.

\begin{figure*}[!ht]
\begin{center}
    \includegraphics[scale=0.5]{baselineSelection-L2010_08567-SB127.pdf}
  \caption{The percentage of RFI vs baseline length for sub band 127 of the observation {\tt L2010\_08567.} The bad baselines (in purple) will be reported in the output of RFI Console.}
  \label{rfivsbaselinecurve}
  \end{center}
\end{figure*}

Currently, RFI Console does not write the flags back to the MS, as it is important to test the new functionality. For the moment, RFI Console writes to the
terminal which baselines look bad. For the observation analyzed in Fig.~\ref{rfivsbaselinecurve}, the output looks like this:
\begin{verbatim}
Baseline CS002LBA x CS501LBA looks bad: 0% rfi
(zero or above 40% abs threshold)
Baseline CS002LBA x CS401LBA looks bad: 0% rfi
(zero or above 40% abs threshold)
Baseline CS002LBA x CS302LBA looks bad: 0% rfi
(zero or above 40% abs threshold)
[...]
Estimated std dev for thresholding, in percentage of RFI: 0.09%
Baseline CS030LBA x CS103LBA looks bad: 1.69% rfi,
10.7*sigma away from est baseline curve
Baseline CS001LBA x CS030LBA looks bad: 1.34% rfi,
6.9*sigma away from est baseline curve
Baseline CS005LBA x CS201LBA looks bad: 1.51% rfi,
9*sigma away from est baseline curve
Found 19/136 bad baselines: CS002LBAxCS501LBA, CS002LBAxCS501LBA,
CS002LBAxCS401LBA, CS002LBAxCS302LBA, CS002LBAxCS301LBA,
CS002LBAxCS201LBA, CS002LBAxCS103LBA, CS002LBAxCS032LBA,
CS002LBAxCS101LBA, CS002LBAxCS030LBA, CS002LBAxCS024LBA,
CS002LBAxCS021LBA, CS001LBAxCS002LBA, CS002LBAxCS003LBA,
CS002LBAxCS004LBA, CS002LBAxCS005LBA, CS002LBAxCS007LBA,
CS030LBAxCS103LBA, CS001LBAxCS030LBA, CS005LBAxCS201LBA
\end{verbatim}

In the case you would like to apply the flaggings suggested by RFI Console to the measurement set, you can create a strategy file and change the two occurrences of\\
{\tt <flag-bad-baselines>0</flag-bad-baselines>}\\
to\\
{\tt <flag-bad-baselines>1</flag-bad-baselines>}.\\
This will make it unnecessary for you to create a new {\tt DPPP.parset} to perform the baseline flagging.

%-------------------------------------------------------------------------------------------------


\subsection{Documentation}

The properties and performance of the AOFlagger have been described in the following papers:

\begin{itemize}
\item Post-correlation radio frequency interference classification methods, 
Offringa et al., MNRAS, Volume 405, Issue 1, pp. 155-167 (\href{http://arxiv.org/abs/1002.1957}{http://arxiv.org/abs/1002.1957}).
\item A LOFAR RFI detection pipeline and its first results, A.R. Offringa et 
al., Proceedings of Science, RFI2010 (\href{http://arxiv.org/abs/1007.2089}{http://arxiv.org/abs/1007.2089}).
\end{itemize}

