\section[Factor: Facet Calibration for LOFAR]{Factor: Facet Calibration for LOFAR\footnote{This section is maintained by David Rafferty({\tt drafferty@hs.uni-hamburg.de}).}}
\label{factor}

Factor is a Python package that allows for the production of wide-field HBA
LOFAR images using the facet calibration scheme described in van Weeren et al.\
(2015). Note that Factor is still beta software. Please report bugs to
{\tt drafferty@hs.uni-hamburg.de}. To initialize your environment for Factor,
users on CEP2 and CEP3 should run the following commands:
\begin{verbatim}
use Lofar
source ~rafferty/init_factor
use Casa
use Pythonlibs
\end{verbatim}

If you want to install Factor yourself, follow the instructions in the {\tt
README.md} file at \url{https://github.com/lofar-astron/factor}.

%-----------------------------------------------------------
\subsection{Usage}
\label{factor:usage}

Factor is run from the command line with the {\tt runfactor} executable as
follows:
\begin{verbatim}
Usage: runfactor [-v|-q|-d] <parset>

Options:
  --version   show program's version number and exit
  -h, --help  show this help message and exit
  -d                    enable dry-run mode
  -q                    enable quiet mode
  -r RESET, --reset=RESET
                        comma-separated list of directions to reset
  -t                    enable test mode
  -v                    enable verbose mode
\end{verbatim}
The parset specifies the parameters of the run. These are described in the next
sections. The reset ({\tt -r}) option is used to reset one or more directions in
the case of a failed run that requires that all operations for the directions
to be run again from scratch.

\subsection[Data Preparation: Pre-Facet Calibration Pipeline]{Data Preparation: Pre-Facet Calibration Pipeline\footnote{This 
subsection was written by Andreas Horneffer ({\tt ahorneffer@mpifr-bonn.mpg.de}) with a lot of help from Tim Shimwell}}
\label{factor:pre-facet-pipeline}

The input data to Factor must have the average amplitude scale set and average clock
offsets removed. Furthermore, the LOFAR beam towards the phase center should be
removed. 
The data should be concatenated in frequency to bands of about 2~MHz bandwidth (so about 
10--12~subbands). All bands (= input measurement sets) need to have the same number of
frequency channels\footnote{Factor does lots of averaging by different amounts to keep 
the data-size and computing time within limits. If the input files have different numbers 
of channels then finding valid averaging steps for all files gets problematic.}. Also the 
number of channels should have many divisors to make averaging to different scales easy.
The data should then undergo direction-independent, phase-only self
calibration, and the resulting solutions must be provided to Factor. 
%ca. 2 MHz bands
%same num-channels in all bands
%many divisors of num-channels

%These steps
%can be automated using the pre-facet-calibration pipeline (see
%\url{http://www.astron.nl/citt/facet-doc/prefacet.html}).

The pre-facet calibration pipeline in intended to prepare the observed data so that it can be used 
in the facet calibration pipeline. It is a parset for the genericpipeline, that first calibrates the 
calibrator, then transfers the gain amplitudes, 
clock delays and phase offsets to the target data and finally does a direction independent phase 
calibration of the target.

Please have a look at the documentation for the genericpipeline at: 
\begin{center}
\url{http://www.astron.nl/citt/genericpipeline/}
\end{center}
You should be reasonably familiar with setting up and running a genericpipeline before running this pipeline parset.

\subsubsection{Download and Set-Up}

The pipeline parset and associated scripts can be downloaded from github: {\bf ToDo: put it on github and adjust this here!}
\footnote{Until it is available on GitHub it can be obtained from CEP-3 by copying 
{\tt /cep3home/horneffer/Pre-Facet-Cal} to your home directory.}
\begin{verbatim}
git clone ...
\end{verbatim}
It contains the following directories: \\
\begin{tabular}{rp{.8\textwidth}}
{\tt bin} & scripts that process date, e.g. do the clock- / TEC- fitting, generate plots etc.\\
{\tt plugins} & scripts for manipulating mapfiles (They may move to the general LOFAR software at some time.)\\
{\tt parsets} & parsets (mostly BBS) that are used by the pipeline\\
{\tt skymodels} & skymodels that are used by the pipeline (e.g. for demixing or calibrating the calibrator)\\
\end{tabular}\\
The main part is the pipeline parset: {\tt Pre-Facet-Cal.parset}. This requires as input data that has 
been pre-processed by the ASTRON averaging pipeline.
It can work both with observations that had a second beam on a calibrator and with interleaved observations in 
which the calibrator was observed just before and after the target. It doesn't do any demixing, but does
A-Team clipping for the target data.\footnote{More versions of the pipeline which also do demixing and / or work 
with raw (non averaged) data are planned and will be added when they are done. They will probably not explained in 
detail but work in an analog fashion to the basic version.}

To run the genericpipeline you need a correctly set up configuration file for the genericpipeline,
see \url{http://www.astron.nl/citt/genericpipeline/#quick-start}.
In addition to the usual settings you need to ensure that the plugin scripts are found, for that 
you need to add the pre-facet calibration directory to the {\tt recipe\_directories} in the pipeline 
parset (so that the {\tt plugins} directory is a subdirectory of one of the {\tt recipe\_directories}).

At the beginning of {\tt Pre-Facet-Cal.parset} there is a section with the following parameters that you need 
to set:\\
\begin{tabular}{rp{.7\textwidth}}
{\tt avg\_timestep} & averaging step needed in NDPPP to average the data to 4 seconds time resolution \\
{\tt avg\_freqstep} & averaging step needed to average the data to 4 ch/SB frequency resolution \\
{\tt cal\_input\_path} & path to the directory in which the calibrator data can be found \\
{\tt cal\_input\_pattern} & pattern that matches the calibrator data files in {\tt cal\_input\_path} (Can e.g. 
  be used to restrict the amount of data for test runs.)xs \\
{\tt calibrator\_skymodel} & path to the skymodel for the calibrator \\
{\tt target\_input\_path} &  same as {\tt cal\_input\_path} but for the target data \\
{\tt target\_input\_pattern} & same as {\tt cal\_input\_pattern} but for the target data \\
{\tt target\_skymodel} & path to the skymodel for the phase-only calibration of the target \\
{\tt num\_SBs\_per\_group} & how many subbands should be concatenated into one measurement set \\
{\tt results\_directory} & path to the directory into which the processed target data will be moved \\
\end{tabular}

Below that is a section with the paths to the parsets and scripts that the pipeline uses. You need to 
adjust the paths to your setup.

\subsubsection{Running the Pipeline}

The steps of the pipeline are described in at bit more detail at: 
\url{http://www.astron.nl/citt/facet-doc/prefacet.html}. 
Before running the pipeline we strongly suggest to go through the pipeline parset and familiarize yourself
at least roughly with how the pipeline is supposed to work. 
In particular the flagging parameters for {\tt NDPPP} in the {\tt ndppp\_prep\_cal} and {\tt ndppp\_prep\_target}
steps probably need to be adjusted to each observation.

Running the pipeline is done in two steps: first only the calibrator part of the pipeline is run. After that the
inspection plots of the fitting routines should be inspected. 
The page at \url{http://www.astron.nl/citt/facet-doc/prefacet.html#plots} contains explanations of the plots in the 
description of the steps. It is rather likely that some additional flagging or so is needed after the first run,
this can be included by modifying the pipeline parset. After that the calibration part needs to be re-run. This
may need to be repeated a number of times. A full re-run of the pipeline can be done by removing the runtime and 
work directories of the previous pipeline run.\footnote{Experts of the genericpipeline can re-run only part of a
pipeline run by either modifying the statefile or making a new pipeline parset that pick up the data at an 
intermediate step and continues from there. But there is no easy way to do that yet.}

Only after the calibrator processing was successful and the fits to the calibration values are satisfactory, 
should the target part of the pipeline be run. This can be done by either adding the steps for the target processing
to the existing pipeline, or by copying the numpy ({\tt *.npy}) files from the work directory of the calibrator part 
to the new work directory and running only the steps for the target processing.

After the pipeline was run successfully, the resulting measurement sets are moved to the specified directory. The
instrument table of the direction-independent, phase-only calibration are inside the measurement sets with the 
names {\tt instrument\_directioninpendent}.

\subsubsection{Quick-start}
\label{factor:quick-start}
Below is a quick-start guide to a typical run.

\begin{enumerate}
\item Collect the input bands (described in sub-section \ref{factor:pre-facet-pipeline}) in a
single directory. The MS names can be anything but must end in ``.MS'' or
``.ms'':
\begin{verbatim}
[/data/factor_input]$ ls
band1.MS         band2.ms         A.MS         b.ms
\end{verbatim}
\item Edit the Factor parset (described in Section \ref{factor:parset}) to fit
your reduction and computing resources. For example, the parset for a single
machine could be:
\begin{verbatim}
[/data]$ more factor.parset
[global]
dir_working = /data/factor_output
dir_ms = /data/factor_input
parmdb_name = instrument_ap_smoothed
interactive = True

[directions]
flux_min_jy = 0.1
size_max_arcmin = 3.0
separation_max_arcmin = 7.5
max_num = 30
groupings = 1:0
check_edges = True

[cluster]
ncpu = 5
\end{verbatim}
\item Run the reduction:
\begin{verbatim}
[/data]$ runfactor factor.parset
\end{verbatim}
\end{enumerate}

%-----------------------------------------------------------
\subsection{Parset}
\label{factor:parset}

Below is an template parset that shows the available parameters. Note that the
parameters are grouped by sections (denoted by headings such as {\tt [global]},
{\tt [directions]}, etc.). Required parameters are noted.

\begin{verbatim}
[global]
# Full path to working dir where Factor will run (required). All output will
# be placed in this directory
dir_working = /data/wdir

# Full path to directory containing selfcal-ed bands, will be scanned for all .MS
# and .ms files (required). Note that these files will be modified by Factor, so
# please keep a copy of these data elsewhere
dir_ms = /data/bands

# Parmdb name for dir-indep. selfcal solutions (stored inside the input
# band measurement sets, so path should be relative to those; default =
# instrument)
parmdb_name = instrument_ap_smoothed

# Make final mosaic (default = True)
make_mosaic = True

# Use interactive mode (default = False). Factor will ask for confirmation
# of internally derived DDE calibrators and facets
interactive = False

# Max number of bands per WSClean image when wide-band clean is used (default = 5).
# Smaller values produce better results but require longer run times. Wide-band
# clean is activated when there are more than 5 bands
wsclean_nbands = 2

# Exit if selfcal fails for any direction (default = True). If False, processing
# will continue and the failed direction will receive the selfcal solutions of
# the nearest successful direction
exit_on_selfcal_failure = True

# Use WSClean or CASA for imaging of entire facet (default = wsclean). For large
# bandwidths, the CASA imager is typically faster
facet_imager = casa


[directions]
# Full path to file containing calibrator directions. If not given, directions
# are selected internally using the flux and size cuts below
directions_file = /data/directions.txt

# Flux and size cuts for selecting directions internally (min flux, max size
# of a source, and max separation between sources below which they are grouped
# into one direction; required if no directions_file is given). The number of
# internally derived directions can be limited to a maximum number
# of directions if desired with max_num (default = all). These parameters will
# determine the faceting of the field
flux_min_Jy = 0.3
size_max_arcmin = 2.0
separation_max_arcmin = 6.0
max_num = 50

# Check whether any sources from the initial subtract sky model fall on facet
# edges. If any are found, the facet regions are adjusted to avoid them (default
# is False)
check_edges = False

# Total number of directions to process (default = all). If this number is
# greater than ndir_selfcal, then the remaining directions will not be selfcal-
# ed but will instead be imaged with the selfcal solutions from the nearest
# direction for which selfcal was done
ndir_total = 10

# Total number of directions to selfcal (default = all)
ndir_selfcal = 5

# Grouping of directions into groups that are selfcal-ed in parallel, defined as
# grouping:n_total_per_grouping. For example, groupings = 1:5, 4:0 means two
# groupings are used, with the first 5 directions put into groups of one (i.e.,
# each direction processed in series) and the rest of the directions divided
# into groups of 4 (i.e., 4 directions processed in parallel). Default is one at
# a time (i.e., groupings = 1:0)
groupings = 1:5, 4:0

# If groups are used to process more than one direction in parallel, reordering
# of the directions in the groups can be done to maximize the separation
# between directions in each group (default = True)
allow_reordering = True

# A target can be specified to ensure that it falls entirely within a single
# facet. The values should be those of a circular region that encloses the
# source and not those of the target itself. Lastly, the target can be placed
# in a facet of its own. In this case, it will not go through selfcal but will
# instead use the selfcal solutions of the nearest facet for which selfcal was
# done
target_ra = 14h41m01.884
target_dec = +35d30m31.52
target_radius_arcmin = 10.2
target_has_own_facet = False


[cluster]
# Full path to cluster description file. Use clusterdesc_file = PBS to use the
# PBS / torque reserved nodes. If not given, the clusterdesc file for a single
# (i.e., local) node is used
clusterdesc_file = PBS

# Full path to a local disk on the nodes for I/O-intensive processing. The path
# must be the same for all nodes. At the moment, the local disk is only used by
# WSClean jobs. If not given, the default directory in the working directory is
# used
dir_local = /tmp

# Maximum number of CPUs per node to use (default = all)
ncpu = 6

# Maximum fraction of the total memory per node that WSClean may use (default =
# 0.9)
fmem = 0.5

# Number of directions to process in parallel on each node (default = 1). If
# directions are split into groups to be processed in parallel (with the
# groupings parameter), this parameter controls how many directions are run
# simultaneously on a single node. Note that the number of CPUs (set with the
# ncpu parameter) will be divided among the directions on each node
ndir_per_node = 1

# Number of imager jobs to run per node (affects initsubtract operation and
# facetimage; default = 1). If your nodes have many CPUs and > 32 GB of memory,
# it may be advantageous to set this to 2 or more
nimg_per_node = 1


# MS-specific parameters (optional)
[ms1.ms]
init_skymodel = /data/ms1.sky

[ms2.ms]
init_skymodel = /data/ms2.sky
\end{verbatim}

\subsection{Directions}
\label{factor:directions}

The directions for which DDE calibration is performed can be selected
automatically in Factor or specified in a file. The format of the file is as
follows:

\begin{verbatim}
# This is an example directions file for Factor.
#
# Directions should be sorted in order of reduction (e.g., bright to faint).
#
# name position atrous_do mscale_field_do cal_imsize solint_ph solint_amp
# field_imsize dynamic_range region_selfcal region_field peel_skymodel
# outlier_source

s1  14h41m01.884,+35d30m31.52 False False 512  1 30  5600 LD empty empty    s1.skymodel True
s2  14h38m29.584,+33d57m37.82 False False 1024 1 30  5600 LD empty empty    empty       False
s25 14h21m07.482,+35d35m22.87 False False 1280 2 240 5600 LD empty s25.rgn  empty       False
\end{verbatim}
This file should be specified in the parset file as the {\tt directions\_file}
parameter.

If no {\tt directions\_file} is given, the directions and their order of
processing are determined internally using the flux, size, and separation limits
specified in the parset as follows. First, the initial apparent-flux
clean-component (CC) skymodels (either given as input in the parset or generated
by the initial subtraction operation) are grouped by LSMTool using the
thresholding algorithm. These grouped patches of CCs are then filtered to meet
the specified flux and size limits ({\tt flux\_min\_Jy} and {\tt
size\_max\_arcmin}, respectively) and nearby patches are merged (specified by
{\tt separation\_max\_arcmin}). The resulting patches are then sorted by
apparent flux (from bright to faint) at the lowest available frequency, and the
final {\tt directions\_file} is written to the file {\tt
factor\_directions.txt} in the working directory.

Lastly, one can specify that more than one direction be processed in parallel
with the groupings parameter. The groupings parameter specifies the grouping
level (the number of directions to process simultaneously) and the total number
of directions at each grouping level. For example, {\tt groupings = 1:5, 4:0}
means two groupings are used, with the first 5 directions put into groups of one
(i.e., the directions are processed in series) and the rest of the directions
divided into groups of 4 (i.e., 4 directions processed in parallel). Shuffling
is done between neighboring groups to achieve the largest minimum separation
between directions in the groups. This sorting attempts to minimize the effects
that any artifacts from one direction might have on the other simultaneously
processed directions.


%-----------------------------------------------------------
\subsection{Parallelization}
\label{factor:parallel}

Factor has been designed to run as much as possible in a parallel manner. To
this end, support is present for distributing the reduction over the processors
and nodes of a compute cluster. To use Factor on a single computer, no setup is
necessary beyond specifying the maximum number of CPUs to use. To use Factor on
multiple nodes of a compute cluster, the required setup depends on the cluster
layout (distributed vs. shared file system) and scheduling system (if any), as
described in the following sections.

\subsubsection{Cluster without a scheduler}
To use Factor on a cluster that does not use a scheduler to assign nodes to the user,
you must provide a LOFAR pipeline cluster description
file that tells Factor which nodes are available. An example of such a file is
shown below.

\begin{verbatim}
ClusterName = MyCluster
Compute.Nodes = [node01, node02]
\end{verbatim}

\subsubsection{Cluster with Torque / PBS }
For use on a cluster that uses torque and PBS, you can set {\tt clusterdesc =
PBS}. Factor will then automatically determine the nodes for which you have a
PBS reservation and use them. Note that you must ask for all the nodes you need
in a single PBS script (e.g., to use 6 nodes, with {\tt \#PBS -l
nodes=6:ppn=6}), so that all nodes are available for the full Factor run. An
example PBS script is shown below.

\begin{verbatim}
#!/bin/bash
#PBS -N Factor
#PBS -l walltime=100:00:00
#PBS -l nodes=6:ppn=6
#PBS -j oe
#PBS -o output-$PBS_JOBNAME-$PBS_JOBID
cd $PBS_O_WORKDIR
source /home/sttf201/init-lofar.sh
runfactor factor.parset
\end{verbatim}

\subsubsection{Cluster with a shared file system}
Factor works by default on a compute cluster with a shared file system.

\subsubsection{Cluster with a distributed file system}
Factor does not yet work on multiple nodes with a distributed file system (such
as CEP2 and CEP3). It does work on a single node of such a system.


%-----------------------------------------------------------
\subsection{Resuming}
\label{factor:resuming}

Due to the potentially long run times and the consequent non-negligible chance
of some unforeseen failure occurring, Factor has been designed to allow easy
resumption of a reduction from a saved state and will skip over any steps that
were successfully completed previously. In this way, one can quickly resume a
reduction that was halted (either by the user or due to some problem) by simply
re-running Factor with the same parset.

For example, one can specify that only the first 5 directions be processed.
Once these directions are done, Factor will exit. One can then alter the parset
and specify that 10 directions should be done. Upon restarting, Factor will skip
over the first 5 directions and start with the 6th one (and ending with the 10th
one).


%-----------------------------------------------------------
\subsection{Structure}
\label{factor:structure}

Factor is structured based on operations and actions. An operation is composed
of a sequence of actions. For example, the initial subtraction operation
(denoted {\tt initsubtract}) performs the following actions: image at high resolution,
make a sky model, subtract the sky model, average, image at low resolution,
make another sky model, and finally subtract the second sky model. For details,
see the Facet Calibration Pipeline documentation at
\url{http://www.astron.nl/citt/facet-doc}.

The results of the processing are stored in the {\tt results} directory in the
working directory. Inside these subdirectories, results are organized by
operation and direction. For example, for a direction named ``dir1'', the
results produced during the {\tt facetselfcal} operation are stored in {{\tt
results/facetselfcal/dir1/}.
