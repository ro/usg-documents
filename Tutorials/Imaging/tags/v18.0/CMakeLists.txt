
## ==============================================================================
##
##  Collect included files
##
## ==============================================================================

## Create copy of the directories containing the figures to be included

execute_process(
  COMMAND cp -r ${CMAKE_CURRENT_SOURCE_DIR}/figures .
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  )

file (GLOB ImagingCookbook_includes *.tex)

foreach (_include ${ImagingCookbook_includes})
  execute_process(
    COMMAND cp ${_include} .
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    )
endforeach (_include)

## ==============================================================================
##
##  Process the source files
##
## ==============================================================================

add_custom_target (ImagingCookbook ALL)

## set up the list of source files
file (GLOB ImagingCookbook_sources lofar_imaging_cookbook.tex)

foreach (_tex ${ImagingCookbook_sources})

  ## get the name of the file without the extension
  get_filename_component (_tex ${_tex} NAME_WE)

  ## define custom target to create PDF file
  add_custom_target(${_tex}
    COMMAND ${PDFLATEX_COMPILER} ${CMAKE_CURRENT_SOURCE_DIR}/${_tex}.tex
    COMMAND ${PDFLATEX_COMPILER} ${CMAKE_CURRENT_SOURCE_DIR}/${_tex}.tex
    DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${_tex}.tex
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/${_tex}.tex
    )

  add_dependencies (ImagingCookbook ${_tex})

endforeach (_tex)
