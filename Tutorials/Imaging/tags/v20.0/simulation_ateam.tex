\svnInfo $Id: simulation_ateam.tex 7612 2011-04-27 09:26:05Z zwieten $


\newpage
\section[LOFAR simulation software]{LOFAR simulation software\footnote{The author of this appendix is Valentina Vacca (vvacca[at]mpa-garching[dot]mpg[dot]de).}}
\label{sec:LSS}
The A-team or other bright sources can strongly affect the observation of  nearby targets.
To understand which is the best strategy to adopt in order to deal with these strong sources, it is useful to make the user aware in advance about their effect on the observed target when the observations will be performed. 
In this context a \emph{LOFAR simulation software}\footnote{This software has been developed by Jose Sabater ({\tt jsm[at]roe[dot]ac[dot]uk}).} has been developed.
The software package is composed of two Python scripts, 
one to simulate a mock data set ({\tt simulate.py}) and the other to compare 
simulations and observations ({\tt compare.py}). They can be found at
\begin{verbatim}
 /opt/cep/tools/simulation/
\end{verbatim}
All the dependencies of the software are installed in the LOFAR cluster CEP3. To source the scripts type 
\begin{verbatim}
> use Lofar
\end{verbatim} 
and 
\begin{verbatim}
> use Cookbook
\end{verbatim}

By using these scripts, the user can simulate two mock Measurement Sets containing respectively the target and A-team sources, and compare them in order to understand which percentage of the data will be affected by the bright sources.

\subsection{Simulating a Measurement Set}
The simulation of the data is performed through the script {\tt simulate.py}.
The script includes three main blocks generating commands and parsets that the user can run in order to:
\begin{enumerate}
\item create an empty mock Measurement Set with the same starting time, ending time, and pointing as the observations, by using {\tt makems}; 
\item update the calibration tables in the Measurement Set with the information to compute the beam of the instrument, by using {\tt makebeamtables};
\item simulate the effect of a model of sources on the data, by using BBS.
\end{enumerate}
If necessary, commands and parsets can be modified by the user before to be run.

When launching the script the user can use the following options:\\
\begin{longtable}{ll}
\centering     
{\tt --ra=RA}&right ascension of the field\\
{\tt --dec=DEC}&declination of the field\\ 
{\tt --time=TIME}&start date and time of the observation\\ 
                 &in the format YYYY/MM/DD/hh:mm:ss.s\\
{\tt --n-time=N\_TIME}&duration of the observation in integer multiples of 10\,s\\
                    &(the default value is 1800 corresponding to 5\,h)\\
{\tt --name=NAME}&name of the simulated field and prefix of the output files\\
{\tt --source=SOURCE1,SOURCE2,...}& name of the source(s) to simulate\\
{\tt --sky-model=SKY\_MODEL}&skymodel of the source(s) to simulate\\
{\tt --path=PATH}& path to store the parsets and Measurement Sets of the\\
& simulation\\
{\tt --overwrite}& overwrite existing parsets and Measurement Sets\\ 
{\tt --lba}& simulate a LBA observation (the default is HBA)\\
{\tt --antenna-conf=ANTENNA\_CONF}&  antenna configuration (the default antenna\\
                        &configurations are HBA\_DUAL\_INNER for\\ 
                        &HBA and LBA\_OUTER for LBA)\\
{\tt --h} or {\tt --help}& shows the usage of the script\\
\end{longtable}


\noindent
The format for time, right ascension, and declination is the same as used in the parset. In particular, we note that declination uses dots instead than colons. At the moment it is possible to insert in the simulation only the sources 3C53 and 3C237, and the A-team sources CygA, CasA, CasA4, TauA, VirA, VirA4, HydraA\footnote{Models for these sources are available e.g. in {\tt /globaldata/COOKBOOK/Models/Ateam\_LBA\_CC.skymodel}. Note: when CasA and VirA are used the simulation is very slow.}. 
The default location of the files produced when launching the script is
\begin{verbatim}
/data/scratch/<user_name>/simulation/ 
\end{verbatim}
The present version of the software creates the working directory {\tt simulation/} and the parsets, while the commands are only displayed on the screen and have to be executed by the user. In the future release the commands will be directly executed by the script and other antenna configurations will be allowed.

\subsubsection{Example}
As an example, we run {\tt simulate.py} to produce a mock observation in the direction of 3C\,299 ($RA=14h21m05.88s$ and $DEC=41^{\circ}44^{\prime}49.490^{\prime \prime}$), with a total observational time $\sim 2$\,h starting at 04:16:02.5 in date 14 April 2013 and we will investigate the effect of Cygnus\,A on the target source:
\begin{verbatim}
python /opt/cep/tools/simulation/simulate.py --ra 14:21:05.88 \
--dec 41.44.49.490 --time 2013/04/14/04:16:02.5 --n-time 720 \
--name 3C299 --source  CygAGG --sky-model \
/globaldata/COOKBOOK/Models/Ateam_LBA_CC.skymodel --overwrite
\end{verbatim}
The script creates the folder {\tt simulation/} in the default directory {\tt /data/scratch/<user\_name>/} containing the parsets\\
\begin{verbatim}
makems_3C299_HBA.parset
predict_CygAGG.parset
\end{verbatim}
and gives as an output on the screen the following commands that the user has to run to produce the mock Measurement Set
\begin{verbatim}
cd /data/scratch/<user_name>/simulation/; 
makems /data/scratch/<user_name>/simulation/makems_3C299_HBA.parset; 
makebeamtables antennaset=HBA_DUAL_INNER ms=20130414_3C299_HBA.MS \
overwrite=True
cp -r /data/scratch/<user_name>/simulation/20130414_3C299_HBA.MS \
/data/scratch/<user_name>/simulation/20130414_3C299_HBA_CygAGG.MS
cd /data/scratch/<user_name>/simulation/; 
(date; calibrate-stand-alone -f \
/data/scratch/<user_name>/simulation/20130414_3C299_HBA_CygAGG.MS \
predict_CygAGG.parset \
/globaldata/COOKBOOK/Models/Ateam_LBA_CC.skymodel; \
date) | tee log_3C299_CygAGG.txt
\end{verbatim}

which is one long command split over multiple lines (splitting indicated by \textbackslash).\\
This output contains all the instructions the user has to follow to produce the mock Measurement Set:
\begin{enumerate}
\item move to the working directory {\tt simulation/}
\item run {\tt makems} with the parset {\tt makems\_3C299\_HBA.parset}\footnote{The antenna configuration files can be found in: {\tt /globaldata/COOKBOOK/Demixing/}} to create the mock Measurement Set {\tt 20131304\_3C299\_HBA.MS}
\item run {\tt makebeamtables}\footnote{Alternativelly: {\tt makebeamtables ihbadeltadir=\$LOFARROOT/etc/StaticMetaData}} on the mock Measurement Set to compute
the station beam model
\item change the name of the Measurement Set from {\tt 20131304\_3C299\_HBA.MS} to \\{\tt 20131304\_3C299\_HBA\_CygAGG.MS}
\item move again to the working directory {\tt simulation/} if you are not there anymore 
\item run BBS on the Measurement Set by using the parset {\tt predict\_CygAGG.parset} to predict the effect of the A-team source(s) on the target. At the moment no default sky model is present, therefore the user has to specify one. The output on the screen of BBS plus the {\tt date} before and after BBS was running are redirected to the file {\tt log\_3C299\_CygAGG.txt}. With these parameters BBS takes $\sim$5\,m to run.
\end{enumerate}


\subsection{Comparing observations and simulations}

Once the simulation has been produced, the user can investigate the effect of the bright sources on the target field with the script {\tt compare.py}. 
The code calculates the percentage of data points affected by the A-team for the XX and YY polarization products, according to two possible criteria:
\begin{enumerate}
\item the user fixes a flux {\tt threshold} and all the data points with flux larger than this threshold are considered affected. In this case the user needs only a mock Measurement Set containing the A-team sources or other bright sources that can affect the data;
\item the user fixes a {\tt level} and compares the flux of the bright source(s) ($S_{\rm A}$) with the flux of a central source in the target field ($S_{\rm T}$). If $S_{\rm A}/S_{\rm T}\geq$ {\tt level}, the corresponding visibility will be considered affected by the bright source(s). In this case the user needs  a mock Measurement Set containing the target source (alternatively, if the observations have been already performed, the observed data can be used) and a mock Measurement Set containing the A-team sources.
\end{enumerate}



When launching the script on the mock Measurement Set containing A-team/bright sources that can affect the observation of a target source of interest, the user can choose among the following options:\\
\begin{longtable}{ll}
\centering
{\tt --s} or {\tt --source=SOURCE.MS}& simulated Measurement Set of the central source\\
& (alternatively the real observation can be used)\\
{\tt --t} or {\tt --threshold=T1, T2, ...}&threshold(s) (the default value is 5\,Jy).\\
&A different threshold for each bright source can\\
& be specified\\
{\tt --l} or {\tt --level=L1, L2, ...}&ratio(s) between the flux of an A-team/bright\\
& source $S_{\rm A}$ and of the target source $S_{\rm T}$. A different\\
&level for each  A-team/bright source can be \\
&specified\\
{\tt -f} or {\tt --stat-function=STAT\_FUNCTION}& statistical function used for the threshold/level.\\
&Possible options are: max, min, median, std, mean\\
& (the default function is median)\\
{\tt  --all-correlations=CORRELATIONS}&    correlations considered (not working yet)\\
{\tt -p} or {\tt --plot} & produce plots\\
{\tt --h} or {\tt --help}& shows the usage of the program\\
\end{longtable}
As an output a percentage of the amount of data above the threshold or above the level selected by the user (and therefore affected by A-team/bright sources) at different times is given for all channels and for XX and YY correlation products.
This percentage is calculated according the statistical function selected among the options.

Due to a bug in the Python module {\tt argparse}, if the options {\tt --t} or {\tt --l} are used, they can not be followed by the name of the Measurement Set containing the A-team sources. In this case the name of the Measurement Set has to be placed before these options, just after {\tt compare.py}. Nevertheless, they can be followed by any other option.

At the moment the code does not allows the simultaneous comparison of multiple bright sources. This option will be available in a future release.
\subsubsection{Example}
As an example, we run {\tt compare.py} on the simulation produced with {\tt simulation.py} at the step before by using the option {\tt --t}:
\begin{verbatim}
python  /opt/cep/tools/simulation/compare.py \
/data/scratch/vacca/LSS/simulation/20130414_3C299_HBA_CygAGG.MS \
--t 4 -f "median"
\end{verbatim}
A combination of the options  {\tt --t} and {\tt --l} is also possible.

The script gives as an output on the screen:
\begin{verbatim}
Successful readonly open of default-locked table \
/data/scratch/<user_name>/simulation/20130414_3C299_HBA_CygAGG.MS: \
25 columns, 1274400 rows
===================================
Flux density threshold: 4.000000
Freq:  0 XX:   2.98% YY:   2.86%
Freq:  1 XX:   2.81% YY:   2.81%
Freq:  2 XX:   0.00% YY:   0.00%
Freq:  3 XX:   0.00% YY:   0.00%
Freq:  4 XX:   0.12% YY:   0.06%
Freq:  5 XX:   0.00% YY:   0.00%
Freq:  6 XX:   0.12% YY:   0.12%
Freq:  7 XX:   0.00% YY:   0.00%
Freq:  8 XX:   0.06% YY:   0.06%
Freq:  9 XX:   0.12% YY:   0.06%
Freq: 10 XX:   0.88% YY:   0.64%
Freq: 11 XX:   0.94% YY:   0.82%
Freq: 12 XX:   1.46% YY:   1.34%
Freq: 13 XX:   1.29% YY:   1.29%
\end{verbatim}
The script takes a few seconds to run. During the observation, 10\% of the data will be affected by Cygnus\,A above a threshold of 4\,Jy.

\subsection[Simulation and A-team clipping tutorial]{Simulation and A-team clipping tutorial}
\label{sec: New demixing algorithm}
When the A-team or other bright sources are present for the most of the observational time, the best strategy  to eliminate them from the visibilities is to apply the standard demixing procedure that consists on the subtraction of the A-team sources from the entire observation (see Sect.~\ref{dppp:demixing}). On the contrary, when long HBA observations are performed, A-team sources can be visible just for a fraction of the observation (see for example Fig.~\ref{figateamclipper}) and therefore they can just partially affect the data set. In this case their subtraction from the all observation can be dangerous and it is better to adopt the alternative strategy\footnote{This procedure has been developed by Reinout van Weeren ({\tt rvanweeren[at]cfa[dot]harvard[dot]edu}).} described in the following. 

\subsubsection{Predict}
\label{subsec: New demixing algorithm Predict}
When observations are already available the user does not need to simulate a mock observation but only to simulate the effect of A-team sources on the target. In this case, the first step consists in simulating the A-team sources (VirA, CygA, CasA, TauA) during the observations of interest. Through the stand-alone version of BBS (described in Chapter~\ref{BBS}), the data set can be filled with a sky model containing the A-team sources (VirA, CygA, CasA, TaurA): 
\begin{verbatim}
calibrate-stand-alone -f dataset.MS predict.parset skymodel.skymodel
\end{verbatim}
where 
\begin{enumerate}
\item {\tt data set.MS} is the observed data set;
\item {\tt predict.parset} is a parset aiming at simulating data according to a given sky model;
\item {\tt skymodel.skymodel} is the file containing the model of the A-team sources.
\end{enumerate}
An example of the parset to be used is
\begin{verbatim}
Strategy.InputColumn = DATA
Strategy.ChunkSize = 300
Strategy.UseSolver = F
Strategy.Steps = [predict4]
 
Step.predict4.Model.Sources = [VirA_4_patch,CygAGG,CasA_4_patch,TauAGG]
Step.predict4.Model.Cache.Enable    = T
Step.predict4.Model.Gain.Enable     = F
Step.predict4.Operation             = PREDICT
Step.predict4.Output.Column         = MODEL_DATA
Step.predict4.Model.Beam.Enable     = True
\end{verbatim} 
and an example of a skymodel for LBA observations can be found at
\begin{verbatim}
/globaldata/COOKBOOK/Models/Ateam_LBA_CC.skymodel
\end{verbatim} 
The simulation should require about 1\,h (for a 10\,h long observation, 4 frequency channels and a time step of 5\,s).
As a result the data set will contain in the {\tt MODEL\_DATA} column the information about A-team sources visible during the observations.

\subsubsection{A-team clipper}
\label{Ateamclippersection}
The second step consists in using a Python algorithm that takes in input the observed data set and flags\footnote{An algorithm based on a subtraction procedure is currently under development.} the A-team signal baseline by baseline above a threshold chosen according to the observing frequency. 
Different thresholds are possible for LBA and HBA data: default values are considered in the script but the clip levels can be adjusted by the user.
The default threshold for LBA observations is 50\,Jy. This value is required to remove the A-team contribution but at these low frequencies this implies the flag of most of the data. Therefore, at the moment for LBA observations at this frequency it is strongly recommended to use the standard demixing procedures. For HBA observations the algorithm is highly efficient for A-team sources far away ($\geq$20--30$^{\circ}$ ) from the target. The default threshold is 5\,Jy but values in the range 2--10\,Jy can be suitable for different data sets, being 10\,Jy the safest value in order not to flag the target source. The user can choose the proper value by comparing the amplitude of the observed target and the contribution from A-team sources.
The script can be found at the LOFAR-Contributions GitHub repository:
\begin{verbatim}
https://github.com/lofar-astron/LOFAR-Contributions
\end{verbatim}
Once the user copies it in his home directory it can be launched as follows
\begin{verbatim}
python Ateamclipper.py dataset.MS
\end{verbatim} 
The scripts takes few tens of seconds to run and an example of output is
\begin{verbatim}
Successful read/write open of default-locked table \ 
L123834_SB417_uv.dppp.MS.newtest: 27 columns, 12303270 rows \
Successful readonly open of default-locked table \
L123834_SB417_uv.dppp.MS.newtest/SPECTRAL_WINDOW: 14 columns, 1 rows
------------------------------
SB Frequency [MHz] 134.765625
% input XX flagged 4.12201796758
% input YY flagged 4.12201796758

Cliplevel used [Jy] 4.0



Doing polarization,chan 0 0
Doing polarization,chan 0 1
Doing polarization,chan 0 2
Doing polarization,chan 0 3
Doing polarization,chan 1 0
Doing polarization,chan 1 1
Doing polarization,chan 1 2
Doing polarization,chan 1 3
Doing polarization,chan 2 0
Doing polarization,chan 2 1
Doing polarization,chan 2 2
Doing polarization,chan 2 3
Doing polarization,chan 3 0
Doing polarization,chan 3 1
Doing polarization,chan 3 2
Doing polarization,chan 3 3

% output XX flagged 6.54876711638
% output YY flagged 6.54876711638


\end{verbatim} 

The output gives information about the frequency of the data set and about the consequent clip level applied. The percentage of flags applied to the observation before ({\tt input XX and YY flagged}) and after ({\tt output XX and YY flagged}) the clipper is given as well. If the percentage of flags due to the algorithm is large ($\geq20\%$) it is strongly suggested to apply the standard demixing procedure. 




In Fig.~\ref{figateamsim} an example of the result from this algorithm is shown. In the top left panel, the amplitude versus time of the A-team sources VirA, CygA, CasA, and TauA, simulated as described in Sect.~\ref{subsec: New demixing algorithm Predict}, during an HBA observation of 3C\,299 (PI: Dr. Chiara Ferrari) is shown for a single baseline. Amplitudes larger than 4\,Jy are present at the end of the observations. In the top right panel the amplitude versus time from the same baseline is shown for the raw observed data set. In the real observation, a pattern can be identified when the simulated A-team sources are above 4\,Jy. In the bottom left panel the amplitude versus time is shown after the standard demixing subtraction of  VirA, CygA, CasA, and TauA, while in the bottom right panel after their flag with the new demixing algorithm with a cut threshold of 4\,Jy.

\begin{figure}[ht]
\begin{center}
\includegraphics[scale=0.45]{ateamclipper.png}
\caption{Example of amplitude of A-team sources versus time for a single baseline of an HBA observation. The A-team sources affect the observation just for a fraction of the total observing time. \label{figateamclipper}}
\end{center}
\end{figure}
\begin{figure}[ht]
\begin{center}
\includegraphics[scale=0.45]{ateam.png}
\end{center}
\caption{\emph{Top left:} Simulation of the A-team sources VirA, CygA, CasA, and TauA during an HBA observation of 3C\,299 (PI: Dr. Chiara Ferrari). \emph{Top right:} Amplitude versus time from the raw data set. \emph{Bottom left:} Amplitude versus time after the standard demixing subtraction of VirA, CygA, CasA, and TauA. \emph{Bottom right:} Amplitude versus time after the flag of VirA, CygA, CasA, and TauA with the new demixing algorithm with a cut threshold of 4\,Jy. The sources affecting the data is likely Cygnus\,A. \label{figateamsim}}
\end{figure}


