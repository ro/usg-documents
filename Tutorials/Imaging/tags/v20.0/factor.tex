\section[Factor: Facet Calibration for LOFAR]{Factor: Facet Calibration for LOFAR\footnote{This section is maintained by David Rafferty({\tt drafferty@hs.uni-hamburg.de}).}}
\label{factor}

Factor is a Python package that allows for the production of wide-field HBA
LOFAR images using the facet calibration scheme described in van Weeren et al.\
(2016) to correct for direction-dependent effects (DDEs). To initialize your environment for Factor,
users on CEP3 should run the following commands:
\begin{verbatim}
source ~rafferty/init_factor
\end{verbatim}

If you want to install Factor yourself, follow the instructions in the {\tt
README.md} file at \url{https://github.com/lofar-astron/factor}. Detailed documentation for Factor is available at \url{http://www.astron.nl/citt/facet-doc}.

\subsection[Data Preparation: Pre-Facet Calibration Pipeline]{Data Preparation: Pre-Facet Calibration Pipeline\footnote{This
subsection was written by Andreas Horneffer ({\tt ahorneffer@mpifr-bonn.mpg.de}) with a lot of help from Tim Shimwell}}
\label{factor:pre-facet-pipeline}

The input data to Factor must have the average amplitude scale set and average clock
offsets removed. Furthermore, the LOFAR beam towards the phase center should be
removed. The data should be concatenated in frequency to bands of about 2~MHz bandwidth (so about
10--12~subbands). All bands (= input measurement sets) need to have the same number of
frequency channels\footnote{Factor does lots of averaging by different amounts to keep
the data-size and computing time within limits. If the input files have different numbers
of channels then finding valid averaging steps for all files gets problematic.}. Also the
number of channels should have many divisors to make averaging to different scales easy.
The data should then undergo direction-independent, phase-only self
calibration, and the resulting solutions must be provided to Factor.
%ca. 2 MHz bands
%same num-channels in all bands
%many divisors of num-channels

%These steps
%can be automated using the pre-facet-calibration pipeline (see
%\url{http://www.astron.nl/citt/facet-doc/prefacet.html}).

The pre-facet calibration pipelines are intended to prepare the observed data so that they can be used
in Factor. The pipelines are defined as parsets for the genericpipeline, that first calibrate the
calibrator, then transfer the gain amplitudes, clock delays and phase offsets to the target data, do a direction-independent phase calibration of the target, and finally image and subtract sources.

Please have a look at the documentation for the genericpipeline at:
\begin{center}
\url{http://www.astron.nl/citt/genericpipeline/}
\end{center}
You should be reasonably familiar with setting up and running a genericpipeline before running this pipeline parset.

\subsubsection{Download and Set-Up}

The pipeline parsets and associated scripts can be found at: \url{https://github.com/lofar-astron/prefactor}. These pipelines require as input data that has been pre-processed by the ASTRON averaging pipeline.
They can work both with observations that had a second beam on a calibrator and with interleaved observations in
which the calibrator was observed just before and after the target. No demixing is done, but
A-Team clipping is performed for the target data.\footnote{Versions of the pipeline (parset) which also do demixing are being planned.}

To run the genericpipeline you need a correctly set up configuration file for the genericpipeline,
see \url{http://www.astron.nl/citt/genericpipeline/#quick-start}.
In addition to the usual settings you need to ensure that the plugin scripts are found, for that
you need to add the pre-facet calibration directory to the {\tt recipe\_directories} in the pipeline
parset (so that the {\tt plugins} directory is a subdirectory of one of the {\tt recipe\_directories}).

\subsubsection{Overview and examples}

The most recent overview of the pre-facet pipeline functionality as well as several use cases and troubleshooting information can be found on the pre-facet pipeline GitHub wiki at: \url{https://github.com/lofar-astron/prefactor/wiki}

After the pipeline was run successfully, the resulting measurement sets are moved to the specified directory. The
instrument table of the direction-independent, phase-only calibration are inside the measurement sets with the
names {\tt instrument\_directioninpendent}. These measurement sets are the input to the Initial-Subtract pipeline, which should be run next to do the imaging and source subtraction. Once the Initial-Subtract pipeline has finished, the data are ready to be processed by Factor.


%-----------------------------------------------------------
\subsection{Factor tutorials}

This section includes examples of setting up and using Factor. Setting up a Factor run mainly involves creating a parset that defines the reduction parameters. For many of these parameters, the default values will work well, and hence their values do not need to be explicitly specified in the parset. Therefore, only the parameters relevant to each example are described below. For a description of all parameters, see \url{http://www.astron.nl/citt/facet-doc/parset.html}.

\subsubsection{Quick-start}
\label{factor:quick-start}
Below are the basic steps in a typical Factor run on a single machine (e.g., a single CEP3 node).

\begin{enumerate}
\item Collect the input bands and Initial-Subtract sky models (produced as described in sub-section \ref{factor:pre-facet-pipeline}) in a
single directory. The MS names must end in ``.MS'' or
``.ms'':
\begin{verbatim}
[/data/factor_input]$ ls
L99090_SB041_uv.dppp_122298A79t_119g.pre-cal.ms
L99090_SB041_uv.dppp_122298A79t_119g.pre-cal.wsclean_low2-model.merge
L99090_SB041_uv.dppp_122298A79t_121g.pre-cal.ms
L99090_SB041_uv.dppp_122298A79t_121g.pre-cal.wsclean_low2-model.merge
L99090_SB041_uv.dppp_122298A79t_123g.pre-cal.ms
L99090_SB041_uv.dppp_122298A79t_123g.pre-cal.wsclean_low2-model.merge
L99090_SB041_uv.dppp_122298A79t_125g.pre-cal.ms
L99090_SB041_uv.dppp_122298A79t_125g.pre-cal.wsclean_low2-model.merge
\end{verbatim}
\item Edit the Factor parset (described in detail at \url{http://www.astron.nl/citt/facet-doc/parset.html}) to fit
your reduction and computing resources. For example, the parset for a single
machine could be:
\begin{verbatim}
[/data]$ more factor.parset
[global]
dir_working = /data/factor_output
dir_ms = /data/factor_input

[directions]
flux_min_jy = 0.3
size_max_arcmin = 1.0
separation_max_arcmin = 9.0
max_num = 40
\end{verbatim}
\item Run the reduction:
\begin{verbatim}
[/data]$ runfactor factor.parset
\end{verbatim}
\end{enumerate}


\subsubsection{Selecting the DDE calibrators and facets}

Before facet calibration can be done, suitable DDE calibrators must be selected. Generally, a suitable calibrator is a bright, compact source (or a group of such sources).\footnote{Ideally calibrators should be selected that provide roughly uniform coverage of the field with separations less than the typical distance over which the direction-dependent effects vary strongly (this distance depends on the severity of the ionospheric conditions, but is usually less than a degree or so). However, the achievable uniformity of coverage will vary from field to field, depending on the specific distribution of sources.} The DDE calibrators can be provided to Factor in a file (see \url{http://www.astron.nl/citt/facet-doc/directions.html} for details) or can be selected internally. This example shows how to set the parameters needed for internal selection.

Three parameters in the {\tt[directions]} section of the parset are most important for the internal calibrator selection: {\tt flux\_min\_Jy}, {\tt size\_max\_arcmin}, and {\tt separation\_max\_arcmin}. These parameters set the minimum flux density (in the highest-frequency band) and maximum size of the calibrators and the maximum allowed separation that two sources may have to be grouped together as a single calibrator (see \url{http://www.astron.nl/citt/facet-doc/parset.html} for details). The following are good values to use as a starting point:

\begin{verbatim}
[directions]
flux_min_Jy = 0.3
size_max_arcmin = 1.0
separation_max_arcmin = 9.0
\end{verbatim}

It is recommended that you set {\tt interactive = True} under {\tt [global]} while experimenting with the calibrator selection, as Factor will select the calibrators and then pause to allow you to verify them. Now, run Factor (with {\tt runfactor factor.parset}) and once it asks for verification, open one of the {\tt *high2} images generated during the Initial Subtract pipeline in ds9 or casaviewer. Then load the region files located in the {\tt dir\_working/regions/} directory (where {\tt dir\_working} is the working directory defined in your Factor parset). Figure~\ref{F:example_field_layout} shows an example of such an image with the regions overlaid.

\begin{figure}
\begin{center}
 \hspace{-1.5cm}\includegraphics[scale=0.3]{factor_field_layout_example.png}
 \caption{Example of internally generated calibrators and facets. Yellow boxes show the regions to be imaged during self calibration (the inner box shows the region over which sources are added back to the empty datasets) and green regions show the facets. Each direction is labeled twice: once for the calibrator region and once for the facet region. Note the elliptical region that encompasses the faceted area. This region is controlled by the {\tt faceting\_radius\_deg} parameter under [directions] in the parset: inside this radius (adjusted for the mean primary beam shape), full facets are used; outside this radius, only small patches are used that are much faster to process. This radius can be adjusted to limit the size of the outer facets and the region to be imaged. \label{F:example_field_layout}}
\end{center}
\end{figure}

Check that none of the calibrator image regions are overlapping significantly. If they are, try adjusting the above parameters to either group the calibrators together (so that they form a single calibrator) or remove one of them. Also check that no facet is larger than a degree or so. If such large facets are present, try adjusting the above parameters to obtain more calibrators (e.g., by lowering {\tt flux\_min\_Jy}). If the large facets are on the outer edge of the faceted region, you should also consider reducing {\tt faceting\_radius\_deg} to limit their size.

Once the calibrator and facet selection is acceptable, Factor will make a directions file named\\ {\tt factor\_directions.txt} in {\tt dir\_working}. This file can now be edited by hand if desired (e.g., to specify a sky model or clean mask for a calibrator).


\subsubsection{Imaging a target source}\label{S:target_only}

If you are interested only in a single target source, you may want to process just the brightest sources in the field and those nearest the target. Figure~\ref{F:example_target_layout} shows the layout of an example field, with the target source (a galaxy cluster) indicated by the blue circle in facet\_patch\_468. Note that the faceting radius has been reduced substantially to speed up processing (since facets outside of this radius are small patches that run much more quickly than full facets). A few facets (e.g., facet\_patch\_566 and facet\_patch\_273) contain bright sources with artifacts that affect the target. Additionally, although they contain fainter sources, artifacts from the neighboring facets (e.g., facet\_patch\_424) also affect the target significantly. Therefore, a good strategy is to process these facets before processing the target. Note that the lowest possible noise would be obtained by processing the entire field but would require much longer to complete.

\begin{figure}
\begin{center}
 \hspace{-1.5cm}\includegraphics[scale=0.3]{factor_target_layout_example.png}
 \caption{Example of the field layout for a case in which an image is desired only for a single target source (indicated by the blue circle). A region of avoidance was also specified (with a larger radius than the blue circle) that resulted in the curved bulges to the boundary to the north and south of the target. \label{F:example_target_layout}}
\end{center}
\end{figure}

We can limit the processing to the sources above by moving them to the top of the directions file (named {\tt dir\_working/factor\_directions.txt} if generated automatically) and setting {\tt ndir\_process} under {\tt [directions]} in the parset to the number of sources we wish to be processed (including the facet with the target source). Furthermore, it is generally best to put the target facet at the end of those to be processed, so that it benefits from the improvements to the empty datasets from the preceding facets (if you do this, you can set {\tt reimage\_selfcaled = False} under {\tt [imaging]}; see Section~\ref{S:reimaging} for details). Note that you should typically have many more calibrators in the directions file than directions to be processed, as the facets shapes and sizes are determined from all calibrators together and too few can result in very large facets. An example directions file is shown below (note that only the first two columns are shown). In this case, the target facet (facet\_patch\_468) will be the seventh direction to be processed, so we set {\tt ndir\_process = 7} under {\tt [directions]} in the parset. Factor will then stop after the target has been processed.

\begin{verbatim}
# name position ...

facet_patch_566 14h29m38.5962s,44d59m14.0061s ...
facet_patch_499 14h31m36.5221s,44d41m39.6252s ...
facet_patch_424 14h34m38.5876s,44d48m00.415ss ...
facet_patch_379 14h36m04.2603s,44d45m16.2451s ...
facet_patch_273 14h40m14.6989s,45d10m53.6821s ...
facet_patch_573 14h29m33.1473s,44d19m43.2515s ...
facet_patch_468 14h32m44.3536s,44d20m50.7736s ...
......
facet_patch_550 14h30m03.9315s,46d51m44.6965s ...
facet_patch_480 14h32m28.9389s,45d13m49.3407s ...
facet_patch_778 14h20m03.7803s,44d09m40.3293s ...
\end{verbatim}

Additionally, if the target source is diffuse, Factor may not be able to adjust the facet boundaries properly to ensure that it lies entirely within a single facet. To ensure that no facet boundary passes through the target source, you can define the position and the size of an avoidance region under the {\tt [directions]} section of the parset. The {\tt target\_ra}, {\tt target\_dec}, and {\tt target\_radius\_arcmin} parameters determine this avoidance region. An example of such a region for the target above is given below and the resulting facets are shown in Figure~\ref{F:example_target_layout}. Note that the boundaries of the target facet (facet\_patch\_468) are curved to avoid this region.

\begin{verbatim}
[directions]
target_ra = 14h31m59.7s
target_dec = +44d15m48s
target_radius_arcmin = 15
\end{verbatim}


\subsubsection{Peeling bright sources}

In many observations there will be one or more strong sources that lie far outside the FWHM of the primary beam but nevertheless cause significant artifacts inside it. However, because of strong variations in the beam with frequency at these locations, self calibration does not generally work well. Therefore, Factor includes the option to peel these outlier sources. A high-resolution sky model (in makesourcedb format) is needed for this peeling. Factor includes a number of such sky models for well-known calibrators (see \url{https://github.com/lofar-astron/factor/tree/master/factor/skymodels} for a list). If an appropriate sky model is not available inside Factor, you must obtain one and specify it in the directions file. To enable peeling, the {\tt outlier\_source} entry in the directions file must be set to {\tt True} for the source.

It is also possible that a strong source (such as 3C295) will lie inside a facet. Such a source can be peeled to avoid dynamic-range limitations. Peeling will be done if a source exceeds {\tt peel\_flux\_Jy} (set under the {\tt [calibration]} section of the parset) and a sky model is available. After peeling, the facet is then imaged as normal.


\subsubsection{Checking the results of a run}

You can check the progress of a run with the checkfactor script by supplying the Factor parset as follows:

\begin{verbatim}
[/data]$ checkfactor factor.parset
\end{verbatim}

A plot showing the field layout will be displayed (see Figure~\ref{F:checkfactor_example}), with facets colored by their status. Left-click on a facet to see a summary of its state, and press ``c'' to see a plot of the self calibration images and their clean masks. An example of such a plot is shown in Figure~\ref{F:factor_success_example}. For a successful self calibration, you should see a gradual improvement from {\tt image02} to {\tt image42}.

\begin{figure}
\begin{center}
 \hspace{-1.5cm}\includegraphics[scale=0.4]{factor_checkfactor_example.png}
 \caption{Example {\tt checkfactor} plot. \label{F:checkfactor_example}}
\end{center}
\end{figure}

\begin{figure}
\begin{center}
 \hspace{-1.5cm}\includegraphics[scale=0.45]{factor_success_example.png}
 \caption{Example of self calibration images for a successful run. The self calibration process starts with {\tt image02} (the image obtained after applying only the direction-independent solutions from pre-Factor) and proceeds to {\tt image12}, {\tt image22}, etc. In some cases, Factor will iterate a step until no more improvement is seen. These steps are indicated by the ``iter'' suffix.  \label{F:factor_success_example}}
\end{center}
\end{figure}

If self calibration has completed for a direction, select it and press ``t'' to see plots of the differential TEC and scalar phase solutions and ``g'' to see plots of the slow gain solutions. These plots can be useful in identifying periods of bad data or other problems.


\subsubsection{Dealing with failed self calibration}\label{S:failure}

Self calibration may occasionally fail to converge for a direction, causing to Factor to skip the subtraction of the corresponding facet model. An example in which self calibration failed is shown in Figure~\ref{F:factor_failure_example}. Note how the image noise does not improve much in this case during the self calibration process. While Factor can continue with the processing of other directions and apply the calibration solutions from the nearest successful facet to the failed one (depending on the value of the {\tt exit\_on\_selfcal\_failure} option under {\tt [calibration]} in the parset; see \url{http://www.astron.nl/citt/facet-doc/parset.html} for details), it is generally better to stop Factor and try to correct the problem.  In these cases, a number of options are available that may help correct the failed self calibration: turn on multi-resolution self calibration, turn on adaptive thresholding, or supply a custom clean mask.

\begin{figure}
\begin{center}
 \hspace{-1.5cm}\includegraphics[scale=0.35]{factor_failure_example.png}
 \caption{Example of self calibration images for an unsuccessful run in which the automatic clean mask included strong artifacts. In this particular case, the problem was solved by setting {\tt selfcal\_adaptive\_threshold = True} under {\tt [imaging]} in the parset.  \label{F:factor_failure_example}}
\end{center}
\end{figure}

Multi-resolution self calibration involves imaging at decreasing resolutions, from 20 arcsec to full resolution ($\approx 5$ arcsec). This process can stabilize clean in the presence of calibration errors. An example of such a situation is shown in Figure~\ref{F:factor_multires_example}. Multi-resolution self calibration can be enabled by setting {\tt multires\_selfcal = True} under {\tt [calibration]} in the parset.

\begin{figure}
\begin{center}
 \hspace{-1.5cm}\includegraphics[scale=0.25]{factor_multires_example.png}
 \caption{Example of self calibration images at the same stage of reduction for an unsuccessful run (left) in which an incorrect source structure is present (the bright sources both have ``L''-shaped morphologies) and the successful run (right) in which multi-resolution self calibration was used. \label{F:factor_multires_example}}
\end{center}
\end{figure}

Adaptive thresholding can help the automatic masking in the presence of strong artifacts by adjusting the thresholds depending on the maximum negative pixel value. An example of such a situation is shown in Figure~\ref{F:factor_failure_example}. Adaptive thresholding can be enabled by setting {\tt selfcal\_adaptive\_threshold = True} under {\tt [imaging]} in the parset.

In some cases, usually when the calibrator has a complex structure, it may be necessary to supply a clean mask made by hand. Such a clean mask can be made by opening one of the self calibration images (e.g., {\tt *image01.image}; see \url{http://www.astron.nl/citt/facet-doc/operations.html} for details of the {\tt facetselfcal} output) in casaviewer and drawing a region that encloses the source emission but excludes any artifacts. Save the region in CASA format to a file and specify the full path to this file in the appropriate entry of the directions file (named {\tt dir\_working/factor\_directions.txt} if generated automatically). The full path must then added to the {\tt region\_selfcal} entry in the directions file. Note that a supplied mask will be unioned with the interally generated one.

To restart self calibration for the failed direction, you will need to reset it. See Section~\ref{S:resumming_factor} below for instructions on how to reset a direction.


\subsubsection{Reimaging at the end of a run}\label{S:reimaging}

Reimaging of facets is desirable for two reasons: (1) to improve the noise in facets done at the beginning of a run when the empty datasets still contained significant artifacts from poorly subtracted sources in neighbouring facets and (2) to make images with different resolutions, weights, etc. (e.g., to increase sensitivity to diffuse, extended emission).

By default, Factor will only reimage facets for which self calibration was successful. These facets are reimaged once all other directions have been processed, allowing them to benefit from the improvements to the empty datasets from other directions processed later (directions for which self calibration was not done are imaged at the end and so already use the improved data). Figure~\ref{F:factor_reimage_example} shows a comparison of a facet image before and after reimaging.

\begin{figure}
\begin{center}
 \hspace{-1.5cm}\includegraphics[scale=0.4]{factor_reimage_example.png}
 \caption{Example of a facet image: before reimaging (left) and after reimaging (right), showing the improvement due to the later processing of neighboring facets. \label{F:factor_reimage_example}}
\end{center}
\end{figure}

Note that if you are interested in only a single target source that was processed at the end (and hence will not benefit from reimaging), you should disable this reimaging by setting {\tt reimage\_selfcaled = False} under {\tt [imaging]}. See Section~\ref{S:target_only} for an example of such a run.

To reimage all facets with different parameters, set one or more of the facet parameters in the parset under {\tt [imaging]}: {\tt facet\_cellsize\_arcsec}, {\tt facet\_robust}, {\tt facet\_taper\_arcsec}, and \\ {\tt facet\_min\_uv\_lambda}. These parameters can be lists, with one set of images for each entry. For example, the following settings will make two images per facet, one at full resolution (1.5 arcsec cellsize, robust of -0.25, and no taper) and one at lower resolution (15 arcsec cellsize, robust of -0.5, and a 45 arcsec taper):

\begin{verbatim}
[imaging]
facet_cellsize_arcsec = [1.5, 15.0]
facet_robust = [-0.25, -0.5]
facet_taper_arcsec = [0.0, 45.0]
\end{verbatim}

This run will produce two output directories: {\tt dir\_working/results/facetimage/} (with the full-resolution images) and {\tt dir\_working/results/facetimage\_c15.0r-0.5t45.0u0.0/} (with the lower-resolution images).


\subsubsection{Resuming and resetting a run}\label{S:resumming_factor}

If a Factor run is interrupted, it can be resumed by simply running it again with the same command. Occasionally, however, you will want to change some settings (e.g., to supply a clean mask for self calibration) or something will go wrong that prevents resumption. In these cases, you can reset the processing for a direction with the ``-r'' flag to {\tt runfactor}. For example, to reset direction facet\_patch\_350, the following command should be used:

\begin{verbatim}
runfactor factor.parset -r facet_patch_350
\end{verbatim}

