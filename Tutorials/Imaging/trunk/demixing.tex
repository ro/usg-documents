\svnInfo $Id: demixing.tex 7697 2011-05-12 20:26:05Z shulevski $



\section[Demixing]{Demixing\footnote{This chapter is maintained by Aleksandar Shulevski ({\tt a.shulevski[at]astro[dot]rug[dot]nl}) and Roberto F. Pizzo ({\tt pizzo[at]astron[dot]nl})}}
\label{sec:demixing}

%------------------------------------------------------------------------------------------

\subsection{Introduction}
\label{introduction}


In this section we will describe the so-called "demixing" procedure, which should be applied to all LBA (and sometimes HBA) data sets to remove from the target visibilities the interference of the strongest radio sources in the sky, as the A-team (CasA, CygA, VirA, etc...). Removing this contribution is essential to make it possible to properly calibrate the target source. To understand whether demixing is needed for your data, you are suggested to inspect the elevation of the A-team sources during your observation. By combining this information with the distance of the A-team from your target, you can have a clear picture of how critical this procedure is to improve the calibration and imaging of your data. A script has been developed that quickly gives you these overview. It is called {\tt plot\_Ateam\_elevation.py} and can be found in {\tt /opt/cep/tools/cookbook}.


All the files necessary to perform the demixing manually are located in {\tt /globaldata\\/COOKBOOK/Demixing}. Note that the demixing can also be run automatically by using a script developed by Ger van Diepen. A description on how to use this script is reported in Section~\ref{automaticdemixing}, however you are suggested to try the manual procedure at least once, so that you understand what it consists of.

Note that demixing must be performed on data that has not been averaged in time nor in frequency and before any calibration. If you realize that demixing is needed for your data, you should request the Radio Observatory ({\tt sciencesupport[at]astron[dot]nl}) to provide you with an unaveraged sub band of your dataset. Moreover, be aware that the demixing alghoritm is capable of removing the A-team sources from your dataset only if they are outside a critical radius from your target. Tests performed by the commissioners have shown that the minimum distance scales with the observing frequency such that  the higher is the observing frequency, the smaller is the critical radius. This  radius is $\sim$21 degrees at 60 MHz. The cases in which the A-team is too close to the target source can be treated with or the DPPP-demixing (Sect.~\ref{dppp:demixing}) or Sageal (Sect.~\ref{sec:sagecal}).

%------------------------------------------------------------------------------------------

\subsection{Shift Phase Center}
\label{shiftphasecenter}

Make sure that you are using the latest successful software build (run {\tt use LofIm $<$day$>$}), then copy the file  {\tt DPPP.parset} from {\tt /globaldata/COOKBOOK/Demixing} into your working directory. This looks like the following:
%
\begin{verbatim}
msin = <raw_data>.MS
msin.autoweight = True
msin.startchan = 2
msin.nchan = 60
msout = <DPPP_processed_data>.MS
steps = [preflag1]
preflag1.type=preflagger
preflag1.corrtype=auto
\end{verbatim}
%
Edit the {\tt msin} to point to the uncompressed data set you want to demix, and give to {\tt msout} a name corresponding to the new dataset that you will process further. We are assuming here that your dataset has 64 channels, from which the first and last two channels will be removed. In the case you are dealing with a MeasurementSet with different characteristics, change these numbers accordingly. Be aware that the demixing procedure works best if you have at least 64 channels in your initial dataset.\\

Run DPPP: 

\begin{verbatim}
> DPPP DPPP.parset
\end{verbatim}

Successively, copy the script {\tt shiftphasecenter.py} into your working directory and edit it:
%
\begin{verbatim}
msname = '<DPPP_processed_data>.MS'
targets = ( ('CasA', 6.123487680622104, 1.0265153995604648),
            ('CygA', 5.233686575770755, 0.7109409582180791) )


N_channel_per_cell = 60
N_time_per_cell = 10
\end{verbatim}
%
{\tt msname} should be the name of the data set you obtained in the previous step. The list of sources to be demixed contains tuples in the form {\tt (name, ra, dec)}, where {\tt ra}  and {\tt dec}  are expressed in radians. You can also add more bright sources (e.g. Vir A, Tau A) if they are above the horizon and at an elevation above 15 degrees. In this case, note that demixing will then take longer to finish (even a few hours longer). Moreover, remember that adding too many sources to the list (especially weaker ones, which do not contribute that much to the interference) can actually increase the noise in your final target dataset data set. Furthermore, note that the demixing is not effective for sources that are close to the target (closer than 20 degrees).\\

Now run the script {\tt shiftphasecenter.py}: 
%
\begin{verbatim}
> python shiftphasecenter.py
\end{verbatim}

%------------------------------------------------------------------------------------------

\subsection{Averaging}
\label{averaging}

By running  {\tt shiftphasecenter.py} you have produced a number of new data sets (one per source to be demixed) with names having format $<$prefix\_name$>$.MS, where {\tt name} is the list of names of sources we have specified in the script. Now, we will average each of these new data sets using DPPP. Copy the parset {\tt NDPPP\_avg\_CasA.parset} from {\tt /globaldata/COOKBOOK/Demixing} into your working directory, rename it to correspond to the name of the data set you are going to average and edit it:
%
\begin{verbatim}
msin = <prefix_name>.MS
msout = <prefix_name>_avg.MS
steps = [avg]
avg.type = averager
avg.timestep = 10
avg.freqstep = 60
\end{verbatim}
%
edit {\tt msin} and {\tt msout} according to the data set you are working on. The {\tt freqstep} parameter corresponds to the choice of the number of channels you decided to keep in the previous averaging step. {\tt timestep} is chosen empirically, but is should NOT be the same as the timeslot size in our MS (obtained e.g. by running {\tt msoverview}on your original dataset). The value should be large enough to encompass several timeslots. It is best if you do not edit this entry for now.

Proceed now with averaging the data:
%
\begin{verbatim}
> DPPP NDPPP_avg_CasA.parset
\end{verbatim}
%
Repeat this step for each demixed data set AND the target (copy and edit the corresponding parsets). Parsets for CasA, CygA, HydA are provided in {\tt /globalhome/COOKBOOK/Demixing}, for other sources you will have to modify the existing ones. The target data set to be averaged is \\ $<$DPPP\_processed\_data.MS$>$, which was the output of the first DPPP step.


%------------------------------------------------------------------------------------------

\subsection{Demix and Predict}
\label{demixandpredict}

Copy the script {\tt demixing.py} from {\tt /globaldata/COOKBOOK/Demixing} into your working directory and edit it:
%
\begin{verbatim}
msname = '<DPPP_processed_data>.MS'

avg_msnames = ['<DPPP_processed_data>_avg.MS',
               '<prefix_name_1>_avg.MS',
               '<prefix_name_2>_avg.MS']

N_channel_per_cell = 60
N_time_per_cell = 10
N_pol = 4
\end{verbatim}
%
{\tt msname} is the data set obtained in the first DPPP pre-flagging step (the target data set), and {\tt avg\_msnames} is a list containing the names of the averaged data sets (the target plus all the demixed sources). If you are working with more than 60 frequency channels, you should change the scripts accordingly.\\

Run the demixing script: 
%
\begin{verbatim}
> python demixing.py
\end{verbatim}
%
The script will produce an additional data set with the suffix {\tt \_avg\_dem} in the name for every data set that is specified in the {\tt avg\_msnames} entry.

Now, we have to predict the visibilities of these demixed data sets using BBS. The data will be calibrated and predicted data will be put in the MODEL DATA column. This predict step is needed only for the demixed source data sets.\\

First, copy the relevant BBS parsets (ex. bbs\_CasA.parset, etc.) and the A-team clean component sky model {\tt Ateam\_LBA\_CC.skymodel} from {\tt /globaldata/COOKBOOK/Demixing} into your working directory, then create the gds files:

\noindent \begin{verbatim}
makevds <clusterdesc> <prefix>_avg_dem.MS <A-team_source>.vds
\end{verbatim}
\noindent \begin{verbatim}
combinevds <A-team_source>.gds <A-team_source>.vds
\end{verbatim}
%
Now run BBS:
%
\noindent \begin{verbatim}
calibrate -f -key <target_key> --cluster-desc <clusterdesc> --db ldb001 --db-user
 <username> <A-team_source>.gds <A-team_source_parset>.parset 
 Ateam_LBA_CC.skymodel <working_dir>
\end{verbatim}

Do this for every (A-team) source you want to demix.

%------------------------------------------------------------------------------------------

\subsection{Subtraction}
\label{subtraction}

We will now subtract the predicted data of the demixed sources from the averaged data of the target field. Copy the script {\tt subtract\_from\_averaged.py} from {\tt /globaldata/COOKBOOK/Demixing} into your working directory and edit it:

\begin{verbatim}
# The averaged data, including the contribution of other sources
msname = '<DPPP_processed_data>_avg.MS'

# Name of the table containing the mixing matrix
mixingname = 'mixing'

# Names of the measurement sets with predicted data in the MODEL_DATA column
# The predicted data is going to be subtracted from the averaged data
mspredictnames = ( '<prefix_name_1>_avg_dem.MS', '<prefix_name_2>_avg_dem.MS')

# Output measurment set
msnameout = '<target>_sub.MS'
\end{verbatim}

{\tt msname} specifies the averaged target data set, {\tt mspredictnames} is the list of the averaged and demixed data sets which have gone through the BBS predict procedure in the previous step, and {\tt msout} specifies the name of the output data set, i.e. the data containing the target visibilities from which the interference from the off-axis sources has been removed.\\

Run the script {\tt subtract\_from\_averaged.py}

\begin{verbatim}
> python subtract_from_averaged.py
\end{verbatim}

%------------------------------------------------------------------------------------------

\subsection{Calibration of the Demixed Data Set}
\label{calibrationofthedemixeddataset}

Finally, you can calibrate the target data set:\\

\noindent \begin{verbatim}
makevds <clusterdesc> <target_sub>.MS <target_sub>.vds
\end{verbatim}
\noindent \begin{verbatim}
combinevds <target_sub>.gds <target_sub>.vds
\end{verbatim}
\noindent
 \begin{verbatim}
calibrate -f --key <calib_key> --cluster-desc <clusterdesc> --db ldb001 
--db-user <username> <target_sub>.gds <target_parset>.parset 
<target_skymodel>.skymodel <working_directory>
\end{verbatim}

After the target calibration is done, you can copy the script {\tt covariance2weight.py} from \\{\tt /globaldata/COOKBOOK/Demixing} into your working directory  and run it on the calibrated target data set before you image the data:\
%
\begin{verbatim}
> python covariance2weight.py <target_sub>.MS
\end{verbatim}
%
This will copy the covariance matrix of the calibration solutions to the weight column of the MeasurementSet so that during the imaging the baselines with bad calibration solutions will be weighed down. This is an irreversible procedure, so we suggest you to make a copy of the calibrated target data set before running the script. Alternatively, you may choose to not run the script, but flag the data manually (using e.g. CASA) and then image them.

%------------------------------------------------------------------------------------------

\subsection{Automatic Demixing}
\label{automaticdemixing}

\begin{figure*}
\begin{center}
    \includegraphics[scale=0.5]{double_raw.png}
  \caption{The raw visibilities of L26805.}
  \label{doubleraw}
  \end{center}
\end{figure*}
%
%
You can do all of the previous steps in an automated way by using a script developed by Ger van Diepen\footnote{It has been adapted from a previous version created by Neal Jackson and Reinout van Weeren}. In {\tt data/scratch/'user'} on the lce node where you are working, type
%
\begin{verbatim}
> /home/diepen/scripts/do_demixing.py
\end{verbatim}

and read the options that appear on screen.\\
%
%
\begin{figure*}
\begin{center}
    \includegraphics[scale=0.35]{double_ateam_elev.png}
  \caption{The elevation of the A-team and other low frequency bright sources during the target observation.}
  \label{doubleateamelev}
  \end{center}
\end{figure*}
%
%
Note that
\begin{itemize}
\item running the demixing is a very compute intensive process! DO NOT run more than one demixing at a time per lce node;
\item be aware that the demixing requires also a lot of disk space to tun ($\sim$100 GB, but it obviously depends on the size of the input MeasurementSet). Remember to remove the intermediate products as soon as the process is completed;
\item run the demixing only if you know what you are doing;
\item before running multiple intensive processes on more lce nodes, please contact the Radio Observatory ({\tt sciencesupport[at]astron[dot]nl}).
\end{itemize}
%
%
\begin{figure*}
\begin{center}
    \includegraphics[scale=0.6]{Cas_Adem.png}    
  \caption{BBS solutions towards CasA during the target observation.}
  \label{casdem}
  \end{center}
\end{figure*}
%
\begin{figure*}
\begin{center}
    \includegraphics[scale=0.38]{Cyg_Adem.png}    
  \caption{BBS solutions towards CygA during the target observation.}
  \label{cygdem}
  \end{center}
\end{figure*}


Demixing can be performed in an automated way for all sub bands of a LOFAR observation through the Imaging Pipeline. It is not run by default, but upon users request. Such a request should also specify the demixing strategy (sources to demix) and should be sent to the science support group ({\tt sciencesupport[at]astron[dot]nl}).


%------------------------------------------------------------------------------------------

\subsection{Demixing example}
\label{demixingexample}


In this section we will illustrate a demixing example for an HBA observation performed in May 2011({\tt L26805}) of the double-double radio galaxy 1834+62. 
Figure~\ref{doubleraw} shows the amplitude vs time plot of the raw visibilities. From here, it is clear that the second half of the observation is characterized by a big bump, which is hard to justify in terms of target structure. To understand its origin, we plot the elevation and the distance of the A-team sources with respect to the pointing of our target (Fig.~\ref{doubleateamelev}) by using the script {\tt plot\_Ateam\_elevation.py}\footnote{It can be found in {\tt /opt/cep/tools/cookbook}}.  


%
%
\begin{figure*}[!h]
\begin{center}
    \includegraphics[width=9cm]{double_afterdemix.png}
  \caption{The target visibilities after demixing.}
  \label{doubleafterdemix}
  \end{center}
\end{figure*}

%
From this plot we can notice that during our observation CygA and CasA are very high in elevation and pretty close to our target (24 and 33 degrees respectively). Their interference with the target visibilities could be the cause of the bump in the data. The remaining sources, which are also intrinsically less bright, are low in elevation and considerably far from our target, therefore they are likely not affecting the target field.  
Demixing was used to subtract CasA and CygA from the target visibilities. In Figures~\ref{cygdem} and \ref{casdem} we show the BBS solutions for the directions towards the interfering A-team sources. 
The fact that the solutions have a good S/N (the phases, in particular, can be tracked pretty well) indicates that a strong signal was seen by LOFAR towards these directions and it can be well removed from the target visibilities. 
Figure~\ref{doubleafterdemix} shows the amplitude vs time plot for the target after demixing. The big bump present in Figure~\ref{doubleraw} completely disappeared, consequently also the contribution of CasA and CygA. 
%
\
 
