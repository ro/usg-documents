\svnInfo $Id: dpppcalibrate.tex 8671 2011-11-04 22:31:30Z dijkema $
% !TEX root = lofar_imaging_cookbook.tex

\section[Gain calibration with DPPP]{Gain calibration with DPPP\footnote{This section is maintained by Tammo Jan Dijkema ({\tt dijkema[at]astron[dot]nl}).}}
\label{sec:dpppcalibration}

The most common gain calibration procedures can be \href{http://www.lofar.org/operations/doku.php?id=public:user_software:documentation:ndppp#gaincal}{performed}\footnote{\url{http://www.lofar.org/operations/doku.php?id=public:user_software:documentation:ndppp\#gaincal}} with DPPP. Formerly BBS (Appendix~\ref{BBS}) was the standard calibration tool, but for the supported calibration scenarios DPPP is at least 10 times faster. For calibration scenarios that are not (yet) supported in DPPP, is may be necessary to resort to BBS, see (Appendix~\ref{BBS}).

The calibration problem that DPPP can solve is the following: find a set of Jones matrices $\{\mathbf{G}_p\}$ (one for every station $p$) which corrects the measured visibilities $\mathbf{V}_{pq}$ to closely resemble the model visibilities $\mathbf{M}_{pq}$ (for all baselines $pq$), i.e.\ minimize
\begin{equation}\label{eq:dpppcal}
\| \mathbf{V}_{\!pq}-\mathbf{G}_{p}\mathbf{M}_{pq}\mathbf{G}_{q}^H \|
\end{equation}
The matrices $\mathbf{G}$ will be referred to as \emph{gain matrices} although they correct for more than just electrical gains.

\subsection{Calibration variants}
There are various options to restrict the shape of $\mathbf{G}$. The main difference is whether to solve for the amplitude of the solutions or only for the phase. Also, the number of free parameters can be restricted.\\

\begin{tabular}{*1{>\arraybackslash p{.4\textwidth} l l }}
\hline
\textbf{Shape} & \textbf{Calibration type\hspace{3em}} & \textbf{Free parameters}\\
\hline \\
\vspace{-1.3cm}\[
\mathbf{G}_{\textcolor{blue}{p}} = \left( 
\begin{array}{cc}
A_{\textcolor{black}{xx}}^{\textcolor{blue}{(p)}}e^{\phi_{\textcolor{black}{xx}}^{\textcolor{blue}{(p)}}} & 
A_{\textcolor{black}{xy}}^{\textcolor{blue}{(p)}}e^{\phi_{\textcolor{black}{xy}}^{\textcolor{blue}{(p)}}} \\
A_{\textcolor{black}{yx}}^{\textcolor{blue}{(p)}}e^{\phi_{\textcolor{black}{yx}}^{\textcolor{blue}{(p)}}} &
A_{\textcolor{black}{yy}}^{\textcolor{blue}{(p)}}e^{\phi_{\textcolor{black}{yy}}^{\textcolor{blue}{(p)}}} 
\end{array}
\right)
\] & \texttt{fulljones} & 8 \\
%%%%%%
\hline \\
\vspace{-1.3cm}\[
\mathbf{G}_{\textcolor{blue}{p}} = \left( 
\begin{array}{cc}
A_{\textcolor{black}{xx}}^{\textcolor{blue}{(p)}}e^{\phi_{\textcolor{black}{xx}}^{\textcolor{blue}{(p)}}} & 
0 \\
0 &
A_{\textcolor{black}{yy}}^{\textcolor{blue}{(p)}}e^{\phi_{\textcolor{black}{yy}}^{\textcolor{blue}{(p)}}} 
\end{array}
\right)
\]\vspace{-0.25cm} & \texttt{diagonal} & 4 \\
%%%%%%
\hline \\
\vspace{-1.3cm}\[
\mathbf{G}_{\textcolor{blue}{p}} = \left( 
\begin{array}{cc}
e^{\phi_{\textcolor{black}{xx}}^{\textcolor{blue}{(p)}}} & 
0 \\
0 &
e^{\phi_{\textcolor{black}{yy}}^{\textcolor{blue}{(p)}}} 
\end{array}
\right)
\]\vspace{-0.25cm} & \texttt{phaseonly} & 2\\
%%%%%%
\hline \\
\vspace{-1.3cm}\[
\mathbf{G}_{\textcolor{blue}{p}} = \left( 
\begin{array}{cc}
\,e^{\phi^{\textcolor{blue}{(p)}}} & 
0 \\
0 &
\,e^{\phi^{\textcolor{blue}{(p)}}} 
\end{array}
\right)
\]\vspace{-0.25cm} & \texttt{scalarphase} & 1\\
\hline
\end{tabular}

\subsection{Make a skymodel}
To perform a calibration, you need a sky model (see Section~\ref{bbs:source-catalog}). You can get one from the catalogs in \texttt{gsm.py} (see Section~\ref{bbs:gsm}), or make your own. To make the sky model (in text format) readable by DPPP, it needs to be converted from plain text to a \emph{sourcedb}. That is done with the program \texttt{makesourcedb}. Usually the sourcedb is called 'sky' and copied into the data set you're reducing. If you put it elsewhere, it is customary to give it the extension \texttt{.sourcedb}.

\texttt{makesourcedb in=my.skymodel out=L123.MS/sky format='<'}

The part \texttt{format='<'} is necessary to convince makesourcedb that the format is given by the first line in the file. You can use the program \texttt{showsourcedb} to verify the output sourcedb.

It is also possible to calibrate on model visibilities -- in this case no sky model is necessary. See the online documentation of DPPP, the parameter to look for is \texttt{usemodelcolumn}.

\subsection{Calibration}
To perform a phase only calibration, the following parset can be given to DPPP.
\begin{lstlisting}
msin=L123.MS
msout=
steps=[gaincal]
gaincal.sourcedb=L123.MS/sky
gaincal.parmdb=L123.MS/instrument
gaincal.caltype=phaseonly
gaincal.solint=2
gaincal.nchan=0
\end{lstlisting}

The part \texttt{solint=2} specifies that we only want one solution for every two time intervals. This can improve the signal to noise ratio -- but one should have a physical argument that tells that the solutions do not change within the solution interval. The part \texttt{nchan=0} tells that one solution is computed that is assumed constant over the entire band. Setting it to e.g. \texttt{nchan=1} will compute a separate solution for each channel (again, at the cost of signal to noise).

The parset above performs a phase only calibration, and stores the calibration result in the \emph{parmdb} (parameter database) \texttt{L123.MS/instrument}. This file will be created if it is not there yet. If it does exist, the solution in it will be overwritten. Note that it is a convention to save the calibration tables in a file (casa table) called \texttt{instrument} in the data set being reduced. If you store it outside the data set, the convention is to give it the extension \texttt{.parmdb}.

The solution table can (and should!) be inspected with \texttt{parmdbplot.py}, see Section~\ref{bbs:inspect-solutions}. Note that this calibration step does not yet change the visibilities. To perform complex operations on the solutions, like smoothing them, LoSoTo can be used, see Chapter~\ref{losoto}.

\subsection{Applying solutions}\label{dpppcalibrate:applycal}
To apply the calibration solutions in DPPP, the step \emph{applycal} can be used. The following parset applies the solutions that were obtained by gaincal. Note that currently. the solution table is only written at the very end of DPPP, so that it is not possible to solve and apply the solutions in the same run of DPPP.

\begin{lstlisting}
msin=L123.MS
msout=.
msout.datacolumn=CORRECTED_DATA
steps=[applycal]
applycal.parmdb=L123.MS/instrument
\end{lstlisting}

It is a convention to write the output to the column \texttt{CORRECTED\_DATA}, to avoid changing the original data column \texttt{DATA}.

\subsection{Transferring solutions and the beam}
When transferring solutions from a calibrator to a target, the sensitivity of the beam across the sky needs to be taken into account: the instrument does not have the same sensitivity at the position of the calibrator field as at the position of the target field. You can compensate for this by using a model for the LOFAR beam. Effectively, then instead of equation~\ref{eq:dpppcal} the following equation is solved for $\{\mathbf{G}_p\}$:
\begin{equation}
\| \mathbf{V}_{\!pq}-\mathbf{G}_{p}\mathbf{B}_p\mathbf{M}_{pq}\mathbf{B}_q^H\mathbf{G}_{q}^H \|
\end{equation}

In the case of transferring solutions, the calibration is usually about the amplitude and the calibration type should be either \texttt{diagonal} or \texttt{fulljones}.

A parset for calibrating on the calibrator field, taking the beam into account, is given below:

\begin{lstlisting}
msin=L123.MS
msout=
steps=[gaincal]
gaincal.sourcedb=L123.MS/sky
gaincal.parmdb=L123.MS/instrument
gaincal.caltype=diagonal
gaincal.usebeammodel=true
\end{lstlisting}

\subsection{Applying the beam}

When applying the solutions of the calibrator to the target, you should probably not apply the beam, so that another round of calibration is possible afterwards. Only after you are done with all calibration, the beam should be applied (just before imaging). Applying the beam is possible with

\begin{lstlisting}
msin=L123.MS
msin.datacolumn=CORRECTED_DATA
msout=.
msout.datacolumn=CORRECTED_DATA
steps=[applybeam]
\end{lstlisting}

