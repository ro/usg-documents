\documentclass[a4paper,10pt]{scrartcl}
\usepackage{amsmath,amssymb,usg}

%opening

\begin{document}

\title{LOFAR Data Access Library (DAL) User Manual}
\author{Joseph Masters, Lars B\"ahren}
\date{\small\today}
\maketitle

\tableofcontents

\newpage

\section{Introduction}

The purpose of this document is to make it easier to work with the LOFAR Data
Access Library (DAL). This is not intended to be a reference to all the class
methods. For that purpose see the online documentation \cite{doxygen}.

The DAL is both a \cpp\ library (libdal) and a Python module (pydal). The \cpp\
interface is intended primarily, but not exlusively, for developers. Likewise,
the python module is intended primarily, but not exlusively, for astronomers.
The python interface is, and will likely remain, a subset of the methods
available in \cpp.

The \textit{primary} classes in the DAL are: \texttt{dalDatast},
\texttt{dalGroup}, \texttt{dalTable}, \texttt{dalColumn}, \texttt{dalData},
\texttt{dalArray} and \texttt{dalFilter}. These are the generic classes used to
operate on a file of an unspecified type.

A set of \textit{supporting} classes also exist in the DAL, but they are
generally not interesting to the developer. There purpose is to provide common
methods used by the primary classes mentioned above. These supporting classes
are: \texttt{Common}, \texttt{Database}, and \texttt{HDF5Common}.

Finally, the DAL includes \textit{format-specific} classes. These classes are
designed to work with a single type of LOFAR data product such as beam-formed or
transient buffer board data. The format-specific classes are: \texttt{BFRaw},
\texttt{BeamFormed}, \texttt{BeamGroup}, \texttt{BeamSubband},
\texttt{Enumerations}, \texttt{TBB}, \texttt{dalLopesEvent} and
\texttt{lopesevent}.

This document describes only the \textit{primary} classes.

\subsection{A Note on the Examples}

Please keep in mind that Python examples assume you imported the pydal module:
\begin{verbatim}
   $ python
   > from pydal import *
\end{verbatim}

\cpp\ examples assume the inclusion of dal.h:
\begin{verbatim}
   #include "dal.h"
\end{verbatim}

For simplicity, none of the examples include error checking on the return
values.

\section{Motivation}

The purpose of the DAL is to abstract the data product implementation details
from the user.  In this way, the user can generally ignore the nuances of the
underlying file format and focus instead on the high level objects reperesented
therin (i.e. tables, arrays, attributes, etc.).  Although significant, the
storage mechanism is not scientifically interesting to the typical researcher.

\section{File Types}

Officially, the DAL supports three file types: HDF5, CASA Tables and FITS.  At
present, each of these file types have varying levels of support.

\subsection{HDF5}

Hierarchical Data Format Version 5 (HDF5), is a relatively recent format
designed with particular attention to efficient operation on extremely large
data sets.  HDF5 is the intended format for LOFAR data products.  The reasons
for this decision are given in {\em ``Proposed Data Formats``} \cite{Cadot}.

HDF5 is organized much like a unix file system.  The internal structure consists
of a root group, '/', which may recursively contain subgroups.  Each of these
groups may, in turn, contain other groups or n-dimensional arrays of simple or
complex datatypes.  HDF5 tables are particular case of a HDF5 array and thus are
treated in a similar manner.  Almost all objects in a HDF5 file can hold
associated metadata in the form of an attribute.  For an abbreviated list of
HDF5 features see {\em ``HDF5 vs. CASA Tables''} \cite{Diepen}.  For a complete
list see the hdfgroup website \cite{hdfgroup}.

HDF5 files can be both read and written by the DAL.

\subsection{CASA Tables}

CASA Tables in general, and Measurement Sets in particular, are the primary data
product of the Westerbork Synthesis Radio Telescope (WSRT).  As WSRT is also
based at ASTRON (where there is a large community of experienced CASA users),
there is naturally a strong interest and advantage in building upon this
expertise for LOFAR.

CASA tables can currently be read, but not written, by the DAL.

\subsection{FITS}

FITS is the standard astronomical image format.  Among its advantages for
inclusion in the DAL is its widespread use and extensive toolset.  Among its
limitations is the fact that it was not designed for large and complex datasets.

FITS is the lowest of priority of the three supported formats and currently has
almost no actual support in the DAL.

\section{Library Structure}

\subsection{Overview}

The DAL is designed, generally, around a superset of the features of the
underlying formats. CASA supports a set of hierarchical tables, arrays and
attributes. FITS supports multiple tables or arrays and associated keywords.
HDF5 supports all of the above, including groups, as well as metadata in the
form of attributes.

In addition to generic support of different file types, the DAL also supports
the LOFAR-specific support of data products. For example, the beam-formed
classes (BFRaw, BeamFormed, BeamGroup and BeamSubband) are intended for use only
with the LOFAR Beam-formed data product. The transient buffer board class (TBB)
is likewise meant to be used only with its associated LOFAR data product.

The code examples below are meant as a guide to using the non-LOFAR-specific
parts of the library. 

\subsection{Primary classes}

\begin{description}
\item[dalDataset]
  At the highest level, a LOFAR data product is represented by the dalDataset
  object. A dalDataset may contain any number of dalGroup, dalTable and dalArray
  objects. A dalDataset represents the file as a whole and may have associated
  metadata.
  
\item[dalGroup]
  At the moment, the group object within the DAL is unique to HDF5. A group may
  contain sub\-groups, arrays or tables. Additionally, attributes can be attached
  to a group object.
  
\item[dalTable]
  DAL tables can be located at the root level of a file, or within a group. A
  dalTable consits of dalColumn objects and may have associated attributes.
  
\item[dalColumn]
  DAL columns are the components of the dalTable object. The dalData object holds
  the actual data of each column.
  
\item[dalArray]
  DAL arrays can be located at the root level of a file, or within a group. A
  dalArray may have attribute data.
  
\item[dalData]  
  The dalData object holds the actual data from a dalColumn. This allows the
  column information to be stored seperately from the information about the layout
  of the data.
  
\item[dalFilter]
  The filter object can be applied to CASA tables before opening. In the future,
  the same syntax may become available for HDF5 and FITS.
\end{description}

\subsection{Format-specific classes}

\begin{description}
\item[BFRaw] High-level interface between raw beam-formed data and the DAL. 
\item[BeamFormed] High-level interface between beam-formed data and the DAL.
\item[BeamGroup] High-level interface between beam-formed data and the DAL.
\item[BeamSubband] High-level interface between beam-formed data and the DAL.
\item[TBB] High-level interface between TBB data and the DAL.
\end{description}

%% ------------------------------------------------------------------------------
%% Examples

\section{Examples}

\subsection{C++}

\begin{itemize}
  
\item
  Open and read CASA MeasurementSet data:
\begin{verbatim}

\end{verbatim}
  
\item 
  Create and close a new HDF5 file:
\begin{verbatim}
  dalDataset * ds = NULL;
  ds = new dalDataset( FILENAME, "HDF5" );

  ds->close();

  delete ds;
\end{verbatim}
  
\item
  Open and close an existing file:
\begin{verbatim}
  dalDataset ds;

  ds.open( FILENAME );
  ds.close();
\end{verbatim}
  
\item
  Set attributes in a (HDF5) dataset:
\begin{verbatim}
  dalDataset ds;
  ds.open( FILENAME );

  std::string sval = "string test value";
  ds.setAttribute( "STRING_ATTR", sval );

  std::vector<std::string> svals;
  svals.push_back("string");
  svals.push_back("vector");
  svals.push_back("test");
  ds.setAttribute_string( "STRING_ATTRS", svals );

  int ival = 1;
  ds.setAttribute( "INT_ATTR", &ival );

  int ivals[] = { 1, 2, 3 };
  ds.setAttribute( "INT_ATTRS", ivals, 3 );

  uint uival = 2;
  ds.setAttribute( "UINT_ATTR", &uival );

  uint uivals[] = { 1, 2, 3};
  ds.setAttribute( "UINT_ATTRS", uivals, 3 );

  float fval = 3.0;
  ds.setAttribute( "FLOAT_ATTR", &fval );

  float fvals[] = { 1.0, 2.0, 3.0 };
  ds.setAttribute( "FLOAT_ATTRS", fvals, 3 );

  double dval = 3.0;
  ds.setAttribute( "DOUBLE_ATTR", &dval );

  double dvals[] = { 1.0, 2.0, 3.0 };
  ds.setAttribute( "DOUBLE_ATTRS", dvals, 3 );

  ds.close();
\end{verbatim}
  Once the above operations are completed, the HDF5 file will have the following
  structure and contents:
{\small \begin{verbatim}
.
|-- STRING_ATTR    = "string test value"
|-- STRING_ATTRS   = {"string","vector","test"}
|-- INT_ATTR       = 1
|-- INT_ATTRS      = { 1, 2, 3 }
|-- UINT_ATTR      = 2
|-- UINT_ATTRS     = { 1, 2, 3 }
|-- FLOAT_ATTR     = 3.0
|-- FLOAT_ATTRS    = { 1.0, 2.0, 3.0 }
\end{verbatim}}

\item
Create a (HDF5) integer array within a dataset:
\begin{verbatim}
  dalDataset ds;
  ds.open( FILENAME );

  // define dimensions of array
  vector<int> dims;
  dims.push_back(4);
  dims.push_back(5);
  dims.push_back(6);
  vector<int> cdims;

  int data[4*5*6];
  for (int gg=0; gg<(4*5*6); gg++)
    data[gg] = gg;

  dalArray * array = ds.createIntArray( "int_array", dims,
                                        data, cdims );
  array->close();
  delete array;
\end{verbatim}

\item
Create a (HDF5) group:
\begin{verbatim}
  dalDataset ds;
  ds.open( FILENAME );

  dalGroup * group = ds.createGroup( "group" );
  group->close();
  delete group;
\end{verbatim}

\item
Get a list of groups in a file:
\begin{verbatim}
  dalDataset ds;
  ds.open( FILENAME );

  vector<string> groupnames = ds.getGroupNames();

  ds.close();
\end{verbatim}

\item
Get a list of members of a group:
\begin{verbatim}
  dalDataset ds;
  ds.open( FILENAME );

  dalGroup * mygroup = ds.openGroup( "groupname" );
  vector<string> memnames = mygroup->getMemberNames();

  ds.close();
\end{verbatim}

\item
Open an existing group:
\begin{verbatim}
   dalDataset * ds = new dalDataset;
   ds->open( "somefile.h5" );
   dalGroup * mygroup = ds->openGroup( "groupname" );
\end{verbatim}

\item
Read an attribute from a dataset:
\begin{verbatim}
  dalDataset ds;
  ds.open( FILENAME );

  std::string attr_name("INT_ATTR");
  int iattr = 0;

  ds.getAttribute( attr_name, iattr );

  ds.close();
\end{verbatim}

\item
Read a array dimensions:
\begin{verbatim}
  uint ret = 0;

  dalDataset ds;
  ds.open( FILENAME );

  dalArray * array = ds.openArray( "int_array" );

  std::vector<int> dims =  array->dims();

  array->close;
  delete array;
\end{verbatim}

\end{itemize}

\subsection{Python}

\begin{itemize}

\item
Create a new file:
\begin{verbatim}
   filename = /path/somename.h5
   ds = dalDataset( filename, "HDF5" )
\end{verbatim}

\item
Open an existing file:
\begin{verbatim}
   ds = dalDataset()
   ds.open( "filename.h5" )         "
\end{verbatim}

\item
Create a group in a file:
\begin{verbatim}
   array_group = ds.createGroup( "groupname" )
\end{verbatim}

\end{itemize}

%% ------------------------------------------------------------------------------
%% Working with standard LOFAR datasets

\section{Working with standard LOFAR datasets}

\subsection{TBB time-series data}

\subsubsection{Structure of the data}

A LOFAR Time-Series data product is stored as a HDF5 file, grouped by station,
then by dipole:
{\small\begin{verbatim}
/
|
|-- Station001                          ... Group
|   |
|   |-- 001000000                       ... Dataset
|   |-- 001000001                       ... Dataset
|
|   `
|-- Station002                          ... Group
|   |
|   |-- 002000000                       ... Dataset
|   |-- 002000001                       ... Dataset
|
\end{verbatim}}
For a complete description of this data format, see the
\textit{LOFAR data format ICD: TBB Time-series data} \cite{ts-def}.

\subsubsection{C++ interface}

{\small
\begin{verbatim}
#include "dal.h"

using namespace std;
using namespace DAL;

int main() {

  dalDataset ds;
  ds.open( "tbb-time-series.h5" ) )

  // The first 3 characters are the station id.
  // The next 3 are the rsp id.
  // The last 3 are the rcu id.
  string id = "001002003";


  // read the time series data for dipole 'id'
  // from 'start' index for 'length' values into 'data'

  int start = 25;
  int length = 20;
  short data[length];
  ds.read_tbb( id, start, length, data );

  for (int index = 0; index < length; index++)
    {
      printf("%d\n", data[ index ]);
    }

  vector<string> groups = ds.getGroupNames();
  for (uint idx=0; idx<groups.size(); idx++)
    cerr << groups[idx] << endl;
  cerr << endl;

  dalArray * array = ds.openArray( id, "Station001" );

  string telescope("");
  ds.getAttribute( "TELESCOPE", telescope );
  cerr << telescope << endl;

  uint time = 0;
  array->getAttribute( "TIME", time );
  cerr << time << endl;

  array->close();
  ds.close();

  return 0;
}
\end{verbatim}}

\subsubsection{Python interface}


\subsection{Beam-formed data}

\subsubsection{Structure of the data}

A LOFAR Beam-Formed file is stored per observation as an HDF5 file, grouped by
beam and then subband.  All attribute data, described below, is defined in the
\textit{LOFAR Beam-Formed Data Format ICD} \cite{BF-ICD}.

{\small \begin{verbatim}
beam-formed-file.h5         <-------  Root level of dataset
|--beam000                  <-------  First beam group
|  |--SB000                 \
|  |--SB001                  )------  Table for each subband
|  `--SB00N                 /
|--beam001                  <-------  Second beam group
|  |--SB000                 \
|  |--SB001                  )------  Table for each subband
|  `--SB00N                 /
`--beam00N                  <-------  Nth beam group
   |--SB000                 \
   |--SB001                  )------  Table for each subband
   `--SB00N                 /
\end{verbatim}}

\begin{description}
\item[Root-level] The root level of the file contains the majority of associated
  meta-data, describing the circumstances of the observation. These data
  attributes include time, frequency, weather and other important characteristics
  of the dataset.
\item[Beam Group] Each observation beam is stored as a seperate group within the
  file, each containing its own subband tables and pointing information.
\item[Subband Data tables] The subband tables are where the bulk of the data
  reside.  Each beam has a fixed number of tables; one per subband. The subband
  frequency information is stored as an attribute of the table.
\item[Full Resolution] The default data stored in each subband table is full
  resolution, polarized voltages from the dipoles. These voltages are stored as
  16-bit complex pairs for both X and Y.  In other words, each sample is stored
  as (X-real, X-imaginary)(Y-real, Y-imaginary) for a total of 64-bits.
\item[Total Intensity] Although complete, full resolution data is very large due
  to a very high sampling rate.  In some cases, the observer may not be especially
  interested in polarization data and instead choose to receive only the total
  intensities of the samples.  Because the total intensity information can be
  stored as a single 32-bit floating point value, the size of the file is
  effectively halved.  In this case, each subband table consists of one total
  intensity value per sample instead of two complex pairs as described above.
\item[Downsampled] Furthermore, the observer may prefer a smaller file with data
  averaged along the time axis, or ``downsampled''.  A relatively modest
  downsample factor (i.e. 128) will drastically reduce the size of the data file
  and, in many cases, still provide interesting, high-resolution data for analysis.
\item[Channelized] Another option for the user is to bin the data into frequency
  channels, effectively downsampling the data along the time-axis by a factor of
  the number of frequency bins. \\
  In this case, the subband table would have total intensities for each channel,
  per sample.
\end{description}

\subsubsection{Interface implementation}

The following DAL classes are specifically written for LOFAR Beam-Formed Data:

\begin{description}
\item[BFRaw] Used to translate \textit{raw} beam-formed samples from the telescope
  into a formatted HDF5 product.
\item[BeamFormed] High-level interface which allows easy access to the HDF5
  beam-formed data product.
\item[BeamGroup] High-level interface which allows easy access to the HDF5 beam
  groups within the data product.
\end{description}

\subsubsection{C++ interface}

The following example program shows how to open and do some simple inspection of
a HDF5 beam-formed data product:

{\small \begin{verbatim}
#ifndef BEAMFORMED_H
#include <BeamFormed.h>
#endif

#define FILENAME "beam-formed-file.h5"

using namespace std;

int main()
{
  // open a HDF5 beam-formed data product

  DAL::BeamFormed * file = NULL;
  file = new DAL::BeamFormed( FILENAME );

  // print a summary of the file's contents

  file->summary();

  // retrieve a beam from the file

  DAL::BeamGroup * beam = NULL;
  beam = file->getBeam( 0 );

  // print a list of the sources, if any are recorded
  //   in the file header

  std::vector<std::string> sources = file->sources();
  cerr << "Sources:" << endl;
  for (unsigned int idx=0; idx<sources.size(); idx++)
    {
      cerr << sources[ idx ] << endl;
    }
  cerr << endl;

  int subband = 0;
  int start = 0;
  int length = 20;
  std::vector< std::complex<short> > xvals;
  std::vector< std::complex<short> > yvals;
  std::vector< std::complex<short> > xx;

  // Retrieve some X-polarization data from subband 0

  beam->getSubbandData_X( subband, start, length, xx );

  printf( "First %d xx values for subband %d.\n" ,
          length, subband );

  for (int ii=0; ii < length; ii++ )
    {
      printf( "(%d,%d)\n" , xx[ii].real(), xx[ii].imag() );
    }
  printf("\n");

  // Step through the X & Y polarization data for subband 0,
  //    10 samples at a time, 10 times

  for (unsigned int count=0; count < 10; count++ )
    {
      beam->getSubbandData_XY( subband, start, length,
                               xvals, yvals );

      printf( "Values %d through %d\n", start, start + length );
      for (unsigned int ii=0; ii < xvals.size(); ii++ )
        {
          printf( "(%d,%d),(%d,%d)\n" , xvals[ii].real(),
                  xvals[ii].imag(),
                  yvals[ii].real(),
                  yvals[ii].imag() );
        }
      printf("\n");

      xvals.clear();
      yvals.clear();

      start += length;
    }

  delete beam;
  delete file;
}
\end{verbatim}}

\subsubsection{Python interface}

The following example script shows how to open and do some simple inspection of
a HDF5 beam-formed data product:

{\small \begin{verbatim}
#! /usr/bin/env python

from pydal import *
import sys

# The BeamFormed object represents the file.
#  The parameter is the name of the beam-formed file.
file = BeamFormed( "beam-formed-file.h5" )

# Here's a print to screen summary of the file.
file.summary()

# print a list of the beams
print file.beams()

# get beam 0
beam = file.getBeam(0)

# show the beam pointing direction
print beam.ra()
print beam.dec()

# show the beam summary
beam.summary()

# get X complex data for subband 0, start 0, length 10
# returns a numpy array
sb0x = beam.getSubbandData_X( 0, 0, 10 )
print sb0x

# get Y complex data for subband 0, start 0, length 10
# returns a numpy array
sb0y = beam.getSubbandData_Y( 0, 0, 10 )
print sb0y

# get X and Y complex data for subband 0, start 0, length 10
# returns a numpy array
sb0xy = beam.getSubbandData_XY( 0, 0, 10 )
print sb0xy
\end{verbatim}}

%% ------------------------------------------------------------------------------
%% Known Issues

\section{Known Issues}

\subsection{C++}

\subsection{Python}

\begin{enumerate}{}{}

\item
\begin{verbatim}
 I'm providing a complete list of what worked and didn't
 work for me:
- center_freq, dispersion_measure, main_beam_diam,
  sub_beam_diameter, total_integration_time, and
  weather_* methods give Segmentation faults
- epoch_lst, epoch_mjd, summary (after
  listing '-- Observer'), methods give RuntimeErrors:
  - RuntimeError: basic_string::_S_construct
     NULL not valid
- filename contains the absolute path stored in the file
- station_temperatures method gives:
  - Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: No to_python (by-value) converter found
    for C++ type: std::vector<int, std::allocator<int> >

\end{verbatim}

\end{enumerate}

\begin{thebibliography}{99}

\bibitem{ts-def} B\"ahren, L. (2008) LOFAR-USG-ICD-001.pdf
\bibitem{Cadot} Cadot, S. (2005) Proposed Data Formats
\bibitem{Diepen} Diepen, G. v. (2008) HDF5 vs. CASA Tables
\bibitem{BF-ICD} Masters, J. (2008) BeamFormed ICD
\bibitem{WisePlan} Wise, M. (2006) LOFAR User Software Plan
\bibitem{hdfgroup} \verb|http://hdfgroup.org|
\bibitem{doxygen} \verb|http://usg.lofar.org/doxygen|
\bibitem{usg} \verb|http://usg.lofar.org|

\end{thebibliography}

\pagebreak

\appendix

\section{Appendicies}

\subsection{Current State of the Library}

The following tables show the status of each DAL classes in terms of it's HDF5,
Casa, FITS and Python support.

\begin{table}[h]
  \caption{dalArray}
  \centering
  \begin{tabular}{ | p{.4\textwidth} | c | c | c | c | }
    \hline
    & \textbf{HDF5} & \textbf{CASA} & \textbf{FITS} & \textbf{Python} \\
    \hline
    \hline
    dalArray                                  & X &   &   &   \\ \hline
    dims                                      & X &   &   &   \\ \hline
    getName                                   & * & * & * &   \\ \hline
    open                                      & X &   &   &   \\ \hline
    close                                     & X &   &   &   \\ \hline
    getAttribute                              & X &   &   &   \\ \hline
    getAttributes                             & X &   & X &   \\ \hline
    setAttribute                              & X &   &   &   \\ \hline
    extend                                    & X &   & X &   \\ \hline
    write int short int                       & X &   &   &   \\ \hline
    write int int int                         & X &   &   &   \\ \hline
    write int comp<float> int                 & X &   &   &   \\ \hline
    write int comp<short> int                 & X &   &   &   \\ \hline
  \end{tabular}
\end{table}

\begin{table}[ht]
  \caption{dalShortArray}
  \centering
  \begin{tabular}{ | p{.4\textwidth} | c | c | c | c | }
    \hline
    & \textbf{HDF5} & \textbf{CASA} & \textbf{FITS} & \textbf{Python} \\
    \hline
    \hline
    dalShortArray                             & X &   &   &   \\ \hline
    readShortArray                            & X &   &   &   \\ \hline
  \end{tabular}
\end{table}

\begin{table}[ht]
  \caption{dalIntArray}
  \centering
  \begin{tabular}{ | p{.4\textwidth} | c | c | c | c | }
    \hline
    & \textbf{HDF5} & \textbf{CASA} & \textbf{FITS} & \textbf{Python} \\
    \hline
    \hline
    dalIntArray                               & X &   &   &   \\ \hline
    readIntArray                              & X &   &   &   \\ \hline
  \end{tabular}
\end{table}

\begin{table}[ht]
  \caption{dalFloatArray}
  \centering
  \begin{tabular}{ | p{.4\textwidth} | c | c | c | c | }
    \hline
    & \textbf{HDF5} & \textbf{CASA} & \textbf{FITS} & \textbf{Python} \\
    \hline
    \hline
    dalFloatArray                             & X &   &   &   \\ \hline
  \end{tabular}
\end{table}

\begin{table}[ht]
\caption{dalComplexArray\_float32}
\centering
  \begin{tabular}{ | p{.4\textwidth} | c | c | c | c | }
    \hline
    & \textbf{HDF5} & \textbf{CASA} & \textbf{FITS} & \textbf{Python} \\
    \hline
    \hline
    dalComplexArray\_float32                  & X &   &   &   \\ \hline
  \end{tabular}
\end{table}

\begin{table}[ht]
\caption{dalComplexArray\_int16}
\begin{center}
  \begin{tabular}{ | p{.4\textwidth} | c | c | c | c | }
    \hline
    & \textbf{HDF5} & \textbf{CASA} & \textbf{FITS} & \textbf{Python} \\
    \hline
    \hline
    dalComplexArray\_int16                    & X &   &   &   \\ \hline
  \end{tabular}
\end{center}
\end{table}

\begin{table}[ht]
\caption{dalColumn}
\begin{center}
  \begin{tabular}{ | p{.4\textwidth} | c | c | c | c | }
    \hline
    & \textbf{HDF5} & \textbf{CASA} & \textbf{FITS} & \textbf{Python} \\
    \hline
    \hline
    dalColumn                                 & * & * & * & X \\ \hline
    dalColumn string                          & * & * & * & X \\ \hline
    dalColumn hid\_t,hid\_t,string            & X &   &   &   \\ \hline
    dalColumn string,string                   & * & * & * & X \\ \hline
    dalColumn Table,string                    &   & X &   &   \\ \hline
    addMember                                 & X &   &   & X \\ \hline
    getName                                   & * & * & * & X \\ \hline
    setName                                   & * & * & * &   \\ \hline
    setFileType                               & * & * & * &   \\ \hline
    getDataType                               &   & X &   & X \\ \hline
    getSize                                   & * & * & * & X \\ \hline
    close                                     &   & X &   &   \\ \hline
    getType                                   & * & * & * &   \\ \hline
    isArray                                   &   & X &   & X \\ \hline
    isScalar                                  &   & X &   & X \\ \hline
    shape                                     &   & X &   & X \\ \hline
    ndims                                     &   & X &   & X \\ \hline
    nrows                                     &   & X &   &   \\ \hline
    data int,int                              & X & X &   & X \\ \hline
    data                                      & X & X &   & X \\ \hline
  \end{tabular}
\end{center}
\end{table}

\begin{table}[ht]
\caption{dalData}
\begin{center}
  \begin{tabular}{ | p{.4\textwidth} | c | c | c | c | }
    \hline
    & \textbf{HDF5} & \textbf{CASA} & \textbf{FITS} & \textbf{Python} \\
    \hline
    \hline
    dalData                                   & * & * & * &   \\ \hline
    dalData str,str,vec,long                  & X & X &   &   \\ \hline
    $\sim$dalData                             & * & * & * &   \\ \hline
    datatype                                  & * & * & * &   \\ \hline
    fortran\_index                            & * & * & * &   \\ \hline
    c\_index                                  & * & * & * &   \\ \hline
    get                                       & X & X &   & X \\ \hline
  \end{tabular}
\end{center}
\end{table}

\begin{table}[ht]
\caption{dalDataset}
\begin{center}
  \begin{tabular}{ | p{.4\textwidth} | c | c | c | c | }
    \hline
    & \textbf{HDF5} & \textbf{CASA} & \textbf{FITS} & \textbf{Python} \\
    \hline
    \hline
    dalDataset                                & X & X &   & X \\ \hline
    dalDataset char*,str                      & X &   & X &   \\ \hline
    init                                      & X & X &   & X \\ \hline
    open                                      & X & X & X & X \\ \hline
    close                                     & X & X &   &   \\ \hline
    getAttributes                             & X &   &   &   \\ \hline
    getId                                     & X &   &   &   \\ \hline
    getAttribute                              & X &   &   &   \\ \hline
    getAttribute\_string                      & X &   &   &   \\ \hline
    getAttribute\_string vector               & X &   &   &   \\ \hline
    getAttribute\_int                         & X &   &   &   \\ \hline
    getAttribute\_uint                        & X &   &   &   \\ \hline
    getAttribute\_double                      & X &   &   &   \\ \hline
    createArray                               & X &   &   & X \\ \hline
    createIntArray                            & X &   & X & X \\ \hline
    createFloatArray                          & X &   &   & X \\ \hline
    createComplexFloatArray                   & X &   &   &   \\ \hline
    createTable string                        & X &   &   & X \\ \hline
    createTable string,string                 & X &   &   & X \\ \hline
    createGroup                               & * & * & * & X \\ \hline
    openTable string                          & X & X &   & X \\ \hline
    openTable string,string                   & X &   &   & X \\ \hline
    setFilter string                          & * & * & * & X \\ \hline
    setFilter string,string                   & * & * & * & X \\ \hline
    openArray string                          & X &   &   &   \\ \hline
    openArray string,string                   & X &   &   &   \\ \hline
    getGroupNames                             & X &   &   &   \\ \hline
    openGroup                                 & X &   &   & X \\ \hline
    listTables                                &   & X &   & X \\ \hline
    getType                                   & * & * & * & X \\ \hline
    getName                                   & * & * & * &   \\ \hline
    getFileHandle                             & X &   &   &   \\ \hline
    read\_tbb                                 & X &   &   &   \\ \hline
    ria\_boost                                &   &   &   & X \\ \hline
    rfa\_boost                                &   &   &   & X \\ \hline
  \end{tabular}
\end{center}
\end{table}

\begin{table}[ht]
\caption{dalFilter}
\begin{center}
  \begin{tabular}{ | p{.4\textwidth} | c | c | c | c | }
    \hline
    & \textbf{HDF5} & \textbf{CASA} & \textbf{FITS} & \textbf{Python} \\
    \hline
    \hline
    dalFilter                                 & * & * & * &   \\ \hline
    dalFilter string,string                   &   & X &   &   \\ \hline
    dalFilter string,string,string            &   & X &   &   \\ \hline
    set string                                &   & X &   &   \\ \hline
    set string,string                         &   & X &   &   \\ \hline
    setFileType                               & * & * & * &   \\ \hline
    isSet                                     & * & * & * &   \\ \hline
    get                                       & * & * & * &   \\ \hline
  \end{tabular}
\end{center}
\end{table}

\begin{table}[ht]
\caption{dalGroup}
\begin{center}
  \begin{tabular}{ | p{.4\textwidth} | c | c | c | c | }
    \hline
    & \textbf{HDF5} & \textbf{CASA} & \textbf{FITS} & \textbf{Python} \\
    \hline
    \hline
    dalGroup                                  & * & * & * & X \\ \hline
    dalGroup char*,void*                      & X &   &   & X \\ \hline
    dalGroup hid\_t,char*                     & X &   &   & X \\ \hline
    $\sim$dalGroup                            & X &   &   &   \\ \hline
    open                                      & X &   &   &   \\ \hline
    getName                                   & * & * & * & X \\ \hline
    setName                                   & * & * & * & X \\ \hline
    createShortArray                          & * & * & * &   \\ \hline
    createIntArray                            & * & * & * & X \\ \hline
    createFloatArray                          & * & * & * & X \\ \hline
    createComplexFloatArray                   & * & * & * &   \\ \hline
    createComplexShortArray                   & * & * & * &   \\ \hline
    getId                                     & X &   &   & X \\ \hline
    getMemberNames                            & X &   &   &   \\ \hline
    getAttribute                              & X &   &   & X \\ \hline
    setAttribute                              & X &   &   &   \\ \hline
    ria\_boost                                &   &   &   & X \\ \hline
  \end{tabular}
\end{center}
\end{table}

\begin{table}[ht]
\caption{dalTable}
\begin{center}
  \begin{tabular}{ | p{.4\textwidth} | c | c | c | c | }
    \hline
    & \textbf{HDF5} & \textbf{CASA} & \textbf{FITS} & \textbf{Python} \\
    \hline
    \hline
    dalTable                                  & * & * & * &     \\ \hline
    dalTable string                           &   & X &   & X   \\ \hline
    $\sim$dalTable                            &   & X &   &     \\ \hline
    printColumns                              & X & X &   & X   \\ \hline
    openTable void*,str,str                   & X &   &   &     \\ \hline
    openTable str                             &   & X &   & X   \\ \hline
    openTable str,MSReader                    &   & X &   & X   \\ \hline
    openTable str,MSReader,filt               &   & X &   &     \\ \hline
    GetKeyword                                &   & X &   &     \\ \hline
    GetKeywordType                            &   & X &   &     \\ \hline
    createTable                               & X &   &   & X   \\ \hline
    getColumn\_complexInt16                   & X & X &   &     \\ \hline
    getColumn\_complexFloat32                 & X & X &   & X   \\ \hline
    getColumn\_Float32                        & X & X &   & X   \\ \hline
    getColumn                                 &   & X &   & X   \\ \hline
    addColumn                                 & X &   &   & X   \\ \hline
    addComplexColumn                          & X &   &   & X   \\ \hline
    removeColumn                              & X &   &   & X   \\ \hline
    writeDataByColNum                         & X &   &   & X   \\ \hline
    setFilter string                          & * & * & * & X   \\ \hline
    setFilter string,string                   & * & * & * & X   \\ \hline
    appendRow                                 & X &   &   & X   \\ \hline
    appendRows                                & X &   &   & X   \\ \hline
    listColumns                               & X & X &   & X   \\ \hline
    readRows                                  & X &   &   & X   \\ \hline
    getAttribute                              & X & X &   & X   \\ \hline
    setAttribute                              & X &   &   & X   \\ \hline
    findAttribute                             & X &   &   & X   \\ \hline
    getNumberOfRows                           & X &   &   & X   \\ \hline
    getName                                   &   & X &   & X   \\ \hline
    getColumnData                             &   & X &   &     \\ \hline
  \end{tabular}
\end{center}
\end{table}

\begin{table}[ht]
\caption{Database}
\begin{center}
  \begin{tabular}{ | p{.4\textwidth} | c | c | c | c | }
    \hline
    & \textbf{HDF5} & \textbf{CASA} & \textbf{FITS} & \textbf{Python} \\
    \hline
    \hline
    Database                                  & * & * & * &   \\ \hline
    $\sim$Database                            & * & * & * &   \\ \hline
    query                                     & * & * & * &   \\ \hline
  \end{tabular}
\end{center}
\end{table}

\end{document}
