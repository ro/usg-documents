
## ==============================================================================
##
##  Process the source files
##
## ==============================================================================

add_custom_target (ICD ALL)

## set up the list of source files
file (GLOB ICD_sources LOFAR-USG-ICD*.tex)

foreach (_tex ${ICD_sources})
  
  ## get the name of the file without the extension
  get_filename_component (_tex ${_tex} NAME_WE)
  ## submit LaTeX document
  latex_document (${_tex})
  ## dependency on collective target
  add_dependencies (ICD ${_tex})
  
endforeach (_tex)

## ==============================================================================
##
##  Processing of figures
##
## ==============================================================================

add_subdirectory (figures)
