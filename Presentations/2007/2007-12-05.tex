\documentclass[12pt,a4paper]{scrartcl}
\usepackage[landscape]{geometry}
\usepackage{fancyhdr}

\parindent 0pt

\pagestyle{fancy}
\renewcommand{\footrulewidth}{0.4pt}
\fancyhead[R,L]{}
\fancyfoot[C]{}
\fancyfoot[R]{\thepage/\pageref{lastpage}}

%% macro definitions

\newcommand{\aipspp}{\textsc{AIPS}\texttt{++}}
\newcommand{\cpp}{\textsc{C}\texttt{++}}
\newcommand{\lofar}{\textsc{LOFAR}}
\newcommand{\lopes}{\textsc{LOPES}}

%% ------------------------------------------------------------------------------
%% begin of document

\begin{document}

\Large

\begin{titlepage}
  \title{\Huge Running in the family \\[2mm] LOPES, LOFAR/CR and LOFAR USG}
  \author{Lars B\"ahren}
  \date{\small LOPES collaboration meeting \\ Karlsruhe, 05 Dec 2007}
  \maketitle
  \thispagestyle{empty}
\end{titlepage}

%% ------------------------------------------------------------------------------
\clearpage

\section*{Overview}

\begin{enumerate}
  \item Personel: \lofar/CR \& \lofar-USG
  \item Summary of activities
  \item Decisions on basic tools
  \item Migration of the code base
  \item Changes due to migration
  \item Organization of the code repository
  \item Functionality and Synergies
  \item Current \& future development
\end{enumerate}

%% ------------------------------------------------------------------------------
\clearpage

\section*{The close family: LOFAR/CR @ RU Nijmegen}

\begin{itemize}
\item Andreas Horneffer
  \begin{itemize}
  \item C/\cpp\ pipeline
  \item inspection of LOFAR TBB data (raw time-series)
  \item trigger for LOFAR/CR observation modes
  \end{itemize}
\item Lars B\"ahren
  \begin{itemize}
  \item beamforming/imaging module
  \item configuration \& build environment
  \item porting \& testing
  \end{itemize}
\item Kalpana Singh
  \begin{itemize}
  \item gain/phase calibration routines
  \item inversion of poly-phase filter (PPF)
  \item beam-simulations for UHEP mode
  \end{itemize}
\end{itemize}

%% ------------------------------------------------------------------------------
\clearpage

\section*{The extended family: LOFAR User Software Group}

\begin{itemize}
\item Michael Wise (UvA)
  \begin{list}{\textbf{--}}{}
  \item coordinator for software development
  \item link between software enginering and observatory
  \end{list}
\item Joseph Masters (UvA)
  \begin{list}{\textbf{--}}{}
  \item Data Access Library (DAL)
  \item Data formats
  \item Parallelization
  \end{list}
\item Lars B\"ahren (RUN)
  \begin{list}{\textbf{--}}{}
  \item CR data processing pipeline
  \item Central server (website \& respository)
  \item Build \& testing environment
  \end{list}
\item Alexander Usov (Leiden)
  \begin{list}{\textbf{--}}{}
  \item Source detection
  \end{list}
\end{itemize}

%% ------------------------------------------------------------------------------
\clearpage

\section*{Summary of activities}

\begin{itemize}
\item setting up of central server for website, Wiki and code repository
  \begin{center}
    \texttt{usg.lofar.org}
  \end{center}
\item a number of design and format decisions
  \begin{itemize}
  \item choice of code repository
  \item choice of build system
  \item definition of time-series data format
  \item definition of pulsar data format
  \end{itemize}
\item input to Critical Design Review (CDR), Apr 2007
  \begin{itemize}
  \item first overall inventory of required software development
  \item refinement and description of pipelines for observation modes
  \end{itemize}
\item input to DCLA Review, May 2007
  \begin{itemize}
  \item continued refinement of the various pipelines
  \item demonstration of first building blocks to the user software
  \end{itemize}
\end{itemize}

%% ------------------------------------------------------------------------------
\clearpage

\section*{Summary of activities {\small (cont.)}}

\begin{itemize}
\item Software Management Plan
\begin{itemize}
\item input to mid-term review of the \lofar\ project
\item (hopefully) complete inventory of the work required to deliver all
  \lofar\ observation modes
  \begin{itemize}
  \item breakdown of the individual tasks
  \item time estimates
  \item responsibilities / project roles
  \end{itemize}
\item step-wise roll-out of modes $\rightarrow$ activity plan to deliver
  incremental functionality
  \begin{itemize}
  \item Definition
  \item Realization \\
    {\normalsize (code implementation, connection to control layers, etc.)}
  \item Integration / Delivery \\
    {\normalsize (Integration, system testing, documentation, accpetance test)}
  \end{itemize}
\end{itemize}
\end{itemize}

%% ------------------------------------------------------------------------------
\clearpage

\section*{Decisions on basic tools}

\begin{enumerate}
\item \textbf{HDF5}
  \begin{list}{\textbf{--}}{}
  \item container for newly defined data formats
  \item equal but independently made choice as in recommandations for \lofar\
  \item able to handle expected large data volumes/rates and manipulations
    thereof
  \end{list}
\item \textbf{Subversion}
  \begin{list}{\textbf{--}}{}
  \item moving away from CVS
  \item publicly accessible central code repository (public read)
  \end{list}
\item \textbf{CMake}
  \begin{list}{\textbf{--}}{}
  \item cross-platform makefile generator
  \item generation of project files (UNIX Makefiles, XCode, Eclipse, KDevelop)
  \item integrated creation of packages (source \& binary)
  \item integrated test evironment (CTest, Dart)
  \end{list}
\item[$\Rightarrow$] decisions agree with choices made in other large-scale
  projects
  \begin{list}{\textbf{--}}{}
  \item avoid isolation by ``island solution''
  \item open-source software
  \end{list}
\end{enumerate}

%% ------------------------------------------------------------------------------
\clearpage

\section*{Migration of the code base: LOPES-Tools $\rightarrow$ CR-Tools}

\begin{itemize}
\item dedicated migration session (L. B\"ahren, 13.---26. Aug)
  \begin{enumerate}
  \item freeze C/\cpp\ contents of Nijmegen CVS repository
  \item porting of library
  \item porting of applications
  \item commit of all ported code to central code repository
  \end{enumerate}
\item basic migration finished before last Young \lopes\ meeting
\item configuration/build environment
  \begin{itemize}
  \item cut loose from \aipspp/CASA framework
  \item separate dependency on optional external components from core code base
    \begin{itemize}
    \item Glish-dependencies confined in single module $\rightarrow$ C/\cpp\ code
      can be build using \textsc{casacore}
    \item dynamic configuration of plotting package used
    \item build tools for \lopes$^{STAR}$, if ROOT and MySQL are available
    \end{itemize}
  \end{itemize}
\item classes API documentation available via \quad \verb|usg.lofar.org/doxygen|
\end{itemize}

%% ------------------------------------------------------------------------------
\clearpage

\section*{Migration of the code base:  Changes}

\begin{itemize}
\item changes will mainly effect people actively developing code (but not only)
\item support of multiple platforms
  \begin{itemize}
  \item before migration: 4 machines running \lopes-Tools
  \item porting to several platforms: SuSE, Debian, Max OS X
  \item porting to different machines: laptop, desktop computer, cluster node
  \end{itemize}
\item essential 3rd party libraries part of distribution
\item simplified installation procedure:
  \begin{enumerate}
  \item Check out the source code from the repository:
    \begin{verse}
      \tt svn co http://usg.lofar.org/svn/code/trunk
    \end{verse}
  \item Run the build/installation script
    \begin{verse}
      \tt cd build \\
      ./build.sh cr
    \end{verse}
  \item[$\rightarrow$] essential external packages will be build first
  \item[$\rightarrow$] configuration/build system part of code distibution
  \item[$\rightarrow$] no special privileges required for local user installation 
  \end{enumerate}
\end{itemize}


%% ------------------------------------------------------------------------------
\clearpage

\section*{Organization of the code repository}

\begin{tabular}{lll}
  \begin{minipage}[t]{0.25\linewidth}
    \normalsize
    \textsl{Top level}
\begin{verbatim}
  usg
  |-- build
  |-- data
  |-- devel_common
  |-- doc
  |-- external
  |-- release
  `-- src
\end{verbatim}
\end{minipage}
&
\begin{minipage}[t]{0.25\linewidth}
  \normalsize
  \textsl{External packages}
\begin{verbatim}
  usg
  |-- build
  |-- data
  |-- devel_common
  |-- doc
  |-- external
  |   |-- CMake
  |   |-- blitz
  |   |-- boost
  |   |-- casacore
  |   |-- cfitsio
  |   |-- hdf5
  |   `-- wcslib
  |-- release
  `-- src
\end{verbatim}
\end{minipage}
&
\begin{minipage}[t]{0.40\linewidth}
  \normalsize
  \textsl{User Software}
\begin{verbatim}
  usg
  |-- build
  |-- data
  |-- devel_common
  |-- doc
  |-- external
  |-- release
  `-- src
      |-- CR-Tools
      |   |-- apps
      |   |   `-- GlishClients
      |   |-- data
      |   |-- implement
      |   |   |-- Analysis
      |   |   |-- ApplicationSupport
      |   |   |-- Calibration
      |   |   |-- Imaging
      |   |   |
      |   |-- scripts
      |   |   |-- glish
      |   |   `-- python
      |   `-- test
      |-- BDSM
      `-- DAL
\end{verbatim}
\end{minipage}
\end{tabular}

%% ------------------------------------------------------------------------------
\clearpage

\section*{Functionality and Synergies}

\begin{itemize}
\item I/O
  \begin{itemize} \parskip 0pt
  \item ability to access standard data products
  \item generic framework for supplying data to the pipeline(s)
  \end{itemize}
\item Calibration
  \begin{itemize} \parskip 0pt
  \item phase calibration using TV signal
  \item RFI mitigation
  \item antenna directivity pattern
  \end{itemize}
\item Signal processing
  \begin{itemize}
  \item up-sampling of recorded time-series
  \end{itemize}
\item Beamforming/Imaging module
  \begin{itemize} \parskip 0pt
  \item commonly required functionality
  \item support of non-standard beamforming modes
  \item decoupling of beamforming and imaging
  \end{itemize}
\item[$\Rightarrow$] lot of synergies between LOPES / LOFAR / Auger-Radio
  \begin{itemize}
  \item[$\rightarrow$] upcoming meeting (Dec 2007) to line up development
    efforts
  \end{itemize}
\end{itemize}


%% ------------------------------------------------------------------------------
\clearpage

\section*{Current \& future development}

\underline{current development:}

\begin{itemize}
\item inspection of LOFAR TBB data (A. Horneffer)
\item finish integrating new beamforming/imaging module with the new pipeline
  \begin{itemize}
  \item[$\rightarrow$] basic connection complete, functional testing on data
    pending
  \end{itemize}
\item connect I/O module to the Data Access Library (DAL)
  \begin{itemize} \parskip 0pt
  \item[$\rightarrow$] ability to read/process \lofar\ TBB data
  \item[$\rightarrow$] completed before christmas
  \end{itemize}
\end{itemize}

\underline{future development:}

\begin{itemize}
\item import remaining code still in CVS repository (Glish scripts, calibration
  tables)
\item porting to further platforms (local cluster, CEP, etc.)
\item continued support for increasing number of (scientific) users
\item upgrade of imaging: polarization, parallization, etc.
\item handling of external sensor data
\end{itemize}

%% ------------------------------------------------------------------------------

\label{lastpage}

\end{document}
