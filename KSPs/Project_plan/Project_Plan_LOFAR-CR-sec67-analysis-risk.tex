%% -- Section 6 -----------------------------------------------------------------

\section{Analysis and follow-up}

% This is about the work that is done interactively or at least under
% human supervision. E.g. the statistical analysis.

The first effort in the analysis of the \lofar\ data is to establish a reliable
relationship between the measured radio pulse height and the primary energy of
the \eas. The \lopes\ project and the Monte Carlo simulation \textsc{reas} by
Tim Huege have already provided a good framework, but due to the relatively
small scale of \lopes\ only part of the theory and simulation results 
have been experimentally tested until now.

When primary energies can be reliably calculated, an energy spectrum will be
made, composition studies will be done, and anisotropies and possible point
sources will be searched for. Also, with detailed knowledge of energy and
nature of the primary, it is possible to study high energy forward cross-sections and inelasticity parameters.

Other research topics include the search for neutrinos in nearly
horizontal (also `double-bang') events,  \eas-thunderstorm interactions, and simultaneous Gerasimova-Zatsepin
showers.

Sub-second electromagnetic processes in and around thunderstorms are monitored
to study their development and interdependence.

The search for radio signals from \uhecr\ in the lunar regolith is based on
a different emission mechanism (Cherenkov radiation from the lunar regolith) and
can be carried out separately from the other research. 

\subsection{Steps}

The analysis can be split up into three categories: primary energy determination,
CR physics and \eas\ physics. A more detailed discussion of these topics has been given in Section 1.2.

\subsubsection*{Primary energy determination}

\begin{itemize}
\item The self-trigger of \lofar\ will be tested and compared with the trigger
  from the scintillator plates. The radio-only trigger might be insensitive to
  showers with small geomagnetic angles, while the scintillator plates will
  not pick up signals from very inclined or old electromagnetic showers.
  Since other \lofar\ stations can only trigger on the radio signal, it has to
  be understood which showers are missed to prevent construction of incorrect
  energy spectra and false anisotropies.
  
\item The detailed geometry of the pulse will be examined by evaluating the
  lateral distribution of the pulse strength, polarization of emission, shape
  of the shower front, and temporal pulse profile. At the same time these
  characteristics are simulated for a broad range of energies, arrival
  directions and for different particles. An important aspect of the modeling
  is understanding what role secondary emission mechanisms play
  (Cherenkov emission; transition radiation; electric field effects). Data
  from \lopes\ can be used to compare simulation and experiment and work
  towards a definite model to deduce primary energy and particle nature from
  \lofar\ data. 
\end{itemize}

\subsubsection*{CR physics}

\begin{itemize}
\item Construction of energy spectrum for \hecr\ and \vhecr\ and analysis of the
  composition of cosmic rays between the knee and the ankle. 
\item Search for anisotropies with an angular resolution of 0.5$^{\circ}$ and
  possible point sources within the galaxy.
\item Determination of forward cross-sections and inelasticity parameters at
  high energies ($>10^{17}$\,eV) which cannot be measured in
  particle accelerators since the detector would have to be place inside the
  particle beam.
\item Detection of \uhecr\ particles by looking for Cherenkov radio flashes from
  the moon. The energy spectrum in this region is not yet clearly established
  and detection of these particles is a result in itself. The experiment will
  either observe super-\textsc{gzk} particles or be able to put new upper limits
  on \uhecr\  and neutrino flux.
\end{itemize}

\subsubsection*{EAS Physics}

\begin{itemize}
\item Search for isotropic radio emission form \eas. The emission of \eas\ may have an isotropic component caused by emission from electrons and positrons after scattering.
   Although this emission is expected to be much
  weaker than the beamed MHz emission simply because the radiating particles are distributed over a large solid angle, the high sensitivity of \lofar\ might
  be sufficient to detect it.
\item The Gerasimova-Zatsepin effect would be a powerful way to directly determine the mass of the primary, and works as follows.
When a CR travels through the solar radiation field it can loose a proton
  by photo-disintegration. The proton and the CR can both initiate an air
  shower in the Earth's atmosphere. When both showers are recorded the nature
  of the primary particle can be found by taking the ratio between the energies
  of the two showers. The simultaneous showers have distances up to several 100~km, which makes the effect
  invisible for most air shower experiments but not for \lofar. However, our recent estimates indicate that the rate of occurrence is too small to be of practical value.
\item In the case of nearly horizontal showers, events triggered close to the
  ground (young showers with a small radius of shower front curvature) would
  be a possible signature of neutrinos. A special
  type is the `double-bang' shower induced by tau-neutrinos. In this
  case tau lepton initiates the second shower. These showers have to be nearly
  horizontal to have a large enough path-length for both showers to develop.
  The third interesting type of nearly horizontal showers is also a double
  event: The primary particle triggers a shower high in the atmosphere, and one
  of secondary muons initiates a new shower at lower altitude resulting in
  two subsequent radio flashes.
\item
  To study the radiation mechanism in greater detail,
  it is important
  to do MC shower simulations with CORSIKA as well as to take measurements. Comparing the amount
  of radiation expected from these simulations may give us an indication of the
  actual mechanisms as they are measured in the field.
\item The trail of ionisation electrons left behind by an EAS reflects radio signals
which might be picked up by \lofar (`passive radar'). This technique is used to detect ionisation
trails of meteors in the atmosphere. Radar reflection could lead to a powerful increase in LOFAR's acceptance  of HECRs. 
\end{itemize}

\subsubsection*{Thunderstorm physics}

\begin{itemize}
\item During thunderstorm conditions large electric fields amplify the \eas\ 
  radio pulse. Data taken during thunderstorms should be excluded from the
  general data analysis. Other effects, however, can be studied during these
  periods. The ionization electrons left behind by the \eas\ are accelerated in
  the background field, producing isotropic radiation. These electrons may
  act as seeds for (runaway) breakdown and lightning initiation. If \lofar\ also 
  records lightning discharges correlation between \eas\ and electrical
  processes in thunderclouds can be studied.
\item \lofar\ will be used to detect and locate lightning strikes. The result
  will be compared with the \textsc{knmi} lightning detection system.
\item By producing sky-maps from data recorded in a second around a lightning
  strike, sub-second electromagnetic processes can be studied, such as lightning
  initiation, leader development and high altitude events, like blue jets and
  sprites. The high temporal resolution of \lofar\ makes it possible to see the
  development of these processes in time.
\end{itemize}

\subsection{Resources needed}

For detailed analysis atmospheric conditions need to be measured, such as
temperature, air pressure and atmospheric electric field strength. Measuring
devices for these quantities are needed in each \lofar\ station.

\subsection{Timeline and manpower}

The following packages are assigned to members of the group:

\begin{center}
  \begin{tabular}{lll}
    \sc Work-package & \sc Position & \sc Responsible \\
    \hline
    \vhecr & 1 Post-Doc, 2.5 years & A. Horneffer \\
    \hecr  & 1 Post-Doc, 2.5 years & K. Singh \\
    \rfi\ analysis & 2 PhD student, 2 years & L. B\"ahren \\
    Storage, Archiving and Cataloging & 1 PhD student, 1 year & A. Nigl  \\
    Pulse computations & 2 PhD students, 2 years  & S. Laf\`ebre\\
    Simulations & 2 Post-Docs, 2 years & J. Petrovi\'c\\
    Energy reconstruction, calibration & 1 PhD student, 2 years
  \end{tabular}
\end{center}

The CORSIKA simulations have been started, and can be done independent of the experiment itself.
It is expected that a library of MC results will be available within a year.

\subsection{Responsibilities of team members.}

\begin{list}{\textbf{--}}{} \parskip 0pt

\item \textbf{A. Horneffer} \vhecr
\item \textbf{L. B\"ahren} \rfi\ removal, lightning detection
\item \textbf{K. Singh} \hecr
\item \textbf{S. Laf\`ebre} is responsible for testing and running the CORSIKA
  simulations on Stella and the various computing clusters. He will also be involved in
  analysing the results of these simulations.
\item \textbf{A. Nigl} is in charge of analyzing signals from other fast varying
  sources like the bursting Sun and Jupiter and in addition study of feasibility
  of VLBI with distant \lofar\ stations.
\item \textbf{S. Buitink} EAS-thunderstorm interactions, simulations
\item \textbf{J. Petrovi\'c} Inclined showers, neutrino detection, MC CORSIKA simulations.
\item \textbf{T. Huege} MC radio simulations
\item \textbf{J. Bacelar} \uhecr
\end{list}


\section{Risk analysis}

% General remark: if we already have identified a number of risks -- can 
% we also present suggestions/ideas how we will be addressing these issues. I 
% guess it would be a good thing if were able to demonstrate that we are aware
% of certain risks, but also that we are taking active steps to either resolve
% potential problems and/or minimize their potential effect on the project.

\subsection{Technical risks}

\medskip

\risk{Non-detection of radio pulses from cosmic rays}
     {There had been the risk that it is not possible to detect radio pulses
       from cosmic rays.}
     {Hardly any science possible for \lofar/CR.}
     {For the \hecr\ and \vhecr\ range the presence and 
       detectability of the radio pulses has already been proven by the \lopes\  
       experiment. \newline 
       For the \uhep\ range even a non-detection will be a success as it
       will set the experimental upper limit lower than current theoretical
       predictions for the \uhep\ neutrino flux.}
     {None required.}

\risk{Radio only trigger}
     {The radio only trigger has not  yet been shown to work. It has not been
       possible to try this out with \its; so it can only be done as soon as
       CS-1 is ready; important to demonstrate and quantify the performance as
       soon as possible.}
     {Trigger cannot be generated based on the radio data only, but has to
       provided by another detection system (particle detectors).}
     {Hard to estimate}
     {A small particle detector array (5-6 detectors) will be installed. Close
       interaction with the design team at ASTRON is required to ensure the
       final design will meet the requirements of \lofar/CR.}
%   \notes{Who will be taking care of this? Do we have everything in place (e.g.
%     algorithm, software, ...) to perform these tests?}

\risk{Transient Buffer Board}
     {The \tbb s are essential to the \lofar/CR observing  mode. It is therefore
       important that (at least some) \tbb s are installed before summer 2006, so
       that the trigger can be tested.}
     {Conflict in resource allocation/usage with the Transients KSP. The \tbb s is
       used differently for transient and CR measurements (visibilities versus 
       voltage time series) so that either CR signal or transient signal can be
       read out from buffer but not both.}
     {No exact estimate possible at this point, because it is not clear how 
       resources will be allocated to the individual KSPs. Conflict most likely
       if Transients and CR are about to observe in parallel.}
     {This conflict can best be resolved by allocating half the number of \tbb s per station 
       to CRs and to Transients each. A small group has been set up to work in
       close cooperation with the \lofar\ station group at ASTRON}
%   \notes{Haven't we already decided to set up a small group (Heino, Andreas,
%     Sven) to look into this together with the station group from ASTRON and
%     people from the Transients \ksp?}

\risk{{\sc hecr} and {\sc uhep} observation mode}
     {CRs below $10^{17}$ eV will not be detected by single antennas but only
       with a dedicated beam. The main constraint for this observation mode is,
       that the complete processing chain -- i.e. from data acquisition at
       dipole level, through processing on BlueGene, up to dumping the contents
       of the \tbb s -- has to take place within the 1~sec time-interval for
       which the raw data are buffered on the \tbb s. As data transport via
       the network and buffering in the input section to BlueGene will
       introduce considerable delays, the raw-data related to the time interval
       for which a peak was detected may no longer be present in the \tbb s.}
     {No detailed reprocessing possible, as this would be starting from the raw
       time-series of each dipole.}
     {High}
     {(i) The processing intended to run on BlueGene could be performed on
       additional dedicated hardware installed close to the central core. This
       would reduce data transport- and control-communication (MAC) time, as
       well as avoid passing through the input section at CEP. However, this
       solution would introduce considerable additional costs. \newline
       (ii) The number of antennas per station can be traded against the
       time-interval over which raw time-series data are buffered: reducing
       the number of antennas contributing to the station beam by e.g. a
       factor of 2 would extend the buffered time-interval by the same amount.}

\risk{Calibration}
     {At \lopes\ we will eventually have an absolute calibration 
       i.e we will know how much V/m at the antenna level corresponds to a 
       primary energy from a specific azimuth and zenith angle.}
     {Lack of information for proper reconstruction of the physical parameters
       of CR \eas.}
     {Will absolute calibration be provided for \lofar\ signal levels?}
     {Probably, the easiest then is to use absolute calibration at \lofar,
       assuming that slightly different frequencies and bandwidth can be
       corrected for.}
     
\risk{Data storage and analysis}
     {For CR data collected during the coming year or so provisions have been
       made. Data storage and access over longer time scale have to be
       organized.}
     {The amount of data generated by \lofar\ and/or the post-processing is too
       large.}
     {No real problem foreseen.}
     {Eventually the whole data management for \lofar\ will be GRID enabled, such
       that resources can be joined and do not have to be provided by one single
       site/university/institute. We already have started negotiations with 
       \textsc{sara} (Amsterdam) about storage of the raw data.}
%   \notes{How does this change if we not only consider ``pure'' CR data, but
%     also data from observation of thunderstorms of moon observations?}

\risk{Data transport}
     {Once the data streaming through the \tbb s are frozen, they will have to 
       be transported to either RU Groningen or RU Nijmegen. Depending on the 
       observation mode this will be between 1.6 GB/event and 5.9 TB/event,
       which have to be retrieved from the \lofar\ stations.}
     {Long data transmission times will reduce the duty cycle of the CR
       observations.}
     {Will depend on capacity of the \lofar\ network and the I/O performance
       of the data storage facility.}
     {Needs to be discussed with the \lofar\ (software) engineering team, in
       order to identify possible bottle-necks.}

\risk{Data format}
     {A data format needs to be defined, which is flexible and powerful enough
       to allow the handling of large amounts of raw time-series data-sets.}
     {---}
     {---}
     {Involvement in the recently started efforts to (a) define the standard
       data products as required by the KSPs and (b) creation of a general 
       data access library. Such activities have been initiated by \lofar\
       user software group.}

\risk{{\sc aips++/casa}}
     {(a) With the \lopestools\ software built on top of the \textsc{aips++/casa}
       library, there always is the risk, that a certain functionality either
       no longer is provided properly or that parts of the library are subject
       to reorganization.
       \newline
       (b) Installation of a fully-fledged \textsc{aips++/casa} system presents
       the major obstacle for development and/or usage of the \lopestools, since
       no binary collection is distributed but everything is compiled from source.
       \newline
       (c) Activities are under way to replace Glish as scripting language in
       favour of Python.}
     {(a) Changes or bugs in the \textsc{aips++/casa} library will result in
       errors in the \lopestools\ software.
       \newline
       (b) Though considerable progress has been made by the \textsc{aips++/casa} 
       project as well as by ourselves to simplify installation, enhanced level of 
       expertise is required to set up an operating system.
       \newline
       (c) With large parts of the \lopestools\ kernel still written in Glish this
       code might become useless if support for Glish is terminated.}
     {(a) Likely to happen once in a while, since \textsc{aips++/casa} is under
       active development, with currently ALMA (and not \lofar) as major driver.
       \newline
       (b) Certainly -- as already encountered and still the case.
       \newline
       (c) Transition to Python certain; backwards compatibility plans unknown.}
     {(a) Regular test builds. Errors can be resolved by a
       single programmer typically within (a fraction of) a day.
       \newline
       (b) Continue work on providing installation script. At a later stage, if
       \lopestools\ gets merged into the \lofar\ user software, the distribution
       infrastructure therein can be used. A possible alternative is the
       distribution of static binaries.
       \newline
       (c) Keep close contact with people involved in the active development of
       \textsc{aips++/casa} (e.g. Ger van Diepen).}

\risk{{\sc aips++/casa}}
     {\textsc{alma} has become \textsl{the} major player in the active development
       of \textsc{aips++/casa}, effectively controlling its direction. Though originally ASTRON was a key-member in the original
       \textsc{aips++} consortium, no official commitment exists after the 
       consortium has been dissolved.}
     {Design decisions are taken, which affect further \textsc{aips++/casa} based
       development for \lofar, or even affect already written code.}
     {Hard to guess, but likely}
     {(i) Evaluate the significance of \textsc{aips++/casa} for \lofar\ software
       developement. (ii) Establish a link between the \lofar\ project and the
       \textsc{aips++/casa} core team, to have up-to-date feedback on ongoing
       development and design decisions. (iii) Get ASTRON to consider a commitment
       to \textsc{aips++/casa}, re-establishing its position in the project
       and gain (at least sufficient amount of) control over where the
       development is heading for. Such a request based on the outcome of (i) 
       will be steered by the \lofar\ User Software Group towards ASTRON R\&D
       (M. d. Vos).}


\subsection{Resource/financial and other risks}

%\textsl{We submitted a NWO-M proposal, which was not granted -- this will 
%result in a shortage of funding for additional positions.}

Most of the vital funding for the CR KSP -- apart from the science -- has been secured. Available positions have been filled in already. Also, we have obtained funding of the 5 particle detectors. Funding of the following positions has not yet been granted:
\begin{center}
  \begin{tabular}{ll}
    Passive Radar & 1 Post-Doc, 3 years \\
    CR-experiment & Data analyst/Operator, 3 years \\
    Post processing pipeline & ICT expert, 3 years \\
  \end{tabular}
\end{center}

