%% -- Section 3 -----------------------------------------------------------------

\section{Organization of the Key Science Team}

\subsection{Membership}

The main criterion for becoming/being a member is:
`Doing science with \lofar/CR or aiming at doing science with \lofar\ as soon
  as relevant observations can be done,
  and/or 
  contributing to the infrastructure (hardware, software) which is required
  to carry out science with \lofar/CR.'

A list of present members of \lofar/CR is given in Table \ref{tab:members} below.

\begin{table}[htb]
  \centering
  \caption{Members of the \lofar/CR Key Science Team}
  \smallskip
  \begin{tabular}{lll}
    \hline
    \sc name & \sc affiliation & \sc function \\
    \hline \hline
    Bacelar, J. (Jose)                   & KVI, Groningen \\ 
    B\"ahren, L. (Lars), PhD             & RU \\
    Buitink, S. (Stijn), PhD             & RU \\
    Falcke, Prof. dr. H.D.E. (Heino)     & ASTRON \& RU & spokes-person \\
    Haungs, Dr. A. (Andreas)             & FZK-IK, Karlsruhe \\
    Horneffer, Dr. A. (Andreas), Post-Doc & RU \\
    Huege, Dr. T. (Tim), Post-Doc,        & FZK-IK, Karlsruhe \\
    Kuijpers, Prof. dr. J. (Jan)         & RU & PI \\
    Laf\`ebre, S. (Sven), PhD            & RU \\
    Nehls, S. (Steffen), PhD             & FZK-IK, Karlsruhe \\
    Nigl, A. (Andreas), PhD              & RU \\
    Petrovi\'c, Dr. J. (Jelena), Post-Doc & RU \\
    Scholten, Dr. O. (Olaf)              & KVI, Groningen \\
    Singh, K. (Kalpana), Post-Doc         & RU \\
    Timmermans, Dr. C. (Charles)         & NIKHEF/RU \\
    Van den Berg, Dr. A.M. (Ad)          & KVI, Groningen \\
    \hline
  \end{tabular}
  \label{tab:members}
\end{table}

% \notes{We might want to add a note here, under which conditions membership in
%   \lofar/CR can be revoked. Furthermore we should add a comment, in which way 
%   \lopes\ fits into \lopes/CR; is one a subset of the other or how are they
%   related?}

The number of official functionaries is kept to a minimum: a PI -- who
represents this KSP in the Astronomy Research Committee (ARC) of \lofar\  
which meets once a month -- and a spokesperson. 

The Steering Committee (SC) is the principal governing body of the collaboration
and will meet once per month as soon as LOFAR/CR observations start up. The Steering group will consist of 5 members which are appointed for 3 years by the Key Science Team:
\begin{enumerate} \parskip 0pt
\item PI (chair)
\item Spokesperson
\item representative from Nijmegen
\item representative from KVI
\item representative from Karlsruhe 
\end{enumerate}
KS Team and SC can be expanded to include members, associate
members\footnote{Associate member when only interested in one particular
  aspect of observations} and observers from future collaborations.

\subsection{Communication}

Regular meetings will be held to communicate and discuss developments relevant
for \lofar/CR:
\begin{itemize} \parskip 0pt
\item Weekly KSP meetings at the member institutes: this includes all project
  members located in Nijmegen, plus eventual visitors from the partner
  institutes (already taking place since 2 years).
\item Monthly meetings of the SC after start-up of CR observations.
\item Meetings of the full consortium twice a year.
\item Meetings of the \lopes\ collaboration (usually hosted by FZK) twice a year (already since 2 years).
\end{itemize}
Further, a weekly progress report is distributed via email (since 3 years).

\subsection{Areas of responsibilities}

\begin{description} \parskip 0pt
\item[Bacelar, J.] \uhep\  Analysis and flux determination;
\item[B\"ahren, L.] Code development for \lopestools\ and \lofar/CR data
  processing pipeline (\textsc{snn} position), with emphasis on beam forming,
  imaging, and the environment for the softwar; Monitoring and analysis of
  the radio emission from electric discharges in thunderstorms (also as
  collaboration with \textsc{knmi});
\item[Buitink, S.] Influence of atmospheric electric fields on radio emission
  of \eas\  and \eas-thunderstorm interactions; CR radio science;
\item[Falcke, H.] Spokesperson; International policy; Science and analysis;
\item[Haungs, A.] Correlation of radio shower observables with particle
  measurements of the same showers; Cosmic ray air shower physics;
\item[Horneffer, A.] Code development for \lopestools\ and \lofar/CR data
  processing pipeline; Determination of the primary particle energy from the 
  radio pulse height;
\item[Huege, T.] Monte Carlo simulations of air shower radio emission; 
  Determination of the electric field strength of the emission from \lopes 
  measurements; Theoretical interpretation of the measurements vis-a-vis 
  emission mechanisms;
\item[Kuijpers, J.] PI; General policy; Science and analysis; Overall coordination;
\item[Laf\`ebre, S.] Code development for \lopestools; Design of \vhecr\
  trigger; MC simulations of \eas\ with CORSIKA;
\item[Nehls, S.] Code development for  \lopestools\ and \lofar/CR data processing
  pipeline; Determination  of the primary particle characteristics from the 
  \lopes radio measurements; Environmental eefects on the signal strength;
\item[Nigl, A.] Storage and archiving of CR event-files at data center in
  Nijmegen and \textsc{sara}\footnote{\textsc{sara}: Computing and Networking
    Services (www.sara.nl)} in Amsterdam;
\item[Petrovi\'c, J.] Radio emission of highly inclined CR air showers
  measured with \lopes\ -- possibility for neutrino detection;
  CORSIKA simulations of high energy CR showers;
\item[Scholten, O.] Theoretical modeling of the \uhep\  emission;
\item[Singh, K.] Code development for \lopestools\ and development of the
  \hecr\ trigger;
\item[Timmermans, C.] Particle detectors for a trigger array at \lofar; 
  Development of radio detection of cosmic rays at the Pierre Auger Observatory; 
\item[Van den Berg, A.M.] International and Dutch coordination of radio measurements of 
  HECRs at the Pierre
  Auger Observatory.
\end{description}

\subsection{Plans for expansion}

There is now a growing community dedicated to measuring radio emission from CRs, with groups in Germany, France, the Netherlands and other countries.
One goal of the community is to add radio detection capability to the current leader in
air shower detection, the Pierre Auger Observatory. 
While LOFAR has a high sensitivity that will allow precision measurements of the 
radio emission from air showers and will have a relatively low threshold energy,
Auger covers a large area and thus will be able to cover the gap between the
\vhecr\ and the \uhep\ measurements.

%\begin{itemize}
%\item An additional PhD student will be hired to work on the details of the
%  in-beam detection of CR events and subsequently on the $\nu$-Moon experiment.
%\end{itemize}

\subsection{Data rights}
\label{subsec:data rights}

As to access to data taken or generated under the \lofar/CR KSP, a distinction is made between:
\begin{list}{\textbf{--}}{} \parskip 0pt
\item raw data (i.e. time-series) as acquired from the \tbb s of the radio
  antennae;
\item derived (intermediate) data products, such as image hyper-cubes or
  CR/\eas\ event lists.
\end{list}
For the derived data products the following release scheme is planned:
\begin{list}{\textbf{--}}{}
\item UHECRs: after collection of 1000 events at $10^{18}$ eV (around 2-3
  years observing) there will be a one-year `grace' period for
  analysis/publications after which data become public as a catalogue of
  events;
\item $\nu$-Moon: similar, after 1 month of cumulative observing time, there will
  be a one-year grace period etc.;
\item VHECRs: similar grace period but now after determination of the CR spectrum down to a
  1 pro mille accuracy;
\item Inclined (zenith angle $> 60$ degrees) events: similar, after 1000 events.
\end{list}
Release of the raw data will match the release of the products derived thereof.
\underline{Note:} Data rights, as proposed in this section, are subordinate to the overall \lofar\ data rights as defined by the \textsc{arc}.

\subsection{Publication policy}
\label{subsec:publication policy}

The main scientific results and basic method descriptions of the \ksp\ should 
be published by all members of the \lofar/CR \ksp. Technical papers, minor
papers (e.g. conference papers) that discuss new methods or minor aspects
of the project are published only by the group directly involved. Publications
should be sent for comments to the \emph{entire} \ksp\ \emph{before}
it is being submitted. Publications require the approval by the SC and are 
announced to the \lofar\ Consortium before submission.

The preferred ordering of the author list consists of a leading group of
scientists/engineers who contributed most to the work (e.g. the members of
the sub-project), with the first author taking the main responsibility for
the result and the write-up of the paper. The remaining members of the
collaboration are added in alphabetic order.

For the general \lofar/CR papers, new members of the \ksp\ become co-author of
publications, half a year after the beginning of their membership. This
period may be shortened if the new members have made significant contributions
to a specific paper or to the entire project before the formal beginning of
their membership. Members remain authors of a paper that is being drafted by
the \ksp\ until half a year after the termination of the membership. This
`grace period' is extended by an additional three months period for every year the author
has been member of the KSP, with a maximum of 1.5 years.

% \notes{
%   Contents of section so far has been adapted from
%   \cite{data and publication rights}, which originally is targeted towards
%   \lofar\ as a whole; though this in fact might be a good starting point,
%   we will need to expand the draft and be a bit more specific here. \\
%   Do we have a policy for papers resulting from a collaboration
%   with people from outside the \lofar/CR group (also: with this require
%   access rights to our data)?
% }

