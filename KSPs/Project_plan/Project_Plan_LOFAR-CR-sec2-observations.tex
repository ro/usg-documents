%% -- Section 2 -----------------------------------------------------------------

\section{Proposed observations}

\subsection{Instrumental setup (baselines/prerequisites)}
\label{subsec:instrumental setup}

\begin{description}
\item[UHEP:] The whole \lofar\ array is configured into the tied array mode and
  beams are formed into the direction of the Moon for the frequency range of
  110--190\,MHz. The field of view should cover a sizeable part of the whole
  lunar surface. The best situation for a good self-trigger system would be to
  define a unit (a set of closed-spaced antennas) covering a total area of
  $500 \hbox{m} \times 500 \hbox{m}$ (see Fig.~\ref{fig:core-subdivision}
  below for a schematic representation of the core sub-division). For the
  chosen band, this unit will have a field of view of 50 arc-min down to
  10 arc-min dependent on the azimuth and elevation of the Moon. Such a
  unit will be operated in tied array mode, constantly tracking the Moon.%
  \begin{figure}[htb]
    \centering
    \includegraphics[width=.6\textwidth]{figures/core-subdivision.eps}
    \caption{Sub-division of the central/virtual core into groups delivering
      separate beams for the \uhep\ observation mode; the antennas are
      organized into 4 groups consisting of $M$ stations of $N$ elements each.}
    \label{fig:core-subdivision}
  \end{figure}%
The self-triggering requirement would then be that each unit sees a pulse with
  a power  larger than a predetermined threshold.
At least two beams of 16\,MHz bandwidth distributed over at least 64\,MHz will
  be formed, covering the entire Moon, thus allowing an anti-coincidence trigger.
  This data is searched for a short radio pulse that has been dispersed by the
  ionosphere.

  In the chosen band, the ionospheric dispersion is 27\,ns/MHz (for a
  typical value of  the Total Electron Content (\tec)). This dispersion is linearly dependent on the specific
  \tec\ for the real-time observing conditions, and can very up to a factor of
  three. Here we note that the original pulse from the Moon is fully linearly
  polarized. The polarization angle is given by the plane of the incoming
  primary particle. Therefore, fixing the polarization
  angle of the measured signal will determine a preferred direction of the
  incoming flux of cosmic-particles to be measured. Furthermore, since the polarization
  angle will rotate depending on the exact value of \tec,  analysis of this
  rotation at different frequencies will determine accurately the value of
  \tec\ on-line for that event. This will further allow an accurate calculation
  of the ionospheric dispersion.

  After a trigger is detected the raw time series data of the beam for the
  last second is stored, as well as  10 milliseconds of the raw (unfiltered)
  data from the  Transient Buffer Boards (\tbb s). For the post-processing, the entire \textsc{hba} bandwidth above
  the FM band up to 200\,MHz will be used (as stored in the \tbb).

  For the detection the \lofar\ virtual core will be used but \tbb\ data from more
  remote stations will be included in the data analysis to locate the CR
  impact site and direction. For the direction estimate the polarization
  signal will be crucial.

  This mode uses most resources of \lofar. As more bandwidth leads to a higher
  signal-to-noise for the pulse detection, other observations in parallel would decrease
  the sensitivity.

\item[VHECR:] Individual antenna signals are searched for the occurrence of 
  correlated pulses in a set of antennas in a station (4 or more) within a 
  pre-selected time window of about 3 microsec. These act as triggers to 
  download and store the recent voltage time series of individual antennas over 
  the last millisecond from the Transient Buffer Boards (\tbb s).

  \textbf{Mode A:} In this mode, all stations are observing with low-band 
  antennas. The CR observations can be done in parallel with other 
  measurements, apart from transient measurements when they also use the \tbb s.

  \textbf{Mode B:} The set-up of the instruments is completely (including the 
  choice of the frequency band) determined by other experiments. In each station
  some of the \tbb s -- say 50\,\% -- are recording time series data, while the 
  remaining \tbb s can be configured differently (according to the requirements of 
  other experiments). 
  We we do not need optimal data from all antennas per station as the antennas 
  in a station are packed more densely than the characteristic lateral scale of 
  radio emission. But as the arrival times of high energy CRs are not predictable
  we have to be sensitive to CR events all the time in order to get good flux 
  measurements and not to miss the most interesting CRs.
  These observations can be done simultaneously with other observations.
  
\item[HECR:] The \tbb s are used and downloaded in parallel with \vhecr -A/B
  observations. The trigger is provided by pulse detection within a beam in a
  fixed direction. Thus the trigger occurs on beam-formed and filtered data that needs to be
  converted back into a time series. Details of this conversion -- e.g.\ at
  which level this happens -- are as yet unclear. We will use either dedicated
  beams for CR measurements from specific directions on the sky or piggy-back
  on beams from other measurements.

\item[TS-mode] This mode is similar to the \vhecr\ mode A except that longer time 
  slices will be stored. The triggering is done on lightning strikes, which are easily
  recognizable in a dynamic spectrum.
  A full second is read out from the \tbb s of all antennas of all stations,
  half a second before the trigger and half a second after the trigger. In the core station
  the scintillator plates can give a trigger, making it possible to study EAS-thunderstorm
  interaction.
\end{description}

\subsection{Observing time}
\label{subsec:observing time}

\begin{description}
\item[UHEP:] 1 month of accumulated observing time under good ionospheric conditions
  is needed to reach a sensitivity that is four orders of magnitude better than
  the sensitivity of existing experiments, and allows detection of CRs with
  energies beyond the \textsc{gzk} limit (see Figs.~\ref{fig:nu-moon limits}).
  Additional observing time is desirable to increase the sensitivity even further.
 
  
\item[VHECR mode A:] About 1 month of accumulated observing time is required as 
  early as possible after construction and installment of  the \tbb s to get the 
  analysis started on about 1000 good events. Thereafter as often as available, 
  in any case during bad ionospheric conditions. As the \uhep\ and the \vhecr\
  modes use different frequency bands this cannot be the same observing time as
  the \uhep\ time.
  
\item[VHECR mode B:] Continuous observations, and at least 1/3 of the time with
  the low-band antennas.
  
\item[HECR:] Continuous and in parallel with the \vhecr -A/B observations. How
  long we will need a CR dedicated beam will be determined from future 
  observations of anisotropies in the CR distribution.

\item[TS-mode] This mode is switched on whenever there is thunderstorm activity 
  within a distance of around $100$~km from \lofar.
\end{description}

\subsection{Constraints}
\label{subsec:constraints}

\begin{list}{\textbf{--}}{}
\item The \uhep\ measurements require at least normal ionospheric conditions (no 
  scintillation) and the ability to determine the Dispersion Measure (DM) of the
  ionosphere from \textsc{gps} and \lofar\  calibrations.
\item Since the radio emission originates in \eas\ at altitudes below 10\,km 
  ionospheric conditions are irrelevant for the \vhecr\ and \hecr\ measurements.
  Meteorological environment parameters, such as pressure and temperature, on
  the other hand will be needed.
\item The stations should not be plagued by a large number of short \rfi\ pulses 
  and the instrument should be running properly.
\item Absolute calibration of the antennas should be available within a year.
\item The 1 month observations of \uhep\ and \vhecr\ in mode A should exclude 
  thunderstorms. However, in \vhecr\ mode we also do want to take data during 
  thunderstorms to determine the effect of thunderstorm electric fields on the 
  radio emission from \eas.
\item We have not yet tested the radio-only trigger concept of the \vhecr\
  and \hecr\ modes as, so far, our measurements have been done with the \lofar\ 
  Prototype Station \lopes\ at Karlsruhe where the trigger is provided by
  particle detectors. We will have to experiment with 5
  well-shielded particle detectors to initially provide a coincidence trigger. The
  installment of these particle detectors is foreseen.
\item The \vhecr\ and \hecr\ observations can, in general, be done while other
  observations are going on. Constraints are observations of \vhecr\ in mode A
  which requires all stations using the low-band antennas, and those observing
  programmes of the Transient KSP which use \tbb s. To prevent a possible conflict
  with the latter we propose to reserve 50\% of the \tbb s per station for CRs.
\end{list}

\subsection{Field choice}
\label{subsec:field choice}

\begin{itemize}
\item For \uhep\ measurements essentially the Moon is observed.
\item Not relevant for \vhecr\ and \hecr\ measurements. Good coverage of the
  entire sky, year and atmospheric conditions is desirable.
\item Switching to TS mode will take place based on the data provided by the
  electric field mills; if readouts indicate build-up or nearing of severe
  atmospheric conditions -- which will most likely will cause most astronomical
  observations to be shut down -- this observation mode will be activated.
\end{itemize}

\subsection{Timeline}
\label{subsec:timeline}

We are looking forward to take our first CR observations with \lofar\  as soon as the
first station is available, to extend our present results from \lopes. 
The 1 month \vhecr\ in mode A is requested as early as possible after the full 
array has been constructed. 
The first month of \uhep\ observations is requested as early as possible after 
that. The other measurements are expected to accumulate data as time goes on.

\subsection{Data products, deliverables of observations}
\label{subsec:data_products}

\subsubsection{CR detection}

For the \uhep\ mode the time series data of the formed beam for about 1\,ms
around  the trigger is requested.\\[0.3cm]
For the \vhecr\ and \hecr\ modes, the individual antenna voltage time series
are  requested for about 1\,ms around the trigger, e.g.\ assembled into event
files  with the data from one station each. Beam forming will be done off-line
to allow  for near-field beam forming.\\[0.3cm]
The characteristic amount of data per event is estimated as follows:

\begin{description}
\item[UHEP:]\ \\
  \sumline{Time series data from the formed beam\\
    $ \hbox{2 pol} \times
    2^{27} \hbox{ samples} \times
    8 \hbox{ Bytes/sample}$:}{\\2.0\,GB/event}
  \sumline{Raw time series data from individual dipoles\\
    $  77 \hbox{ stations} \times
    96 \hbox{ antennae} \times
    2 \hbox{ pol} \times
    2^{17} \hbox{ samples} \times
    2 \hbox{ Bytes/sample}$:}{\\\underline{3.6\,GB/event}}
  \sumline{Total:}{5.6\,GB/event}
\item[VHECR:]\ \\
  \sumline{Raw time series data from individual dipoles\\
    $ 32 \hbox{ core stations} \times
    96 \hbox{ antennae} \times
    2 \hbox { pol} \times
    2^{17} \hbox{ samples} \times
    2 \hbox{ Bytes/sample}$:}{\\1.6\,GB/event}
\item[HECR:]\ \\
  \sumline{Similar, but now only one station is involved\\
    $ 96 \hbox{ antennae} \times
    2 \hbox { pol} \times
    2^{17} \hbox{ samples} \times
    2 \hbox{ Bytes/sample}$:}{\\48\,MB/event}
\item[TS-mode] \ \\
  \sumline{Similar to \vhecr\ but full raw data from \tbb\\
    $ 77 \hbox{ stations} \times
    96 \hbox{ antennae} \times
    2 \hbox{ pol} \times
    2 \cdot 10^{8} \hbox{ samples} \times
    2 \hbox{ Bytes/sample}$:}{\\5.9\,TB/event}
\end{description}

The event rate is uncertain and estimated at one triggered event per station
per 10 minutes.

The data products, as listed above, need to be augmented by information on the 
atmospheric conditions (temperature, air pressure, humidity, etc.)\ and the overall
EM-environment. For the latter we require continuous dynamic spectra, independent
of whether CR are observed in a dedicated run or in piggy-back mode. 
This also includes measuring the atmospheric electric field, at least at ground 
level. Finally, data from
the set of particle detectors which will be installed at the core station, are required as well.

\subsubsection{Data monitoring}
  
To confirm that CR events occurred during special conditions such as lightnings
(see Subsection \ref{subsubsec:addmethods}), it is crucial to monitor the sky
by producing daily dynamic spectra. These give an overview of any kind of
detection exceeding the noise level of the telescope. These could be constructed from the
average power received by all antennae of the whole \lofar\, or separately for 
each station or dedicated beam respectively.

Figure \ref{fig:dstool} shows an example how to display those dynamic spectra.
By implementing an interactive graphical user interface, features such as burst
drifts, frequency fine structures, and periodicities can be analyzed. For the prototype
station \lopes\ such plots can be found in \cite{web:lopesmonitor}.
More examples can be found in \cite{web:quicklooks} and
\cite{web:osra-spectra}.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.75\linewidth]{figures/dstool}
  \caption{Display of a dynamic spectrum for one day with intensity in the
    time-frequency plain. The two additional graphs on the left and bottom are
    cross-cuts parallel to the frequency and time axis respectively.}
  \label{fig:dstool}
\end{figure}

By stretching or splitting the dynamic spectra in time finer details can be
resolved. By adding (average) cross-cuts parallel to the axes profiles in
frequency and time are obtained.

For the production of these dynamic spectra an unfiltered spectrum of one
millisecond can be downloaded once per minute from at least three antennae
($3 < N < 99$) per station. With 77~\lofar\ stations that gives 241 (up to 7623)
spectra with 31\,MB (up to 1\,GB) of data to be transferred per minute.

The dynamic spectra might be produced in one or multiple of the following modes:
\begin{itemize} \parskip 0pt
\item Average of antenna power spectrum (division by average spectrum to reduce
  \rfi); 
\item Multiple beam dynamic spectra (monitoring of the sky with beams suppressing
  man-made \rfi\ on the horizon); 
\item Beams in the directions of the Sun (32 arc-minutes) and Jupiter (46 arc-seconds).
\end{itemize}

For display purpose the following techniques can be used:
\begin{itemize} \parskip 0pt
\item Interactive dynamic spectrum with with cross-cuts (see Figure in this
  Subsection);
\item Antennae power as function of time for direct comparison;
\item Intensity-weighted averaging over frequency (center-of-mass equivalent);
\item Spectrum of light-curve\footnote{Dynamic spectrum collapsed along the
    frequency axis.} (to identify periodicity of the signal).
\end{itemize}


\subsection{Proposed CS1 Experiments}

\notes{This needs to be integrated into some text...}

\csoneexp{Test data for VHECR Trigger}
{Learn how to handle TBB time series data and get input for the VHECR 
trigger development.}
{Collect some time series data from the LBAs, and the Trigger-Alert messages 
generated during the same time. This can then be used to learn how to read 
in and process the time series data. And it will provide input for the 
development of the VHECR trigger. To get an overview over the different RFI
conditions this should be repeated a number of times, e.g., once per hour 
for one day+night.}
{CS10; LBA; TBBs collecting time series; different RFI conditions}
{Time series data for 1 sec of CS10; Dump of the Trigger-Alert messages.}

\csoneexp{Saturation data}
{Learn about the strongest pulses at the CS1 site.}
{Run a simplified version of the VHECR trigger routine to look for the strongest 
pulses around. Tune the parameters so that the trigger rate is between once per hour 
and once per day. After a trigger dump 10\,ms worth of data from all antennas centered
around the trigger time.}
{All antennas; LBA; TBBs collecting time series; whenever the LBAs are used;}
{Time series data from all antennas}

\csoneexp{Particle Detector Trigger}
{Do the particle detectors emit RFI? Do we see pulses from air showers?}
{Trigger CS1 by the signal from the particle detector array. Dump the TBB data for
every trigger. If necessary have the particle detectors emit a radio pulse to check
correlation between particle detectors and CS1.}
{CS10; LBA; TBBs collecting time series}
{Time series data for 1\,ms for every trigger.}

\csoneexp{VHECR Trigger Testing}
{Test the different VHECR trigger implementations}
{Run the VHECR trigger routines on the LCU. Use the results to improve the algorithm
and tune the parameters.}
{CS10; LBA; TBBs collecting time series}
{Time series data for 1\,ms for every trigger.}

\csoneexp{Polyphase Filter Bank inversion No.\,1}
{Collect test data for our polyphase filter bank inversion routine.}
{Get raw and ``polyphase filtered'' data of one antenna to test our inversion routine.}
{One antenna; LBA; Frequency bands: 50-82MHz}
{Time series data for 1 sec; PPF output for this second}

\csoneexp{Polyphase Filter Bank inversion No.\,2}
{Collect test data for our polyphase filter bank inversion routine.}
{Get raw and ``polyphase filtered'' data of one station to test our inversion routine.}
{CS10; LBA; Frequency bands: 50-82MHz; Beam forming to the zenith (all weights = 1)}
{Time series data for 1 sec; Beam former output for this second}

\csoneexp{Moon tracking}
{Show that moon tracking can be done.}
{Track the moon and record the total power in the beam. If possible do so when the
moon is occulting a strong source. Alternatively track a satellite that emits 
detectable signals.}
{All substations; tied array beam; LBA/HBA; Frequency bands: ???}
{Tied array beam data}

\csoneexp{CS1 -- NuMoon parallel run}
{Run CS1/LOFAR and NoMoon/WSRT in parallel.}
{To compare data from the UHEP mode and the NuMoon experiment let both run in 
parallel. First NuMoon and the LBAs collecting the tied array beam, then NuMoon plus 
the HBAs, and then NuMoon and UHEP trigger collecting triggered data.}
{All substations; LBA/HBA; Frequency bands: ???}
{Tied array beam data}

\csoneexp{Moon image}
{What does the moon look like at low frequencies?}
{Make a ``normal'' image of the moon at different frequencies. Also with the HBAs when
available.}
{All substations; LBA/HBA; Frequency bands: ???}
{(Un)cleaned image}

\csoneexp{UHEP Trigger Testing}
{Test the different UHEP trigger implementations}
{Track the moon with the tied array beam, feed the data into our UHEP trigger module
and generate triggers. Show that the triggers are generated within less than one 
second.}
{All substations; HBA}
{Function test}

