\documentclass[12pt]{lofar}
\usepackage{svendefs,texdraw}
%
% Abbr.
%\def\cep   {\textsc{cep}}
%\def\CR    {\textsc{cr}}
%\def\eas   {\textsc{eas}}
%\def\fpga  {\textsc{fpga}}
%\def\lcu   {\textsc{lcu}}
\def\lofar {\textsc{lofar}}
\def\lopes {\textsc{lopes}}
%\def\mtc   {\textsc{mtc}}
%\def\rfi   {\textsc{rfi}}
%\def\tbb   {\textsc{tbb}}
%\def\tc    {\textsc{tc}}
%\def\uhecr {\textsc{uhecr}}
%\def\vhecr {\textsc{vhecr}}
%
\include{definitions}%includes authors, titel, distribution list etc.
%
\renewcommand{\theTitle}   {The LOFAR VHECR Trigger}
\renewcommand{\theTAuthor} {A.\,Hornffer, S.\,Laf\`ebre}
\begin{document}
\maketitle
\newpage
%
\theDistributionList
\vspace{1cm}
\theDocumentRevision
\newpage

\tableofcontents
\newpage

\section{Purpose of this Document}

This document describes the set of software algorithms that together form
the VHECR trigger for \lofar. It is intended to be usable as the basis on 
which the \lofar engineering team can implement the trigger on the actual 
hardware.

It is expected to go through a number of iterations interleaved with 
discussions between the KSP members and the engineering team, as we strive 
to match the requirements of the CR-KSP with the abilities of the hard- and 
software.

\section{Overview}

One of the goals of the cosmic ray key science project of \lofar\ is to 
measure radio pulses from air showers produced by high energy cosmic rays. 
Air shower measurements are different from most other \lofar\ observations: 
They are not observed with long, continuous 
observations, but are observed in short events. These events need to be 
triggered either by an external trigger array of particle detectors or by
a trigger sensitive to the radio pulse of air showers.

The aim of the trigger algorithm is to trigger on all real cosmic ray events
but at the same time reduce the number of false triggers. 
In this case ``all real events'' means all cosmic rays within a certain
parameter range, e.g., primary energy, direction, and position on the ground.
If no parameter range can be found in which all events are detected (or if this
range is too small), then some scientific analyses become impossible as in 
these cases the trigger efficency\footnote{Trigger efficency is the number of 
triggered real events divided by the number of all real events.} may depend
on unknown parameters. Falsely triggered events are less of a problem for the
data analysis, but increase the load on the data transmission and storage 
systems.


\subsection{Properties of EAS Radio Pulses}

The radio pulses from air showers have some properties that sets them apart
from other radio pulses:
\begin{description}
\item[Random occurrence:]\clubpenalty1000\\
  Air shower events occur at random positions in the sky and at random 
  times. 
\item[Very short time scales:]\clubpenalty1000\\
  Air shower events are very short, they last for less than 20\,ns. So the 
  shape of the recorded pulse is given by the impulse response of the  
  electronics.
\item[Limited detection area:]\clubpenalty1000\\
  Air shower events illuminate a patch on the ground which is typically a few 
  hundred meters wide. That means that except for the center of the core only
  one station at a time is hit, and that not all antennas of a station may
  record a detectable pulse.
\item[Curvature of the pulse front:]\clubpenalty1000\\
  Since air shower events occur in our own atmosphere the radio pulse front 
  from an EAS is curved. The usual assumption of a flat pulse front as from a 
  source at infinite distance is no longer valid.
\end{description}

From the random occurrence follows that the trigger should watch as large a 
fraction of the sky for as long as possible. The other properties can be used
to distinguish real air shower events from false events.

\subsection{RFI}

For our purposes RFI can be separated into two groups: 
\begin{description}
\item[Continuous, narrow band transmissions]\clubpenalty1000\\
  All intentional transmissions are continuous on EAS time scales. (Even a 
  short pice of a conversation is much longer than an EAS radio pulse.) Nearly
  all of these are also narrow band.
\item[Short time pulses]\clubpenalty1000\\
  Sparks etc. produce radio pulses that are rather short in time and thus broad
  in frequency. As most of these pulses are non-intentional transmissions they
  are not transmitted with much power and thus are only detectable close to the
  source. Also most of these pulses are longer than EAS radio pulses.
\end{description}

The narrow band RFI can be filtered in frequency space, see 
appendix\,\ref{digital_filtering}. Its effect  is that it rises the effective 
noise that the trigger algorithm sees, thus preventing the triggering on small
pulses. The effect of pulsed RFI is that it can produce false single antenna 
trigger and thus raise the rate of false coincidences. This effect can be 
mitigated by having the trigger algorithm check the pulse shape and
not simply checking for coincidences but also checking for the arrival 
direction and the shape of the footprint.

\subsection{The VHECR Trigger}

The \lofar\ design groups antennas into stations. In every station, sets of 
eight antennas (with two polarization channels each) are connected to one 
dedicated \emph{transient buffer board} (TBB). All TBBs connect to the 
station's \emph{local control unit} (LCU). 
The footprint of the air shower radio signal is larger than the size of a 
station but smaller than the distances between stations (except for the center
of the core). So every station should have its own, independent trigger.
For big events, or smaller events in the core region the \emph{central 
processor} (CEP) should forward triggers to other stations as well.

The \lofar\ hardware design suggests to split up the trigger in the following 
levels, in order to minimize processing and network load:
\begin{enumerate}
\item\textbf{Monitoring single dipole data streams}\clubpenalty1000\\
  In the TBB all antenna streams are RFI filtered and searched for short 
  pulses. Detected triggers are forwarded to the LCU.
\item\textbf{Combining single antenna triggers}\clubpenalty1000\\
  In the LCU, single antenna triggers are `sanity checked'. If they compose
  a valid trigger pattern a local trigger is generated, resulting in a freeze
  and dump of the buffer. This trigger is also forwarded to the CEP.
\item\textbf{Analyzing and combining station triggers}\clubpenalty1000\\
  At the CEP, all events are analyzed. Depending on the event size, the 
  CEP may decide to forward the trigger to other core and\slash or remote 
  stations.
\end{enumerate}
Thus each level is a software or firmware implemented at one point of the 
\lofar\ system. The levels themselves may again be separated into different 
steps.

\section{Monitoring Single Dipole Data Streams}

\begin{figure}
\centertexdraw{
  \def\drawmtc(#1 #2){%
    \textref h:C v:C
    \move(#1 #2)
    \setgray 1
    \rlvec(1 0) \rlvec(0 1) \rlvec(-1 0) \rlvec(0 -1)
    \lfill f:.7
    \setgray 0
    \rmove(-1  .2) \ravec(1 0)
    \rmove(-1  .1) \ravec(1 0)
    \rmove(-1  .4) \ravec(1 0)
    \rmove(-1  .1) \ravec(1 0)
    \rmove(.5 -.3) \htext{{\sc mtc}}
    \rmove(-2 -.4) \rlvec(.1 .3) \rlvec(.1 -.3)
    \rmove(-.3 .0) \rlvec(.2 .3) \rlvec(.2 -.3)
    \rmove(-.3 .5) \rlvec(.1 .3) \rlvec(.1 -.3)
    \rmove(-.3 .0) \rlvec(.2 .3) \rlvec(.2 -.3)
  }
  \drawdim cm \linewd 0.02
  \arrowheadtype t:F
  \arrowheadsize l:.15 w:.1

  \move(2 0)
  \rlvec(3.2 0) \rlvec(0 5) \rlvec(-3.2 0) \rlvec(0 -5)
  \ifill f:.85
  \textref h:R v:T
  \move(5 4.8) \htext{\sc tbb}

  \drawmtc(2.2 0.2)
  \drawmtc(2.2 1.4)
  \drawmtc(2.2 2.6)
  \drawmtc(2.2 3.8)
  \move(4 2)
  \setgray 1
  \rlvec(1 0) \rlvec(0 1) \rlvec(-1 0) \rlvec(0 -1)
  \lfill f:.7
  \setgray 0
  \rmove(-.3  .2) \ravec(.3 0)
  \rmove(-.5  .2) \ravec(.5 0)
  \rmove(-.5  .2) \ravec(.5 0)
  \rmove(-.3  .2) \ravec(.3 0)
  \rmove(.5 -.3) \htext{\sc tc}
  \move(3.2 0.7) \rlvec(.5 0) \rlvec(0  1.5)
  \move(3.2 1.9) \rlvec(.3 0) \rlvec(0   .5)
  \move(3.2 3.1) \rlvec(.3 0) \rlvec(0  -.5)
  \move(3.2 4.3) \rlvec(.5 0) \rlvec(0 -1.5)
  \move(5.0 2.5) \ravec(1 0)
  \textref h:L v:C
  \rmove(.1 0) \htext{to {\sc lcu}}
}
\caption{\label{fig:tbb}
Schematic view of the transient buffer board (TBB). Eight antennas, 
with two dipoles each, are connected to four memory trigger controllers 
(MTCs). Their output is monitored by a TBB-controller (TC).}
\end{figure}

The first level of the trigger will be implemented on the FPGAs of the TBBs.
Figure\,\ref{fig:tbb} shows a schematic view of the transient buffer board.
Each TBB contains four MTCs, dealing with four channels each. Most of the
algorithm will run on the MTCs. As the MTCs have only limited resources it
is possible to trade the number of monitored channels for complexity of the
algorithm, up to monitoring only one out of four channels.

\subsection{Filtering}

To reduce the effective noise that the following stages sees the data streams
should be filtered for narrow band RFI. Due to the scarcity of resources on the
FPGA it was decided to use IIR notch filters for this (see 
appendix\,\ref{digital_filtering}). The frequency and width of the filters
will probably be different for different stations and may also change over 
time.

\subsection{Pulse Finding}

Once filtered, the MTCs should monitor the data stream for signals indicating 
a cosmic ray air shower. In most cases, this will express itself as a short, 
sharp pulse of $\sim20\,$ns wide.\footnote{Because of its narrowness, the 
pulse shape will resemble very much the impulse response function of the 
analog electronics instead of the natural shape of a radio emission
pulse.} In order to detect these pulses, the filtered data stream should be 
analyzed according to the algorithm
\begin{equation}\label{eq:peak}
	|x_i| > \mu_i + k\sigma_i\hfill
\end{equation}
where $x_i$ is the received signal, and $\mu_i$ is the time average of the 
absolute signal, 
running over a time interval containing $n$~samples. $\sigma_n$ is the 
standard deviation over the same block, and $k$ is the threshold factor 
for the standard deviation. As the standard deviation requires taking a square 
root, which is complicated on an FPGA, it will probably implemented as:
\begin{equation}\label{eq:peak-square}
	(|x_i|-\mu_i)^2 >  k^2\sigma_i^2\hfill
\end{equation}
The values of $n$ and~$k$ should be adjustable. A 
useful range for~$n$ is around 50 to 200~samples; an ideal value for~$k$ is 
expected to lie somewhere between 5 and~8.

As $\mu_i$ and~$\sigma_i$ are used only for determining interesting events, and
not for the analysis itself, they do not have to be exact. This opens the door 
to using faster algorithms at the cost of precision. One implementation of 
defining $\mu_i$ and $\sigma_i$ is to recalculate them only once
per block of $n$~samples instead of for every value of $x_i$. This cuts 
calculation time for these values by a factor of~$n$. Further performance 
improvements would be reached by using bit shift
operators (e.g. $\gg n'$ in C++) instead of dividing by $2^{n'}$.

Alternatively, to save memory in the FPGAs, an approximation of the running 
average could be used.
The new average and standard deviation would then be calculated as
\begin{align}\label{eq:sigmu-fpga}
	\mu_i      &= \frac{n-1}{n}\mu_{i-1}+\frac1n|x'_i| &
	\sigma_i^2 &= \frac{n-1}{n}\sigma_{i-1}^2+\mu_{i-1}^2-\mu_i^2+\frac{1}{n}|x'_{i}|^2
\end{align}
where $\mu_{i-1}$ and $\sigma_{i-1}$ are the average and standard deviation of 
the previous sample. Advantages of this algorithm are that the running average 
will be smoother and that it does not require all $n$ samples to remain 
directly accessible from memory. This even allows to greatly increase the value
of $n$ and thus use the same algorithm to look for a slower increase of the 
total power to, e.g., look for lightning. A disadvantage of this algorithm is
that it probably takes needs more multipliers which are already identified as 
a limiting resource of the FPGAs.

\subsection{Trigger Generation}

The first time a sample satisfies equation\,\ref{eq:peak} a trigger message
to the LCU should be generated. Following samples that above the threshold
($=$ satisfy equation\,\ref{eq:peak}) belong to the same trigger. If there are 
some samples  below the threshold followed by samples above the threshold, then
these samples also belong to the same trigger. Only when at least $m$ 
consecutive samples are below the threshold then the trigger is over. The 
optimal value of $m$ needs to be determined, it probably is between~3 and~16.
After such a gap in the pulse has been found the next sample above the 
threshold should start the next trigger.

A dead-time after one trigger has been detected is undesirable as it will 
affect the trigger efficency. Which will then affect the scientific analysis.
 
The trigger message from the TBB to the LCU should contain:
\begin{itemize}
\item The ID of the triggered channel.
\item The time of the trigger with sample time accuracy. E.g. the sample 
  number of the first sample above the threshold.
\item A value representing the width of the pulse. E.g. the number of samples
  between the first sample and the last above the threshold. If the width is 
  large (e.g. more than 255 samples) only the fact that this is a wide pulse
  is needed.
\item Desirable but not required is a value representing the height of the 
  pulse. This could the value of the largest sample, or the sum over all 
  samples in the pulse, or whatever is easier to implement.
\end{itemize}

\section{Combining Single Antenna Triggers}

The first two steps that the LCU should perform on the triggers are a check of 
the pulse width and a coincidence trigger.

The pulse width is checked by testing whether the width value falls into a 
given range that is compatible with the width of an EAS radio pulse. This range
could also be adjusted to the pulse height.

For the coincidence trigger the LCU should sort triggers from all TBBs by time.
When at least $N$ triggers fall within a time window~$t$ a coincidence trigger
is detected. The value of $t$ should be a little more than the light travel 
time across the entire station, the value of $N$ needs to be figured out
and is dependent on the number of channels that is being monitored.

This test and simple coincidence trigger should be performed in any case. 
After this, however, there are several possibilities:
\begin{itemize}
\item The trigger is accepted as-is.
\item The direction of the pulse is determined by one of the following methods:
  \begin{itemize}
  \item An analytical direction fit is performed on the antenna trigger 
    arrival times.
  \item An analytical source position fit is performed on the antenna trigger 
    arrival times.
  \item A least squares fit for a plane wave front is performed on the arrival 
    times.
  \item A least squares fit for a spherical shell wave front is performed on 
    the arrival times.
  \end{itemize}
\item The footprint of the pulse is analyzed.
\end{itemize}
A detailed description and discussion of pros and cons of the direction finding
methods is in appendix~\ref{directions}, a discussion of the footprint
analysis is in appendix~\ref{footprint}. If the antenna trigger times do
not conform with a signal from one direction or if the signal originates from
a point at the horizon or close to the station the trigger should be 
rejected. It should also be rejected if the footprint does not match the shape
expected from an air shower or if it does match the shape expected from RFI.

In the beginning the coincidence trigger will be accepted as-is. The data 
collected this way can then be used to test and develop the trigger algorithm.

When the decision for a trigger is made, the LCU signals all TBBs (even those
that didn't have a trigger or didn't even take part in the triggering) to 
freeze the buffer and to send the relevant part of the raw time data of all 
antennas. 
The amount of data per antenna should be about 1\,ms, e.g., $2^{17}$~samples of
data ($\sim0.66\,$ms). More data takes more time to transfer and needs more 
storage space, less data reduces the frequency resolution of a Fourier 
transform and may complicate correlating data from different stations.

The raw data from the TBBs it is sent to the \lofar\ central processor (CEP) 
together with some statistical information concerning the event, such as 
station ID, time, significance, triggered antennas etc. The total amount of 
data to be transferred then amounts to approximately $\sim25$\,MB of data per 
station. 
Since the data production is not continuous and not extensive, the data 
transfer can be performed without disturbing the data acquisition of other 
observations.

When a trigger is detected (and before the data is transfered) the LCU also
sends a message to the CEP. This message should contain:
\begin{itemize}
\item The station ID.
\item The time of the trigger, with sub-$\mu s$ accuracy. E.g. the trigger time
  of the first antenna, or the average of all antenna trigger times.
\item A value about the size of the air shower. This could be the number of 
  triggered antennas, or preferably a value calculated from the single antenna 
  pulse heights, e.g., the maximum pulse height or the mean of the pulse 
  heights.
\item If available the estimated direction of the air shower.
\end{itemize}

For stations in the core it would also be useful to send a message to the CEP
when a coincidence has been detected that was just not large enough for a 
trigger, but could be if other stations had also a coincidence. In this case
the trigger messages from all antennas should be sent.

A limiting factor for the complexity of the trigger algorithm is that the TBBs
have to be notified while the data is still in the buffer. So the time from
the data arriving at the TBBs until the TBBs receive the message to freeze the 
buffer has to take less than 1\,s. Additionally the trigger algorithm must not
interfere with regular function of the LCU.

\section{Analyzing and Combining Station Triggers}

The CEP has two general functions. The first is to generate a trigger for air 
showers that hit the center of \lofar\ and did not trigger all stations that 
are hit. The second is to trigger additional stations so that more information 
is available if a special event has been found.

The first is only applicable for stations in the center of \lofar\ where the 
typical distance between stations is less than the typical footprint size of 
an air shower, which is in the order of several hundreds of meters. 
It could be implemented in two ways:
\begin{itemize}
\item If the stations also send messages to the CEP when a coincidence was 
  detected that was not large enough for a full station level trigger, then 
  these messages can be checked in the same way as the stations level trigger
  and if the combined data values a trigger all involved stations are 
  triggered.
\item If no such messages are send to the CEP, then the stations close to a 
  triggered station are also triggered.
\end{itemize}

The second function concerns all stations. For the in-depth analysis of 
special events additional data from more or even all stations is desirable.
Currently the definition of special event includes two cases: Extra large 
events and coincides of events. The first case would be detected when either
several stations report a trigger at the same time, or when a station
reports an event with a large pulse height.
The second case would be detected when two or more stations that are more than 
a few hundred meters apart have an internal trigger within a short time window.

The exact parameters for this still have to be figured out. They are also 
adjusted to different observing modes of \lofar. E.g. during special CR 
observations they are set to more sensitive values resulting in more triggers
and thus a higher data transfer load, while during other observations they are
less sensitive or even switched off.

\appendix

\section{Digital Filtering}
\label{digital_filtering}

\subsection{IIR and FIR Filters}

The most thorough way of filtering narrow band RFI is to Fourier transform the 
data to frequency domain, flag the RFI, and transform it back to time domain.
Unfortunately this takes significant computing resources and is not possible
to do in real time on the FPGAs of the TBBs.

The alternative is to use time domain based filtering of the data. In these
filters the filtered sample depends on the 
previous values of raw and filtered samples in the following manner:
\begin{eqnarray*}
	x'_i & = & b_0 x_i + b_1 x_{i-1} + b_2 x_{i-2} \dots \\
             &   & - a_1 x'_{i-1} - a_2 x'_{i-2} - a_3 x'_{i-3} \dots
\end{eqnarray*}
where $x'_i$ represents the filtered (output) sample~$i$, $x_i$~is the 
unfiltered (input) sample~$i$.

If a filter has both nonzero $a$s and nonzero $b$s it is called an
infinite impulse response (IIR) filter, a filter with all $a$s equal to zero
is called a finite impulse response (FIR)\footnote{The names are due to the 
fact that an IIR filter can in theory give an infinite answer to a short 
impulse, while a FIR filter can not.} filter. 

An example of a notch (band-reject) IIR filter is:
\begin{align*}
	\vec b   &= [1, -2\cos\omega_0, 1] &
	\vec a   &= [1+\alpha, -2\cos\omega_0, 1-\alpha]
%	\vec a   &= [0, -2\cos\omega_0, 1-\alpha]
\end{align*}
and
\begin{align*}
	\omega_0 &= 2\pi\frac{\nu_c}{\nu_s} &
	Q        &= \frac{\nu_c}{2\pi\Delta\nu} &
	\alpha   &= \frac{\sin\omega_0}{2Q} 
\end{align*}
where $\nu_c$ is the center frequency, $\Delta\nu$ is the width 
of the filter, and $\nu_s=200\,$MHz is the sampling rate.

Putting a few of those notch filters at the frequencies of the strongest
RFI sources can reduce the effective noise that the triggering algorithm sees
and thus increase its sensitivity.

\subsection{Effect of IIR Filters on LOPES Data}

To test the effect of IIR notch filters, as described in the previous section,
on \lopes\ data several events were tested. The selected events were those 
with the biggest signal to noise in the beam formed data. The narrow band RFI 
was filtered either by the standard lopestools FFT based filter, by four IIR
notch filters at 50, 60, 62.2, and 67.8\,MHz, or there were not filtered at 
all. The following values were calculated:
\begin{itemize}
\item The RMS of the filtered data for single antennas before taking the 
  absolute value. Calculated on a time-span well before the trigger. (That
  means that no pulsed RFI from the particle detectors is included.)
\item The signal to noise ratio for single antennas. Defined as the maximum 
  value of the absolute data during the peak, divided by the RMS of the 
  absolute value from the same time-span as above.
\item The number of antennas with detected peaks. The peak detection was 
  intended to resemble the way it would be done in reality. It was done
  according to equation\,\ref{eq:peak} with the $\mu$ and $\sigma$ calculated 
  with equation\,\ref{eq:sigmu-fpga}. The block size $n$ was set to 100 and
  the factor $k$ was adjusted so that for all three cases the total number of 
  samples above threshold was about the same. So the $\mu$ and $\sigma$ were
  calculated differently and for different data than for the other toe fields!
\end{itemize}
The following table shows the results from four events:

\begin{tabular}{ccccc}
Event ID & Filter & RMS$^*$ & SNR$^*$ & Detected peaks\\
\hline
  8 & FFT & 0.018/0.026/0.038 & 21.4/36.3/48.9 & 8\\
    & notch & 0.029/0.043/0.069 & 13.1/23.3/31.0 & 8\\
    & unfiltered & 0.045/0.064/0.089 & 8.9/16.7/23.2 & 8 \\
\hline
 62 & FFT & 0.02/0.028/0.048 & 5.2/7.0/11.3 & 4 \\
    & notch & 0.032/0.045/0.084 & 3.8/6.0/8.0 & 4 \\
    & unfiltered & 0.048/0.072/0.094 & 2.7/4.0/5.8 & 1 \\
\hline
108 & FFT & 0.024/0.034/0.05 & 3.8/8.2/12.4 & 5\\
    & notch & 0.04/0.06/0.09 & 2.9/5.5/8.5 & 3 \\
    & unfiltered & 0.056/0.088/0.112 & 3.1/5.4/7.7 & 1 \\
\hline
280 & FFT & 0.023/0.033/0.064 & 5.3/7.3/11.2 & 6 \\
    & notch & 0.038/0.054/0.103 & 2.7/5.6/9.2 & 1 \\
    & unfiltered & 0.041/0.065/0.09 & 2.7/4.7/6.3 & 1 \\
\hline
300 & FFT & 0.02/0.033/0.076 & 3.6/10.5/16.4 & 8\\
    & notch & 0.035/0.052/0.113 & 2.4/6.7/13.1 & 6\\
    & unfiltered & 0.047/0.07/0.104 & 2.9/5./9.3 & 2 \\
\end{tabular}\\
$^*$: minimum/mean/maximum of all antennas

The events not listed here showed all the same qualitative results.
The FFT based filtering shows the best performance, the unfiltered data gives
the worst performance, and the IIR notch filters give a performance in between.
Event 8 shows that for large events the filtering is not really necessary as
they are detected anyhow. The other events show that filtering can push events
above the triggering threshold. Event 280 shows that the quality of the 
filtering can make a big difference by pushing many antennas above the 
threshold which otherwise would be just below it. (Events which even FFT 
filtering cannot recover are just not listed here.)


\section{Direction Finding}
\label{directions}


\subsection{Analytical direction finding}
The direction of incidence of a signal can be found from the time delays in the different antennas. 

\subsection{Analytical source finding}
Using a technique called \emph{trilateration}, akin to triangulation, we can find an exact solution
for the source position. In order to do this, we make the assumption that the wavefront of coherent
radio emission is spherically symmetric. For every coincidence trigger (as explained in the previous
section), an additional estimate is made for the position in space where the radio pulse was
emitted.

In a cosmic ray event, we have a set of measurements $[t_i,\vec r_i]$ for every antenna~$i$, where
$t_i$~is the arrival time, and $\vec r_i=(x_i,y_i,z_i)^T$ is the antenna position. If we have this
information for two antennas, we can determine one angle of the source position, so that it is
confined to a hyperboloid. For three antennas, the solution space is contracted to the intersection
of two hyperboloids, yielding a curve. If we add yet another antenna to our array, this contracts
even further, and theoretically this is enough to exactly pinpoint the source of the signal. Adding
more information by using extra antennas will make the source position overdetermined. Using four
antennas, then, we need to solve the system
\begin{align}\label{eq:sf}
 ct_{i0} &=|\vec r_{i0}|\nonumber\\
         &=\sqrt{(x_{i0})^2+(y_{i0})^2+(z_{i0})^2}
\end{align}
where the index~0 represents the emission source and indices $i=1,2,3,4$ represent the four
antennas. Double sub indices indicate a subtraction: e.g.\ $t_{ij}=t_i-t_j$. Through various
substitutions, it can be shown (c.f.\ \cite{yu}), that
\begin{align}
	z_0 &=-\frac{A}{2B}\pm\sqrt{\bigg[\frac{A}{2B}\bigg]^2-\frac{C}{B}}, &
	x_0 &=Dz_0+E, &
	y_0 &=Fz_0+G,
\end{align}
are the coordinates of the source position. They depend on the antenna positions and delays in a
rather complicated manner. The variables $A$,~$B$, and~$C$, for starters, are defined as
\begin{align*}
	A  &=2\big[D(E-x_1)+F(G-y_1)-z_1-HI\big],\\
	B  &=D^2+F^2+1-H^2,\\
	C  &=(E-x_1)^2+(G-y_1)^2+z_1^2-I^2,
\end{align*}
and
\begin{align*}
	D  &=\frac{Y_1Z_2 - Y_2Z_1}{X_1Y_2 - X_2Y_1}, &
	E  &=\frac{Y_2T_3 - Y_1T_4}{X_1Y_2 - X_2Y_1}, &
	F  &=\frac{X_2Z_1 - X_1Y_2}{X_1Y_2 - X_2Y_1}, &
	G  &=\frac{X_1T_4 - X_2T_3}{X_1Y_2 - X_2Y_1}.
\end{align*}
Continuing,
\begin{align*}
	H  &=\frac{1}{ct_{12}}(x_{21}D+y_{21}F+z_{21}), &
	I  &=\frac12 ct_{12}+\frac{1}{2ct_{12}}\big[2(x_{21}E+y_{21}G)-(r^2)_{21}],
\end{align*}
where $(r^2)_{ij}=(x_i^2+y_i^2+z_i^2)-(x_j^2+y_j^2+z_j^2)$. Furthermore
\begin{align*}
	X_1 &=t_{12}x_{31} - t_{13}x_{21}, &
	Y_1 &=t_{12}y_{31} - t_{13}y_{21}, &
	Z_1 &=t_{12}y_{31} - t_{13}z_{21},\\
	X_2 &=t_{12}x_{41} - t_{14}x_{21}, &
	Y_2 &=t_{12}y_{41} - t_{14}y_{21}, &
	Z_2 &=t_{12}z_{41} - t_{14}z_{21},
\end{align*}
and, nearly there,
\begin{align*}
	T_3 &=\tfrac12\big[c^2t_{12}t_{13}t_{32}+t_{12}(r^2)_{31}-t_{13}(r^2)_{21}\big], &
	T_4 &=\tfrac12\big[c^2t_{12}t_{14}t_{42}+t_{12}(r^2)_{41}-t_{14}(r^2)_{21}\big].
\end{align*}
Such a calculation can be done easily in the LCU by defining the variables in the above equations.

The calculation should be carried out preferably with antennas which are separated by long
baselines, in order to increase position accuracy. Doing the above calculation for several
combinations of antennas yields several different source positions. Now if the trigger was due to
noise, these positions should be uncorrelated and lie at random coordinates, not close to each
other. If not, their positions (or at least their directions) should be similar and one could
distinguish between local events (less than, say, a few hundreds of meters away) and events which
occurred farther away.

An advantage of this method is its fast algorithm, which will practically always yield an answer,
except when the distance to a source is \emph{exactly} infinite. A disadvantage is that, given the
fairly limited baselines compared to the distances of real events, the distance is not very accurate
for real cosmic ray events, and also the spherical shell approximation is not entirely correct.

\subsection{Direction fitting}
TBW

\subsection{Source fitting}
Besides analytical source fitting on four antennas, we can also take all antennas at the same time
and fit a source position somewhere in the sky. Again, we make the assumption that the wavefront of coherent
radio emission is spherically symmetric and make the additional requirement that a source position can be
determined before a station level trigger is issued.

In this section, we will discuss a nonlinear least squares method known as the
\emph{Levenberg-Marquardt algorithm}, which is essentially taken
from \cite{nr}.

The least squares approach is to find optimal fit parameters by minimizing the square of the equation to fit.
We can formalize this as finding the minimum of the function
\begin{equation}
	\chi^2(\vec r_0)=\sum_{i=1}^{n}F_i^2(\vec r_0)
\end{equation}
where, starting again from equation~\ref{eq:sf}, and squaring it, we have
$$
  c^2(t_{i0})^2=(x_{i0})^2+(y_{i0})^2+(z_{i0})^2
$$
so that
$$
  F_i(\vec r_0)\equiv c^2t_{i0}^2-|\vec r_{i0}|^2.
$$
and the sum is over all triggered antennas. For a minimum to be achieved, note that the gradient $\nabla_0
\chi^2(\vec r_0)$ must be zero. The components of this vector are:
\begin{align*}
  %\frac{\partial \chi^2(\vec r_0)}{\partial k_0}=\sum_{i=1}^n 4(k_i-k_0)\Big[c^2\tau_i^2-|\vec r_i-\vec r_0|^2\Big]
  \frac{\partial \chi^2(\vec r_0)}{\partial p_0}=4\sum_{i=1}^n (p_i-p_0)F_i(\vec r_0)
\end{align*}
for $p=x,y,z$. Take second partial derivatives to obtain the Hessian matrix. Its elements are:
\begin{align*}
  \frac{\partial^2 \chi^2(\vec r_0)}{\partial p_0^2}
	&=8\sum_{i=1}^n (p_i-p_0)^2-4F_i(\vec r_0)	&(p&=q)\\
  \frac{\partial^2 \chi^2(\vec r_0)}{\partial p_0\,\partial q_0}
	&=8\sum_{i=1}^n (p_i-p_0)(q_i-q_0)		&(p&\ne q).
\end{align*}
This matrix will play a crucial role in determining the minima of our solution space.

The Levenberg-Marcquardt algorithm, which we will use to solve the actual minimization,
approaches the problem in the following way. First, let us define
$$
  \beta_{p}\equiv\frac12\frac{\partial \chi^2}{\partial p_0}
$$
and
\begin{align*}
  \alpha_{pp}&\equiv\frac12\frac{\partial^2 \chi^2}{\partial p_0^2}(1+\lambda)\nonumber\\
  \alpha_{pq}&\equiv\frac12\frac{\partial^2 \chi^2}{\partial p_0\,\partial q_0}  &(p\ne q).
\end{align*}
The equation we will solve in this method, is
\begin{equation}\label{eq:levmar}
  \sum_{q=x,y,z}\alpha_{pq}\,\delta q_0=\beta_p
\end{equation}
where $\delta \vec r_0=(\delta x_0,\delta y_0, \delta z_0)^T$ is the estimated change to be applied
to $\vec r_0$ in the next iteration step.  $\lambda$~is a fudge factor that allows us to control the
type of step to be taken: if $\lambda$~is big, we will just go down along the gradient. If it is
small, we have a much more delicate approximation, but steps will be small: inconvenient if we are
far from the minimum.

Deciding on the value of~$\lambda$ will be done according to the following recipe:
\begin{enumerate}
\item
  Given an initial value for $\vec r_0$, calculate $\chi^2(\vec r_0)$.
\item
  Choose a value for $\lambda$ ---~say 0.001.
\item While $\chi^2$ remains decreasing significantly, do the following:
  \begin{itemize}
  \item
    Solve (\ref{eq:levmar}) to obtain a value for $\delta\vec r_0$. Compute $\chi^2(\vec
    r_0-\delta\vec r_0)$.
  \item
    If $\chi^2(\vec r_0-\delta\vec r_0) \ge \delta\vec r_0$, increase~$\lambda$ by a significant
    factor.
  \item
    If $\chi^2(\vec r_0-\delta\vec r_0)  <  \delta\vec r_0$, decrease~$\lambda$ by a significant
    factor.
  \end{itemize}
\item
  Set $\lambda=0$ to obtain the final value for~$\alpha_{pq}$, and calculate~$\alpha^{-1}$, which is
  the estimated covariance matrix of the standard errors in~$\vec r_0$.
\end{enumerate}

The advantage of a fit instead of an analytical approach, is that it has `fuzzy' solutions: even if
the conditions are not perfect, we will get a sensible result. A major disadvantage is exactly this
fuzziness: it is an iterative algorithm, and there is no guarantee that a solution exists at all, or
that it can be found by iteration. This is because the algorithm, which tries to find minima in the
solution space, might get trapped in a local minimum instead of iterating towards the `real'
minimum. Our kind of minimization problem in particular has a very uneven solution space, with lots
of local minima. It is therefor recommended not to use a fitting algorithm as a trigger mechanism at
all.

\subsection{Error sources in position estimates}\label{sect:error}
\begin{itemize}
\item
The limited time resolution of the received signal $\delta t$ introduces an uncertainty $|\delta\vec
r|=c\,\delta t$. In the case of LOPES, where $\delta t=12.5\,$ns, we find $|\delta\vec r|=3.75\,$m. 
\item
Because the signal is propagated through the atmosphere, the speed of light is slightly lower than
in vacuum. Furthermore, it may not be constant. If we allow a maximum deviation of $3\cdot10^{-4}$
in the speed of light, (which is greatly exaggerated), the uncertainty in the obtained position is
$2\,$m.
\end{itemize}

\section{Footprint Analysis}
\label{footprint}

Air showers emit radio emission into a cone in the forward direction, so it 
illuminates an ellipsoidal area on the ground.  
Inside the ellipse the field strength drops with an exponential decrease with 
distance from the shower axis, at the edge of the ellipse the field strength 
drops much sharper. 

One check is the see whether the footprint is patchy. If there are antennas 
well above the threshold next to working antennas that didn't trigger this is 
a sign for non air shower event.

Another method is to analyze the footprint profile.
An air shower event should show the exponential decrease with distance 
from the shower axis. If the data shows the $^1/_{R^2}$ %$\frac{1}{R^2}$ 
decrease of a close source this is a sign for non air shower event.
But whether the triggering data is sufficient for this kind of analysis 
remains to be seen.

\begin{thebibliography}{100}
\frenchspacing
\bibitem{mem018}R. Braun, \emph{Transients requirements}, \textsc{lofar-astron-mem-018}
\bibitem{rpt065}G. Schoonderbeek, A. Gunst, \emph{Transient buffer board specification},
        \textsc{lofar-astron-rpt-065}
\bibitem{nr}W.H. Press, \emph{Numerical recipes in C: the art of scientific computing}, Cambridge
        University Press, 1988
\bibitem{yu}K. Yu \& I. Opperman, ``Performance of UWB position estimation based on time-of-arrival
        measurements'', \emph{Proc. IEEE UWBST and IWWAN 2004}, pp.~400--404
\end{thebibliography}
\
\end{document}
