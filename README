
CONTENTS:

  - submitting
  - Layout of the repository
  - Generation of documents from LaTeX sources
  - Output file sizes

_________________________________________________________________________________
>>> Submitting
to submit files, you need to have a svn account, please contact Arno Schoenmakers 
(schoenmakers@astron.nl) or Marcel Loose (loose@astron.nl)

_________________________________________________________________________________
>>> Layout of the repository

The documents repository as a rather minimal layout, in order not hide document
within some deep structure:

.
|-- build           ...  Directory where to build documents from LaTeX sources
|-- ADD             ...  Architectural Design Documents
|-- ICD             ...  Interface Coontrol Documents
|-- Minutes         ...  Minutes from various meetings, workshops, etc
|-- Presentations   ...  Presentations given at meetings, workshops, conferences, etc
`-- Tutorials       ...  Tutorials how to use individual pieces of software

_________________________________________________________________________________
>>> Generation of documents from LaTeX sources

1) Using CMake

If you are using CMake (www.cmake.org) on your system, you can profit from the 
add-on functionality build into the configuration scripts:

    cd build
    cmake ..

If you now type 

    make help

you will be presented with a list of build targets; while there is a target for 
each individual document, also a number of collective targets are available, such
as e.g.

    make ICD

which will cause the generation of PDF documents from all ICD LaTeX sources.

2) Without using CMake

In order to e.g. generate the ICDs, change into the directory "ICD" and create
symbolic links for the style file required to define macros and layout parameters:

    cd ICD
    ln -s ../usg.sty .
    ln -s ../svninfo.sty .

Once this is done you can place a plane call to LaTeX (and BibTeX in order to
generate the bibliography), e.g.

    latex  LOFAR-USG-ICD-004
    bibtex LOFAR-USG-ICD-004
    latex  LOFAR-USG-ICD-004

or import this directory into a GUI based typesetting environment.

3) Partial use of CMake

If you have CMake available, but still choose to process the LaTeX input files
yourselve, you at last can make use of CMake to ensure the infrastructure is set
up properly, e.g. by creating the required symbolic links to style files:

    cd build
    cmake ..


_________________________________________________________________________________
>>> Output file sizes

When run through the LaTeX -> dvips -> ps2pdf chain the resulting file sizes are:

-rw-r--r--  1 lars  staff   418694 Mar 30 11:48 LOFAR-USG-ICD-001.pdf
-rw-r--r--  1 lars  staff   362135 Mar 30 11:48 LOFAR-USG-ICD-002.pdf
-rw-r--r--  1 lars  staff  2361535 Mar 30 11:49 LOFAR-USG-ICD-003.pdf
-rw-r--r--  1 lars  staff   717591 Mar 30 11:48 LOFAR-USG-ICD-004.pdf
-rw-r--r--  1 lars  staff    61425 Mar 30 11:48 LOFAR-USG-ICD-005.pdf
-rw-r--r--  1 lars  staff   686750 Mar 30 11:48 LOFAR-USG-ICD-006.pdf
-rw-r--r--  1 lars  staff   643133 Mar 30 11:48 LOFAR-USG-ICD-007.pdf
-rw-r--r--  1 lars  staff   707074 Mar 30 11:48 LOFAR-USG-ICD-008.pdf

If in comparison using the LaTeX -> dvipdfmx chain, the is a notable difference in
the size of the generated files:

-rw-r--r--  1 lars  staff   305527 Mar 30 11:45 LOFAR-USG-ICD-001.pdf
-rw-r--r--  1 lars  staff   243121 Mar 30 11:45 LOFAR-USG-ICD-002.pdf
-rw-r--r--  1 lars  staff  3733977 Mar 30 11:46 LOFAR-USG-ICD-003.pdf
-rw-r--r--  1 lars  staff  1072571 Mar 30 11:45 LOFAR-USG-ICD-004.pdf
-rw-r--r--  1 lars  staff    38757 Mar 30 11:45 LOFAR-USG-ICD-005.pdf
-rw-r--r--  1 lars  staff  1087314 Mar 30 11:46 LOFAR-USG-ICD-006.pdf
-rw-r--r--  1 lars  staff  1034804 Mar 30 11:46 LOFAR-USG-ICD-007.pdf
-rw-r--r--  1 lars  staff  1083422 Mar 30 11:45 LOFAR-USG-ICD-008.pdf

The main trend in this appears to be, that document containing a larger number
of postscript figures will grow in size, while documents mainly consisting of
text, will result in a smaller output PDF file.
