\documentclass[a4paper,10pt]{scrartcl}
\usepackage{amsmath,amssymb,usg,texdraw}

%% Typesetting macros

%\def\eas   {\textsc{eas}}
%\def\fpga  {\textsc{fpga}}
%\def\lcu   {\textsc{lcu}}
%\def\mtc   {\textsc{mtc}}
%\def\rfi   {\textsc{rfi}}
%\def\tbb   {\textsc{tbb}}
%\def\tc    {\textsc{tc}}

%% Begin of document ------------------------------------------------------------

\begin{document}

\title{Trigger Algorithm for UHEP mode detection of LOFAR-CR} 
\author{K. Singh}
\date{\small Version of \today}

\maketitle

\tableofcontents

\section{Purpose of this Document}

This document describes the trigger algorithms that form Ultra High Energy Cosmic 
Rays (UHECR) for LOFAR Cosmic Rays Key Science Project (CR-KSP). 
Aim is to implement the trigger algorithm for this mode of observation 
at Central Processing (CEP) according to the need of LOFAR CR-KSP to 
fulfill the requirement of external particle detectors. 
Indeed this trigger sensitive algorithm should be compatible with the 
requirement of observation and available processing resources at CEP.

This trigger algorithm should be able to discriminate the real cosmic ray events
with radio interference to reduce the load on data transmission and storage systems. 

\section{Overview}

To observe UHECRs ( $>10^{20}$ eV ) we are planning to explore the possibility 
of using the Moon as a detector. Aim is to cover the whole visible lunar
surface with several tied array beams (formed at CEP) and to look for short
pulses of Cherenkov radiation emitted by showers induced in the lunar crust
when the cosmic rays strike it. 

The array of LOFAR core stations will be configured into the tied array 
mode and beams will be formed in the direction of the Moon with 32 MHz 
bandwidth for the frequency range of 110-190 MHz. If a pulse of pre-defined 
strength is found, then CEP will send a command to trigger the buffer 
boards (Transient Buffer Boards - TBBs) at stations to save data on
central storage system.


\subsection{ Overview of Online Processing}

Since we are interested in frequency range 110- 190 MHz, the receiver control
unit (RCU) will select High Band Antennas (HBAs) of LOFAR stations. 
The A/D converter converts the analogue signal into a $12$ bit  digital 
signal at a maximum sampling rate of  200 MHz. Each 
(digital) Receiver Signal Processing (RSP) board receives signals from 4 dual 
polarized antennas, so 8 signals in total. On the RSP board, each input signal 
is filtered in a polyphase filter (PPF), (see
Fig.~\ref{fig:online-processing-HECR-mode}), 
consisting of FIR filter banks of $15^{th}$ order and an IDFT stage 
[Reference of the document about PPF inversion.......].
After implementation  of the polyphase filter, the band is split into $512$ 
equidistant subbands, which are on a grid in frequency space. The negative 
part of original spectrum is omitted, i.e., the real input signal is from 
here on represented by complex signals.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\textwidth]{figures/HECR.eps}
  \caption{Online processing of HECR mode.}
  \label{fig:online-processing-HECR-mode}
\end{figure}

Each subband signal is decimated with a factor of 1024 after filtering. Hence, 
the clock rate after filtering is reduced by a factor of 1024. From the
resulting 512 frequency subbands 165 frequency subbands can be selected for 
further processing, compatible to the processing capacity available at
Central Processor (CEP), BlueGene in Groningen. The selection is controlled 
centrally at the station by the Local Control Unit (LCU). The selected 
subbands have a maximum total effective bandwidth of 32 MHz per polarization. 
This bandwidth is matched to the capacity of the Central Processor (CEP).

To form beams, the antenna signals are combined (beamlets), this is done 
with independent beamformers for each subband. The weights necessary for the
beam-former are calculated in the LCUs and are sent to the beam formers each
second in order to follow sources while the earth rotates. Additionally,
statistical measurements are performed at the station. In this way, we can make
one beam of 32 MHz, or 8 different beams with each 4 MHz bandwidth.

In parallel, with the filtering and beam-forming the digitized raw data can be
stored in a transient buffer board (TBB)(
Fig.~\ref{fig:online-processing-HECR-mode}) which holds 1 second of raw data that
can be ``frozen'' and accessed afterwards upon detection of specific transient
events. Freezing of the buffer contents can be controlled by external triggers.
The stored data or selections thereof can be sent for CEP for further processing.

Therefore, our aim is the generation of external trigger that defines when the
data from the TBBs is to be dumped. 

\subsection{ UHEP mode processing }

The trigger algorithm is essentially a software that runs on CEP level and has
two major steps:

\begin{itemize}

\item \textbf{PPF Inversion (Interpolation):} \\
  This inverts the effects of the poly-phase filter bank, which previously has
  split the frequency range into a number of channels. 

  There the data streams from all stations assigned to one sub-division are
  combined in tied-array mode so that for each sub-division one data stream
  remains (see Fig.~\ref{fig:online-processing-UHEP-mode}). In order to achieve
  (i) optimal field of view and (ii) enable later coincidence tests, 
  the LOFAR central core will be subdivided into 4 group of stations
  \footnote{This number has to be optimised, not yet fully determined}, each of
  these groups providing input to a separate beam. 

  The resulting voltage sum output of each tied array beam will be presented
  as multiple spectral channels, each composed of a time series data with 
  sample rate set by the inverse of channel bandwidth $\sim 5\mu \mathrm sec$. 
  To search for suitable pulses data has to be transformed back to the time
  domain to produce high time resolution ($\sim \mathrm ns$). This can be
  achieved by the exact inversion of what has been implemented at station level,
  more specifically by the inversion of polyphase filters.  

  
\item \textbf{Peak detection:}\\
  If pulse is detected in time series data streams in all subdivisions, a signal is 
  sent to the TBBs of all stations to freeze the buffer and dump the data.
  
  A coincidence test has to be performed on beams of different groups depending on 
  their configuration as:
  
  (a) All beams directed towards the same position; to be indentified as such, an 
  event must detected in multiple beams, forming an coincidence trigger.
  
  (b) Beams covering disjunct areas on the lunar surface, an event must be
  detected in 1-2 beams (depending on exact arrangement of beams) forming an
  anti-coincidence trigger.
\end{itemize}

This mode is triggered by in-beam pulse detection after dedispersion for
ionospheric effects, i.e., after the corrections for phase corruptions introduced
by the Earth's atmosphere as shown in Fig.~\ref{fig:cep-processing}, since we
are interested in phase modifications, ionospheric de-dispersion will be
performed before the inversion of Polyphase filetrs and hence transformation of
signal into time domain.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\textwidth]{figures/UHEP.eps}
  \caption{Online processing of UHEP mode.}
  \label{fig:online-processing-UHEP-mode}
\end{figure}


\begin{figure}[htbp]
  \centering
  \includegraphics[width=\textwidth]{figures/CEP.eps}
  \caption{central processing of UHEP mode.}
  \label{fig:cep-processing}
\end{figure}

\subsection{ HECR  mode processing}

The trigger algorithm is a software that runs on CEP level and is similar to 
UHEP mode, the only difference is for HECR mode PPF inversion and peak detection 
has to run in parallel for all the incoming station beams as shown in 
Fig..~\ref{fig:online-processing-HECR-mode}, i.e., we are interested only in
beamlets and not in tied array mode. Whether or not all of these
incoming  station beams can be scanned for HECR radio pulses, will greatly depend
on the  amount of processing resources allocated to the main observing mode in
which to piggy-back.

\section{ Polyphase Filter Design}

The FFT is a very effecient method of transforming data between the time and 
frequency domains. When used to form a filterbank it constrains the possible 
frequency response of each channel because the impulse response of the channel 
must be no greater than the length of the FFT. With data processed in
non-overlapping  blocks, this constraint on filter length leads to significant
aliasing as the  separation between frequency channels is equal to the width of
the frequency  channels. For example, the -3dB width of a Hamming window is 1.3
times the  frequency channel separation. One way to solve this is to increase the
output  sampling rate by overlapping the section of data being processed. For the
Hamming  window the output data rate increases by a factor of 3.7 if aliasing is
to be kept  below -40dB. As there is considerable overlap between bins in the
frequency domain  the load is reduced by pruning the outputs of the FFT. 

Main advantage of the Polyphase filter bank is that the M-taps filter is
shared between the subbands, and the downsampling is integrated with the
filtering. However, a traditional filter will have the downsampling after the
filtering. Because of the oversampling, width of the generated subbands 
increases so that they slightly overlap and most of the aliasing
appear in this redundant overlapped area.

\subsection{Basic FIR digital filter structures}

A causal FIR filters of order $N $is characterized by a transfer
function\footnote{Frequency response of a filter.} $H(z)$,
\begin{equation}
  H(z)= \sum_{k=0}^{N}h[k]z^{-k},
  \label{eq:transfer-function}
\end{equation}
which is a polynomail in $z^{-1}$ of degree N. Comparing it with
eq.~\ref{eq:impulse-response-function}, it can be easily inferred $z^{-1}$ is one
sample delay. In time domain, the input output relationship can be written as,
\begin{equation}
  y[n]= \sum_{k=0}^{N} h[k]x[n-k]
\end{equation}
where $y[n]$ and $x[n]$ are input and output sequences respectively.

\subsection{Polyphase realization of FIR filters}

FIR filters can be realized based on the polyphase decomposition of its transfer
function which results in a parallel structure. For example, consider a causal
FIR transfer function $H(z)$ of length 9
\begin{equation}
  H(z) = h[0] + h[1] z^{-1} + h[2] z^{-2} + h[3] z^{-3} + h[4] z^{-4}
  + h[5] z^{-5} + h[6] z^{-6} + h[7] z^{-7} + h[8] z^{-8}
\end{equation}
however, in the time domain the impulse-response of the same filter will be,
\begin{eqnarray}
  y[n] & = & h[0]\,x[n] + h[1]\,x[n-1] + h[2]\,x[n-2] + h[3]\,x[n-3] + h[4]\,x[n-4]
  \nonumber \\
  & & + h[5]\,x[n-5] + h[6]\,x[n-6] + h[7]\,x[n-7] + h[8]\,x[n-8]
\end{eqnarray}
Above transfer function can be expressed as a sum of two terms, one term
containing even-indexed coefficients  and other containing the odd-indexed
coefficients,
\begin{eqnarray}
  H(z)&= &(h[0]+h[2]z^{-2}+h[4]z^{-4}+h[8]z^{-8}){}+
  \nonumber\\
  & & {}+(h[1]z^{-1}+h[3]z^{-3}+h[5]z^{-5}+h[7]z^{-7})
\end{eqnarray}
which can be rearranged in a different way
\begin{eqnarray}
  H(z)&= &(h[0]+h[2]z^{-2}+h[4]z^{-4}+h[8]z^{-8}){}+
  \nonumber\\
  & & {}+z^{-1}(h[1]+h[3]z^{-2}+h[5]z^{-4}+h[7]z^{-6})
\end{eqnarray}

And if we use,
\begin{eqnarray}\label{eq:equation-array}
  E_0(z)=h[0]+h[2]z^{-1}+h[4]z^{-2}+h[6]z^{-3}+h[8]z^{-4}      \\
  E_1(z)=h[1]+h[3]z^{-1}+h[5]z^{-2}+h[7]z^{-3}
\end{eqnarray}
and therefore, we can decompose it in to two branches, 
\begin{equation}
  H(z)=E_0({z^2})+z^{-1}E_1({z^2})
\end{equation}
By grouping the terms differently, we can even decompose H(z) in three branches
as,
\begin{equation}
  H(z)=E_0({z^3})+z^{-1}E_1({z^3})+z^{-2}E_2({z^3})
\end{equation}

Therefore, in general case, an L-branch polyphase decomposition of the transfer
function of order $N$ is of the form
\begin{equation}\label{eq:gen-ppf-decomposition}
  H(z)= \sum_{m=0}^{L-1}z^{-m}E_m(z^L)
\end{equation}
where 
\begin{equation}
  E_m(z) = \sum_{n=0}^{[\frac{(N+1)}{L}]}h[Ln+m]z^{-n},\qquad 0 \leq m \leq L-1 
\end{equation}
with $ h[n]=0$ for $n>N$.

Realization based on this kind of decomposition is called a polyphase
realization. The subfilters $E_m(z^{L})$ in the polyphase realization of an FIR
transfer function are also FIR filters. 


These subfilters are special type of lowpass filter with a transfer function
that, by design, has certain zero-valued coefficients.  Due to the presence of
these zero-valued coefficients, these filters  are computationally more efficient
than other lowpass filters of the  same order, for example in
eq.~\ref{eq:equation-array}, expression for $ E_0(z)$, we can decimate the
samples by a factor of 2, because all odd filter coefficients have zero value,
and in this way we are reducing the number of multiplications.

These subfilters are when used as an interpolator(synthesis) filters,  for
reconsturction of the original signal, i.e., inversion of polyphase  filters,
they reserve the nonzero samples of the up-sampler output at  the interpolator
output.

A linear phase FIR filters of order N is either characterized by a symmetric 
impulse response, i.e., $ h[n]=h[N-n]$ or by an antisymmetric impulse response,
the symmetry property of a linear-phase FIR filters is exploited  to reduce the
total number of multipliers into almost half of that in  the direct form
implementations of the transfer function. For example, for a length-8 Type 2 FIR
transfer function, the pertinent decomposition is given by

\begin{equation}
  H(z) = h[0](1+z^{-7})+h[1](z^{-1}+z^{-6})+h[2](z^{-2}+z^{-5})+h[3](z^{-3}+z^{-4})
\end{equation}
which requires 4 multipliers, compared to 8 multipliers in the direct form
realization of the original length-8 FIR filter. The impulse response of the FIR
filters implemented for LOFAR is a sinc funtion to inherit the linear-phase
property. The implemented impulse response ($h[k]$) with all $ (1024 \times 16)$
filter coefficients is shown in Fig.~\ref{fig:transfer-function-ppfcoeff}.

\begin{figure}[htbp]
 \centering
 \includegraphics[width=.8\textwidth]{figures/ppfcoeff.eps}
 \caption{Impulse response of implemented FIR filter.}
 \label{fig:transfer-function-ppfcoeff}
\end{figure}


\subsection{Analysis Filter Bank}

The digital filter bank is a set of digital bandpass filters with either a 
common input or a summed output, the structure shown in
Fig.~\ref{fig:analysis-filter-bank} is called an $M$-band analysis filter bank 
with the subfilters $ H_k(z)$ known as the analysis filters. It decomposes  the
input signal $x[n]$ into a set of $M$ subband signals $v_k[n]$ with each subband
signal occupying a portion of the original frequency band. In LOFAR 1024
band system, i.e., a polyphase filter with 1024 subfilters is implemented; the
negative part of spectrum however is omitted, and hence only 512 bands are 
available for further processing. 


\subsubsection{Design of uniform filter banks with equal passband widths}

Let $ H_0(z)$ represent a causal low-pass digital (FIR) filter with an impulse
response $ h_0[n]$:

\begin{equation}
 H_0(z)= \sum_{n=0}^{\infty}h_0[n]z^{-n}
\end{equation}
Let us now assume that $ H_0(z)$ has its passband edge $\omega_{P}$ and stopband
edge $\omega_S$  around $\frac{\pi}{M}$, where $M$ is some arbitrary integer, as
indicated in Fig.~\ref{fig:filter-bank}.  Now, consider the transfer function
$H_k(z)$, whose impulse response $h_k[n]$ is defined to be 

\begin{equation}\label{eq:impulse-response}
 h_k[n]= h_0[n]W_M^{-kn},\qquad 0 \leq k \leq M-1 ;
\end{equation}
where $ W_M = e^{\frac {-j2\pi}{M}}$, as defined in Eq.~\ref{eq:DFT-equation},
will be   
\begin{equation}
 H_k(z)= \sum_{n=0}^{\infty}h_k[n]z^{-n} = \sum_{n=0}^{\infty}h_0[n](zW_{M}^{k})^{-n},\qquad 0 \leq k \leq M-1
\end{equation}
i.e., 
\begin{equation}
  H_k(z) = H_0(zW_{M}^{k}),\qquad 0 \leq k \leq M-1 
\end{equation}
with a corresponding frequency response

\begin{equation}
H_k(e^{j\omega}) = H_0(zW_{M}^{k}),\qquad 0 \leq k \leq M-1.
\end{equation}
i.e., the frequency response of $ H_k(z)$ is obtained by shifting the response of
$ H_0(z)$ to the right,  by an amount $ \frac{2\pi k}{M}$. The responses of
$H_1(z), H_2(z), ......, H_{M-1}(z)$ are shown in  Fig.~\ref{fig:filter-bank} are
uniformly shifted versions of the response of the basic prototype filter $H_0(z)$.


\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{figures/fig8.eps}
  \caption{Analysis Filter Bank.}
  \label{fig:analysis-filter-bank}
\end{figure}


\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{figures/filter-banks.eps}
  \caption{The bank of M filters $H_k(z)$ with uniformly shifted frequency responses.}
  \label{fig:filter-bank}
\end{figure}


\subsubsection{Polyphase Implementation of uniform filter bank}

Fig.~\ref{fig:analysis-filter-bank} represent a uniform filter bank with the $M$
analysis filters $ H_k(z)$ related through Eq.~\ref{eq:transfer-function}. The
impulse response sequences $ h_k[n] $ of the analysis filters are accordingly
related as Eq.~\ref{eq:impulse-response}. Therefore, instead of realizing each
analysis filter as a separate filter, it is possible to develop a computationally
more efficient realization of the above uniform filter bank. Let the low-pass
prototype transfer function $H_0(z)$ be represented in its $M$-band polyphase
form:
\begin{equation}\label{eq:polyphase-form}
  H_0(z)=\sum_{l=0}^{M-1} z^{-l}E_l(z^M),
\end{equation}
where $E_l(z)$ is the \emph l th polyphase component of $H_0(z)$:
\begin{equation}
  E_l(z)=\sum_{n=0}^{\infty}e_l[n]z^{-n},\qquad 0\leq l\leq M-1.
\end{equation}
Substituting \emph z with $zW_M^k$ in Eq.~\ref{eq:polyphase-form}, we arrive at
the M-band polyphase decomposition of $H_k(z)$:
\begin{equation}\label{eq:polyphase-decomposition}
  H_k(z)=\sum_{l=0}^{M-1}z^{-l}W_M^{-kl}E_l(z^MW_M^{kM})=\sum_{l=0}^{M-1}z^{-l}W_M^{-kl}E_l(z^M),\quad k=0,1,.....,M-1,
\end{equation}
writting, $W_k^{kM}=1$. Above equation can be written in matrix form as
\begin{equation}
  H_k(z)=\left[\begin{array}{ccccc}
      1 & W_M^{-k} & W_M^{-2k} & \ldots & W_M^{-(M-1)k}
    \end{array}\right]
  \left[\begin{array}{c}
      E_0(z^M)\\
      z^{-1}E_1(z^M)\\
      z^{-2}E_2(z^M)\\
      \vdots \\
      z^{-(M-1)}E_{M-1}(z^M)
    \end{array}\right]
\end{equation}
for $k=0,1,....M-1$. All there $M$ equations can be combined into a matrix
equation as
\begin{equation}
  \left[\begin{array}{c}
      H_0(z)\\
      H_1(z)\\
      H_2(z)\\
      \vdots\\
      H_{M-1}(z)
    \end{array}\right]
  = \left[\begin{array}{ccccc}
      1 & 1 & 1 & \ldots & 1 \\
      1 & W_M^{-1} & W_M^{-2} & \ldots & W_M^{-(M-1)}\\
      1 & W_M^{-2} & W_M^{-4} & \ldots & W_M^{-2(M-1)}\\
      \vdots & \vdots & \vdots & \vdots & \vdots \\
      1 & W_M^{-(M-1)} & W_M^{-2(M-1)} & \ldots & W_M^{-(M-1)^2}
    \end{array}\right]
  \left[\begin{array}{c}
      E_0(z^M)\\
      z^{-1}E_1(z^M)\\
      z^{-2}E_2(z^M)\\
      \vdots\\
      z^{-(M-1)}E_{M-1}(z^M)
    \end{array}\right]
\end{equation}
which is equivalent to
\begin{equation}\label{eq:analysis-filter-bank}
  \left[\begin{array}{c}
      H_0(z)\\
      H_1(z)\\
      H_2(z)\\
      \vdots\\
      H_{M-1}(z)
    \end{array}\right]
  =M \mathbf D^{-1}
  \left[\begin{array}{c}
      E_0(z^M)\\
      z^{-1}E_1(z^M)\\
      z^{-2}E_2(z^M)\\
      \vdots\\
      z^{-(M-1)}E_{M-1}(z^M)
    \end{array}\right]
\end{equation}
where $\mathbf D$ denotes the DFT matrix given by Eq.~\ref{eq:DFT-matrix} and 
$\mathbf D^{-1}=\frac{1}{M}D_M^*$ (* signifies the complex conjugates).

\begin{figure}[htbp]
 \centering
 \includegraphics[width=\textwidth]{figures/analysis_filter_bank.eps}
 \caption{Analysis filter bank.}
 \label{fig:analysis-filter-banks}
\end{figure}

An efficient implementation of the M-band analysis filter bank based on 
Eq.~\ref{eq:analysis-filter-bank}, is thus shown in 
Fig.~\ref{fig:analysis-filter-banks}, where the prototype lowpass filter $H_0(z)$
has been implemented in a polyphase form. Therefore,  computational complexity of
Fig.~\ref{fig:analysis-filter-bank-implemented} is much smaller than that of a
direct implementation, as in Fig~\ref{fig:analysis-filter-bank}. For example, an
$M$-band uniform DFT analysis filter bank  based on an $N$-tap prototype lowpass
FIR filter requires, a total of $\frac{M}{2}log_2M+N $ multipliers, whereas a
direct implementation requires $NM$ multiplications.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\textwidth]{figures/analysis-filter-bank.eps}
  \caption{Analysis Filter Bank Implemented.}
  \label{fig:analysis-filter-bank-implemented}
\end{figure}

\subsubsection{Implementation of Polyphase Filter Bank in LOFAR}

As shown in Fig.~\ref{fig:analysis-filter-bank-implemented}, input data samples
are first decimated by a factor of (M=)1024 and then subfilters are implemented
on  these decimated data samples. It is sufficient to compute (subfilter) output
only for every $1025^{th}$ sample and skip the computations of the in-between 
1025 samples. This leads in saving of computational complexity by a factor of 1024.

Therefore, first sample will go in first band subfilter, which is FIR filter
of 16 taps, with polyphase decomposition function $E_{0}[z^{1024}]$, which 
is (according to eq.~\ref{eq:equation-array})
\begin{eqnarray}
  E_{0}[z^{1024}] & = & h[0]\,z^0 + h[1024]\,z^{-1024} + h[2048]\,z^{-2048}
  + h[3072]\,z^{-3072} + h[4096]\,z^{-4096}  \nonumber \\
  & & + h[5120]\,z^{-5120} + h[6144]\,z^{-6144} + h[7168]\,z^{-7168}
  + h[8192]\,z^{-8192}  \nonumber \\
  & & + h[9216]\,z^{-9216} + h[10240]\,z^{-10240}+ h[11264]\,z^{-11264}
  + h[12288]\,z^{-12288}  \nonumber  \\
  & & + h[13312]\,z^{-13312} + h[14336]\,z^{-14336} + h[15360]\,z^{-15360}
\end{eqnarray}

The above equation is self explainatary, i.e., every $1025^{th}$ sample will be
multiplied with the filter coefficients and since rest of the filter coefficients
have zero value, we decimate the input datasamples by a factor of 1024 to avoid
extra multipliers, since any samplevalue multiplied by zero valued coefficient
will give zero value. Thus, first sample will be multiplied by $h[0]$ filter
coefficient in the first subfilter and second sample which will be multiplied by
$h[1]$ which consitutes second band subfilter with polyphase decomposition
function $E_{1}[z^{1024}]$ which is

\begin{eqnarray}
  E_{1}[z^{1024}] & = & h[1]\,z^{-1} + h[1025]\,z^{-1025} + h[2049]\,z^{-2049}
  + h[3073]\,z^{-3073} + h[4097]\,z^{-4097}  \nonumber \\
  & & + h[5121]\,z^{-5121} + h[6145]\,z^{-6145} + h[7169]\,z^{-7169}
  + h[8193]\,z^{-8193}  \nonumber  \\
  & & + h[9217]\,z^{-9217} + h[10241]\,z^{-10241} + h[11265]\,z^{-11265}
  + h[12289]\,z^{-12289}  \nonumber  \\
  & & + h[13313]\,z^{-13313} + h[14337]\,z^{-14337} + h[15367]\,z^{-15367}
\end{eqnarray}
similarly, third sample will be multiplied by $h[3]$, which constitutes third
subband filter $E_{2}[z^{1024}]$ and so on so forth.

After, multiplication of $1024^{th}$ sample with h[1023]in the $1024^{th}$
subfilter, $1025^{th}$ sample will go in first subband filter again and will be
multiplied with $h[0]$ and the first sample which was there will shift towards
right and will be multiplied by $h[1024]$. Further, $1026^{th}$ sample will go in
the second subband filter and will be multiplied $h[1]$ and will shift the second
sample towards right in the second subfilter and will now be multiplied by
$h[1025]$. In this way, with every clockticks samples will enter in subsequent
subfilters and every new sample in a subband filter will cause right shifting of
sample residing over there.

In this way, with each clockticks, a 1024 point IDFT bank, which is represented
by taking complex conjugate of each element of eq.~\ref{eq:DFT-matrix}, will be
applied on output of all (1024) subband filters. This will result finally in 1024
output from analysis filter bank, we will omit negative spectrum thus will keep
only first 512 subbands. These frequency subbands will cover part of the original
frequency range.

Therefore, in each subbands, sample rate is decimated by a factor of 1024, i.e.,
we will get data samples in particular subband with clock rate [1/(200MHz)]/1024,
which was initially 1/(200MHz). However, at any clocktick if we observe all the
subbands we will have all 1024 samples in 1024 subbands.

\subsection{Synthesis filter bank}

A discrete-time signal $x[n]$ is first split into a number of subband signals
$v_k[n]$ by means of an analysis filter bank; the subband signals are then
processed and finally combined by a synthesis filter resulting in one output
signal $y[n]$ as shown in Fig.~\ref{fig:synthesis-filter-bank}. 

If the subband signals are bandlimited to frequency ranges much smaller than 
that of the original input signal, they can be down-sampled before processing, 
which is the case with LOFAR. Because of the lower sampling rate, the processing 
of the down-sampled signals can be carried out more efficiently. 
After processing, these signals are up-sampled before being combined 
by the synthesis bank into a higher-rate signal. 

After processing, these signals of each subbands have to be up sampled to
retrieve the original signal before comibing by the synthesis bank into the higher
rate signal. Since the up-sampling causes periodic repetition of the basic 
spectrum, the unwanted images in the spectra of the up-sampled signal can be 
removed by using a lowpass filter, which is termed as interpolation filter. 
For the case of interpolation filter, a similar arguement holds, if $H(z)$ is an FIR
filter, then the computational savings is by a factor of 1024 (since output of
analysis filter has 1024 zeros between its two consecutive nonzero samples,
because of upsampling). 

If the down-sampling and up-sampling factors are equal to or greater than the
number of bands of the filter bank, then the output $y[n]$ can be made to
retain some or all of the characteristics of the input $x[n]$ by properly
choosing the filters in the structure of synthesis filter bank. 


\begin{figure}[htbp]
 \centering
 \includegraphics[width=.8\textwidth]{figures/synthesis_filter_bank.eps}
 \caption{Synthesis filter bank.}
 \label{fig:synthesis-filter-bank}
\end{figure}

Rewritting Eq.~\ref{eq:polyphase-decomposition} of polyphase reprensentation
of the $k^{\rm th}$  analysis filter $ H_{k}(z)$ :
\begin{equation}
H_k(z)=\sum_{l=0}^{L-1}z^{-l}E_{kl}(z^{L}), \qquad 0 \leq k \leq L-1.
\end{equation}

A matrix representation of the above set of equations is given by
\begin{equation}
\mathbf{h}(z) = \mathbf{E}(z^L) \mathbf{e}(z),
\end{equation}
where
\begin{equation}
 \mathbf{h}(z)=[H_0(z) \quad H_1(z) \quad.......... \quad H_{L-1}(z)]^T ,
\end{equation}

\begin{equation}
 \mathbf{e}(z) = [1 \quad z^{-1} \quad ....... \quad z^{-(L-1)}]^T,
\end{equation}
and

\begin{equation}
\mathbf{E}(z)=
\left[\begin{array}{cccc}
E_{00}(z) & E_{01}(z) & \ldots & E_{0,L-1}(z)\\
E_{10}(z) & E_{11}(z) & \ldots & E_{1,L-1}(z)\\
\vdots & \vdots & \ddots & \vdots\\
E_{L-1,0}(z) & E_{L-1,1}(z) & \ldots & E_{L-1,L-1}(z)
\end{array} \right]
\end{equation}

The matrix $\mathbf{E}(z)$ defined above is called the \emph {Type I Polyphase
Componenet matrix}.  Likewise, we can represent the L synthesis filters in a
\emph {Type II Polyphase form}:
\begin{equation}
G_k(z)=\sum_{l=0}^{L-1}z^{-(L-1-l)}R_{lk}(z^L), \qquad 0\leq k\leq L-1
\end{equation}

In matrix form, the above set of $L$ equations can be rewritten as
\begin{equation}
\mathbf{g}^T(z)=z^{-(L-1)} \mathbf{\tilde{e}}(z)\mathbf{R}(z^L),
\end{equation}
where
\begin{equation}
\mathbf{g}(z)=[G_0(z) \quad  G_1(z) \quad  \ldots \quad  G^{L-1}(z)]^T ,
\end{equation}
\begin{equation}
\mathbf{\tilde{e}}(z)=[1 \quad z \quad ..... \quad z^{L-1}] = \mathbf{e}^T(z^{-1}),
\end{equation}
and
\begin{equation}
\mathbf{R}(z)=
\left[\begin{array}{cccc}
R_{00}(z) & R_{01}(z) & \ldots & R_{0,L-1}(z)\\
R_{01}(z) & R_{11}(z) & \ldots & R_{1,L-1}(z)\\
\vdots & \vdots & \ddots & \vdots\\
R_{L-1,0}(z) & R_{L-1,1}(z) & \ldots & R_{L-1,L-1}(z)
\end{array} \right]
\end{equation}

The matrix $\mathbf{R}(z)$ defined above is called the \emph {Type II polyphase
component matrix}, which represents synthesis filter bank.

\subsubsection {Condition for Perfect Reconstruction}

If the polyphase components matrices satisfy the relation
$\mathbf{R}(z)\mathbf{E}(z)= c \mathbf{I} $ where $\mathbf{I}$ is $ L \times L$
idenity matrix and $ c$ is a constant.

Now, for a given L-channel analysis filter bank, the polyphase matrix
$\mathbf{E}(z)$ is known, and therefore, for perfect construction of signal, a
L-channel Quadrature-mirror filter (QMF) bank can be simply designed by
constructing  a synthesis filter bank with a polyphase matrix
$\mathbf{R}(z)=[\mathbf{E}(z)]^{-1}$. In general, it is not easy to compute  the
inverse of a rational $ L \times L$ matrix. An alternative elegant approach is to
design the analysis filter bank with an invertible polyphase matrix. For example,
$\mathbf{E}(z)$ can be chosen to be a paraunitary matrix satisfying the condition

\begin{equation}
\tilde{\mathbf{E}}(z) \mathbf{E}(z) = c\mathbf{I}, 
\end{equation}
for all $z$, where $\tilde{\mathbf{E}}(z)$ is the paraconjugate of
$\mathbf{E}(z)$ given by the transpose of $\mathbf{E}(z^{-1})$, with each
coefficient replaced by its conjugate and choosing $ \mathbf{R}(z) =
\tilde{\mathbf{E}}(z)$.  Fig.~\ref{fig:synthesis-filter-bank-implemented} shows
the way synthesis filter bank is implemented, derived from analysis filter bank
implemented at LOFAR stations.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\textwidth]{figures/synthesis-filter-bank.eps}
  \caption{Synthesis Filter Bank.}
  \label{fig:synthesis-filter-bank-implemented}
\end{figure}



\begin{thebibliography}{100}
\frenchspacing
\bibitem{ppfinversion} K. Singh, Inversion of polyphase Filter Bank at CEP, LOFAR-004.
\bibitem{Crochiere} R. E. Crochiere, Lawrence R. Rabiner,\emph{Multirate  Digital Signal Processing}, 
         Prentice-Hall Inc., 1983. 
\bibitem{memo??}Jan Stemerdink, \emph{Cascaded polyphase filter banks},
  \textsc{lofar-astron-mem-??}
\bibitem{Mitra} S. K. Mitra, \emph{Digital Signal Processing}, McGraw-Hill,
  2001.
\bibitem{Projectplan} Project Plan LOFAR/CR.
\end{thebibliography}

\end{document}
