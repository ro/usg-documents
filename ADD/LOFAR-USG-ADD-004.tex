\documentclass[a4paper,10pt]{scrartcl}
\usepackage{amsmath,amssymb,usg,texdraw}

%% Typesetting macros

%\def\eas   {\textsc{eas}}
%\def\fpga  {\textsc{fpga}}
%\def\lcu   {\textsc{lcu}}
%\def\mtc   {\textsc{mtc}}
%\def\rfi   {\textsc{rfi}}
%\def\tbb   {\textsc{tbb}}
%\def\tc    {\textsc{tc}}

%% Begin of document ------------------------------------------------------------

\begin{document}

\title{Inversion of Polyphase Filter Bank at CEP} 
\author{K.\,Singh}
\date{\small Version of \today}

\maketitle

\tableofcontents

\section{Purpose of this Document}

The FFT is a very effecient method of transforming data between the time and 
frequency domains (as discussed in appendix\,\ref{DFT}). When used to form a 
filterbank it constrains the possible frequency response of each channel 
because the impulse response of the channel must be no greater than the 
length of the FFT. With data processed in non-overlapping  blocks, this 
constraint on filter length leads to significant aliasing as the  separation 
between frequency channels is equal to the width of the frequency  channels. 
For example, the -3 dB width of a Hamming window is 1.3 times the  frequency 
channel separation. 

One way to solve this problem is to increase the output  sampling rate by 
overlapping the section of data being processed. For the Hamming  window 
the output data rate increases by a factor of 3.7 if aliasing is to be kept 
below -40 dB. As there is considerable overlap between bins in the frequency 
domain the load is reduced by pruning the outputs of the FFT. 

Main advantage of using Polyphase filter bank in comparison to traditional
filter banks is the M-taps filter is shared between the subbands, and the 
downsampling is integrated with the filtering (frequency response of linear
filters and basic digital filter structure is discussed in appendix\, 
\ref{freq_LTI} and \ref{FIR_digital}). However, a traditional filter 
will have the downsampling after the filtering. Other thing is because of the 
oversampling, width of the generated subbands increases so that they slightly 
overlap and most of the aliasing appear in this redundant overlapped area
(\cite{Crochiere}).

Because of the implementation of Polyphase Filter bank at stations of LOFAR,
datastream is downsampled by a factor of 1024 and after processing data will be in
frequency domain (\cite{memo}). To search for suitable pulses from Air Showers
in real time, we need to tranform data in time domain again and upsample data
to reconstruct the original signal by interpolation (\cite{Mitra}), which can
be achieved by the inversion of polyphase filter bank (\cite{Projectplan}).

\section{Polyphase Realization of Filter Sturcture}

\begin{figure}[htbp]
 \centering
 \includegraphics[width=.8\textwidth]{figures/ppfcoeff.eps}
 \caption{Impulse response of implemented FIR filter.}
 \label{fig:transfer-function-ppfcoeff}
\end{figure}

Finite Impulse Response (FIR) filters (see appendix\, \ref{FIR_digital} for
details) can be realized based on the polyphase decomposition of its transfer 
function which results in a parallel structure of subfilters, as
discussed in appendix\, \ref{polyphase_FIR}. These subfilters 
are special type of lowpass filter with a transfer function that, by design, 
has certain zero-valued coefficients. Due to the presence of these zero-valued 
coefficients, these filters  are computationally more efficient than other 
lowpass filters of the  same order (\cite{Mitra}).

The same subfilters are when used as an interpolator (synthesis) filters,  for
reconsturction of the original signal, i.e., inversion of polyphase filters,
they reserve the nonzero samples of the up-sampler output at  the interpolator
output.

A linear phase FIR filters of order N is either characterized by a symmetric 
impulse response, i.e., $ h[n]=h[N-n]$ or by an antisymmetric impulse response,
the symmetry property of a linear-phase FIR filters is exploited  to reduce the
total number of multipliers into almost half of that in  the direct form
implementations of the transfer function.

The impulse response of the FIR filters implemented for LOFAR is a sinc
funtion to inherit the linear-phase and symmetric property. The implemented 
impulse response ($h[k]$) with all $(1024 \times 16 = 16384)$
filter coefficients is shown in Fig.~\ref{fig:transfer-function-ppfcoeff},
will result in 1024 subfilters of 16 taps each.

\begin{figure}[htbp]
 \centering
 \includegraphics[width=\textwidth]{figures/analysis_filter_bank.eps}
 \caption{Analysis filter bank.}
 \label{fig:analysis-filter-banks}
\end{figure}

\section{Analysis Filter Bank}


The polyphase filter bank is a set of digital bandpass filters with 
either a common input or a summed output, the structure shown in
Fig.~\ref{fig:analysis-filter-banks} is called an $M$-band analysis filter bank 
with the subfilters $ H_k(z)$ known as the analysis filters, which is a
combination of parallel structure of M subfilters and an IDFT stage
(\cite{Mitra}). It decomposes the input signal $x[n]$ into a set of $M$ 
subband signals $v_k[n]$ with each subband signal occupying a portion of 
the original frequency band (see appendix\, \ref{passband_widths}). In LOFAR, 
i.e., a polyphase filter with 1024 subfilters is implemented; thus it is a
1024 subband system, the negative part of spectrum however is omitted, and 
hence only 512 subbands are available for further processing. 

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{figures/analysis-filter-bank.eps}
  \caption{Analysis Filter Bank Implemented.}
  \label{fig:analysis-filter-bank-implemented}
\end{figure}


\subsection{Implementation of Polyphase Filter Bank in LOFAR}

As shown in Fig.~\ref{fig:analysis-filter-bank-implemented} K=1024 and L=16, 
input data samples in time domain, are first decimated by a factor of (M=)1024 
and then (1024 ) 
subfilters are implemented on  these decimated data samples. It is sufficient 
to compute (subfilter) output only for every $1025^{th}$ sample and skip the 
computations of the in-between 1024 samples. This leads in saving of computational
complexity  by a factor of 1024 (see appendix\, \ref{polyphase_implementation}
for details).

Therefore, first sample will go in first band subfilter, which is Finite
Impulse Response (FIR) filter of 16 taps, with polyphase decomposition 
function $E_{0}[z^{1024}]$, which is (according to
eq.~\ref{eq:equation-array} in appendix\, \ref{polyphase_FIR})

\begin{eqnarray}
  E_{0}[z^{1024}] & = & h[0]\,z^0 + h[1024]\,z^{-1024} + h[2048]\,z^{-2048}
  + h[3072]\,z^{-3072} + h[4096]\,z^{-4096}  \nonumber \\
  & & + h[5120]\,z^{-5120} + h[6144]\,z^{-6144} + h[7168]\,z^{-7168}
  + h[8192]\,z^{-8192}  \nonumber \\
  & & + h[9216]\,z^{-9216} + h[10240]\,z^{-10240}+ h[11264]\,z^{-11264}
  + h[12288]\,z^{-12288}  \nonumber  \\
  & & + h[13312]\,z^{-13312} + h[14336]\,z^{-14336} + h[15360]\,z^{-15360}
\end{eqnarray}

The above equation is self explanatory, i.e., every $1024^{th}$ sample will be
multiplied with the filter coefficients in a particular subfilter and since
rest of the filter coefficients have zero value, we decimate the input
datasamples by a factor of 1024 to avoid extra multipliers, since any sample 
value multiplied by zero valued coefficient will give zero value. Thus, first 
sample will be multiplied by $h[0]$ filter coefficient in the first subfilter 
with frequency response function $E_{0}[z^{1024}]$ and second sample which
will  be multiplied by $h[1]$ which consitutes second band subfilter with 
polyphase decomposition function $E_{1}[z^{1024}]$ which is

\begin{eqnarray}
  E_{1}[z^{1024}] & = & h[1]\,z^{-1} + h[1025]\,z^{-1025} + h[2049]\,z^{-2049}
  + h[3073]\,z^{-3073} + h[4097]\,z^{-4097}  \nonumber \\
  & & + h[5121]\,z^{-5121} + h[6145]\,z^{-6145} + h[7169]\,z^{-7169}
  + h[8193]\,z^{-8193}  \nonumber  \\
  & & + h[9217]\,z^{-9217} + h[10241]\,z^{-10241} + h[11265]\,z^{-11265}
  + h[12289]\,z^{-12289}  \nonumber  \\
  & & + h[13313]\,z^{-13313} + h[14337]\,z^{-14337} + h[15367]\,z^{-15367}
\end{eqnarray}
similarly, third sample will be multiplied by $h[2]$, which constitutes third
subband filter $E_{2}[z^{1024}]$ and so on so forth.

After, multiplication of $1024^{th}$ sample with h[1023] in the $1024^{th}$
subfilter, $1025^{th}$ sample will go in first subband filter again and will be
multiplied with $h[0]$ and the first sample which was there will shift towards
right and will be multiplied by $h[1024]$. Further, $1026^{th}$ sample will go in
the second subband filter and will be multiplied $h[1]$ and will shift the second
sample towards right in the second subfilter and will now be multiplied by
$h[1025]$. In this way, with every clockticks samples will enter in subsequent
subfilters and every new sample in a subband filter will cause right shifting of
sample residing over there.

In this way, with each clockticks, a 1024 point IDFT bank as shown in 
Fig.~\ref{fig:analysis-filter-bank-implemented} , which is represented
by taking complex conjugate of each element of eq.~\ref{eq:DFT-matrix} of
appendix\, \ref{DFT}, which are nothing but twiddle factors, will be applied 
on output of all (1024) subband filters. This will result finally in 1024
subband output from analysis filter bank, we will omit negative spectrum 
thus will keep only first 512 subbands. These frequency subbands will cover 
part of the original frequency range (appendix D.1).

Therefore, in each subbands, sample rate is decimated by a factor of 1024, i.e.,
we will get data samples in particular subband with clock rate [1/(200MHz)]/1024,
which was initially 1/(200MHz). However, at any clocktick if we observe all the
subbands we will have all 1024 samples in 1024 subbands.


\section{Synthesis filter bank}

\begin{figure}[htbp]
 \centering
 \includegraphics[width=.8\textwidth]{figures/synthesis_filter_bank.eps}
 \caption{Synthesis filter bank.}
 \label{fig:synthesis-filter-bank}
\end{figure}

A discrete-time signal $x[n]$ is first split into a number of subband signals
$v_k[n]$ by means of an analysis filter bank 
(appendix\,\ref{polyphase_implementation}); the subband signals are then 
processed and finally combined by a synthesis filter resulting in one output 
signal $y[n]$ as shown in Fig.~\ref{fig:synthesis-filter-bank}. 

If the subband signals are bandlimited to frequency ranges much smaller than 
that of the original input signal, they can be down-sampled before processing, 
which is the case with LOFAR. Because of the lower sampling rate, the processing 
of the down-sampled signals can be carried out more efficiently. 
Therefore after processing, these signals has to be up-sampled before being 
combined by the synthesis bank into a higher-rate signal to reconstruct the 
original time domain signal. 

Since the up-sampling causes periodic repetition of the basic 
spectrum, the unwanted images in the spectra of the up-sampled signal can be 
removed by using a lowpass filter, which is termed as interpolation filter. 
For the case of interpolation filter, a similar arguement holds, if $H(z)$ is 
an FIR filter, then the computational savings is by a factor of 1024 (since
output of analysis filter has 1024 zeros between its two consecutive nonzero 
samples, because of upsampling). 

If the down-sampling and up-sampling factors are equal to or greater than the
number of bands of the filter bank, then the output $y[n]$ can be made to
retain some or all of the characteristics of the input $x[n]$ by properly
choosing the filters in the structure of synthesis filter bank (\cite{Mitra}). 

Therefore, synthesis filter bank has again two stages, DFT bank integrated with
the inversion of FIR filter effect.


\subsection {Time Domain Impulse Response Inverter}

To derive the time domain inversion of an impulse response (transfer
function), which is nothing but Toeplitz system of equations of the form 
$ R*h_{inv}= q$ where R is the symmetric Toeplitz matrix (\cite{moon}). For 
example, consider filtering a causal signal x[t] with a filter [h] with 
coefficients [1, 2, 3, -1, -1], the filtering relationaship can be written as

\begin{equation}
  \left[\begin{array}{c}
      y(0)\\
      y(1)\\
      y(2)\\
      \vdots\\
   \end{array}\right]
  = \left[\begin{array}{cccccc}
      1 & \ldots & \ldots & \ldots & \ldots & \ldots \\
      2 & 1 & \ldots & \ldots & \ldots & \ldots \\
      3 & 2 & 1 & \ldots & \ldots & \ldots \\
      -2 & 3 & 2 & 1 & \ldots & \ldots \\
      -1 & -2 & 3 & 2 & 1 & \ldots \\
      \ldots & -1 & -2 & 3 & 2 & 1
    \end{array}\right]
  \left[\begin{array}{c}
      x[0]\\
      x[1]\\
      x[2]\\
      \vdots\\
    \end{array}\right]
\end{equation}

Observe that the elements on the diagonals of the matrix are all the same, the
elements of h shifted down and across. Finding x[t] given y[t] would require
solving a set of linear equations involving this matrix. Let us define
counteridentity matrix here $ J_{k} $ where k defines its dimension 

\begin{equation} 
 \mathbf{J}_{k}=
  \left[\begin{array}{ccccc}
      0 & 0 & \ldots & 0 & 1 \\
      0 & 0 & \ldots & 1 & 0 \\
      \vdots & \vdots & \vdots & \vdots & \vdots \\
      0 & 1 & \ldots & 0 & 0 \\
      1 & 0 & \ldots & 0 & 0
    \end{array}\right]
\end{equation}
 
The equations of Toeplitz system of equations 
$ \mathbf{R}_{k}\mathbf{y}_{k}=\mathbf{x}_{k} $ can be
solved by Levinson Durbin algorithm (\cite{moon}). Assuming that 
$ \mathbf{y}_{k}$ are known for step $k$, the solution to the $(k+1)$st 
step requires solving

\begin{equation}
  \left[\begin{array}{cc}
      \mathbf{R}_{k} & \mathbf{J}_{k}\mathbf{r}_{k} \\
      \mathbf{r}_{k}^{T}\mathbf{J}_{k} & r_{0} 
     \end{array}\right]
  \left[\begin{array}{c}
     \mathbf{v}_{k} \\
     \mu_{k} 
   \end{array}\right]
  = \left[\begin{array}{c}
      \mathbf{x}_{k} \\
      x_{k+1}  
    \end{array}\right]
 \end{equation}

where $ \mathbf{R}_{k}$ denote the $k\times k$ matrix

\begin{equation} 
 \mathbf{R}_{k}=
  \left[\begin{array}{ccccc}
      r_{0} & r_{1} & r_{2} & \ldots & r_{k-1} \\
      r_{1} & r_{0} & r_{1} & \ldots & r_{k-2} \\
      r_{2} & r_{1} & r_{0} & \ldots & r_{k-3} \\
      \vdots & \vdots & \vdots & \vdots & \vdots \\
      r_{k-1} & r_{k-2} & r_{k-3} & \ldots & r_{0} \\
   \end{array}\right]
\end{equation}

and let 
\begin{equation} 
 \mathbf{r}_{k}=
  \left[\begin{array}{c}
      r_{1} \\
      r_{2} \\
      \vdots \\
      r_{k} 
   \end{array}\right]
\end{equation}
 
however,
 \begin{equation} 
 \mathbf{y}_{k+1}=
  \left[\begin{array}{c}
       \mathbf{v}_{k} \\
      \mu_{k}
    \end{array}\right]
\end{equation}

Using the solutions from time $ k$,
$\mathbf{v}_{k} = \mathbf{y}_{k}-\mu\mathbf{J}_{k}\mathbf{x}_{k}$, while, $ \mu_{k}=\frac{x_{k+1}-\mathbf{r}_{k}^{T}\mathbf{J}_{k}\mathbf{y}_{k}}{r_{0}-\mathbf{r}_{k}^{T}\mathbf{y}_{k}} $

We can calculate the time domain inversion of an impulse response (transfer
function) of all 1024 subfilters for LOFAR system, i.e., inversion of 16 taps
of 1024 FIR subfilters.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\textwidth]{figures/synthesis-filter-bank.eps}
  \caption{Synthesis Filter Bank.}
  \label{fig:synthesis-filter-bank-implemented}
\end{figure}


\subsection{Implementation of Inversion of Polyphase Filter Bank}

As shown in Fig.~\ref{fig:synthesis-filter-bank-implemented}, inversion of
polyphase filter banks has two stages, first stage is to implement
DFT matrix eq.~\ref{eq:DFT-matrix} (appendix\, \ref{DFT} ) on all the subbands 
available.
Before implementing this DFT matrix we can regenerate negative spectrum, i.e.,
all 1024 subbands from given 512 subbands, by substituting the complex conjugate 
values of first 512 subbands in rest of the 512 subbands.

Further, to enhance the samplerate each branch of output of DFT matrix needs
to be zero padded. First branch will go in first subfilter
which has 16 taps $I_{0}, I_{k}, I_{2k}, I_{3k}, ....., I_{16k}$  calculated
with the inversion of impulse response of first subfilter $E_{0}(z^{1024})$ of 
taps $h_{0}, h_{k}, h_{2k}, h_{3k}, ....., h_{16k}$ following Levinson-Durbin 
algorithm, similarly, second branch will go in second branch subfilter which
is inverted time domain response of $ E_{1}(z^{1024})$, and so on so forth.

Inverted taps of FIR subfilters will behave in the same way as for forward FIR
filters, i.e, with every clock ticks samples will shift towards right. At any
instant, 16 simultaneous samples of each branch will be processed and output
of first branch will go first in the output $ Y(n)$ 
(Fig.~\ref{fig:synthesis-filter-bank-implemented})
which will be followed by the output of higher branches.

\appendix

\section{Discrete Fourier Transform}
\label{DFT}

For a finite length sequence $ x[n], \quad  0\leq n \leq N-1$, and its DTFT $
X(e^{j\omega})$ is obtained by uniformly sampling $ X(e^{j\omega})$ on the
$\omega$-axis between  $ 0\leq \omega \leq 2 \pi$ at $\omega_k = \frac{2 \pi
k}{N},\qquad  0\leq k \leq N-1$, i.e.,

\begin{equation}\label{eq:DFT-equation}
 X[k]= X(e^{j\omega})\mid _{\omega = \frac{2 \pi k}{N}}= \sum_{n=0}^{N-1}x[n](e^\frac{-j2 \pi k n}{N}),\qquad
  0\leq k \leq N-1 
\end{equation}
if we use the notation $ W_N = e^\frac{-j2 \pi}{N}$, then,
\begin{equation}
 X[k]=\sum_{n=0}^{N-1} x[n]W_{N}^{kn},\qquad  0\leq k \leq N-1 
\end{equation}

The inverse discrete fourier transform (IDFT) is given by 
\begin{equation}
 x[n]=\frac{1}{N}\sum_{k=0}^{N-1} X[k]W_{N}^{-kn},\qquad 0\leq n \leq N-1 
\end{equation}
therefore, the DFT samples can be expressed in matrix form as $ X = D_N[x] $ 
where $X$ is the vector composed of the $N$  DFT samples, $x$ is the vector of
$N$  input samples, and $D_N$ is the $ N \times N$ DFT matrix given by

\begin{equation} \label{eq:DFT-matrix}
  \mathbf{D_N}=
  \left[\begin{array}{cccccc}
      1 & 1 & 1 & \ldots & \ldots & 1 \\
      1 & W_N^1 & W_N^2 & \ldots & \ldots & W_N^{N-1} \\
      1 & W_N^2 & W_N^4 & \ldots & \ldots & W_N^{2(N-1)} \\
      \vdots & \vdots & \vdots & \vdots & \vdots & \vdots \\
      \vdots & \vdots & \vdots & \vdots & \vdots & \vdots \\
      1 & W_N^{N-1} & W_N^{2(N-1)} & \ldots & \ldots & W_N^{(N-1) \times (N-1)}
    \end{array}\right]
\end{equation}

Likewise, the IDFT relations can be expressed in matrix form as $ x = D_n^{-1}X
$,  where $D_N^{-1}$ is the $N \times N$ matrix given by $D_n^{-1} =
\frac{1}{N}D_N^{\ast }$ ($\ast $ signifies the complex conjugate).


\section {Frequency response of LTI system}
\label{freq_LTI}

Input-output relationship of an LTI (linear, time invariant) system, with an 
impulse response h[n] is given by the convolution sum and is of the form 
\begin{equation}\label{eq:impulse-response-function}
  y[n] = \sum_{k=- \infty}^{\infty} h[k] x[n-k],
\end{equation}
where $y[n]$ and $x[n]$ are respectively the output and input sequences. Now if
the input $x[n]$ is a complex exponential sequence of the form $ x[n]= e^{j
\omega n}, - \infty < n < \infty $. Then the ouput $y[n]$ can be expressed as,

\begin{equation}
  y[n]=\sum_{k=-\infty}^{\infty} h[k] e^{j\omega [n-k]} = 
  \left( \sum_{k=-\infty}^{\infty}h[k]e^{-j\omega k} \right) e^{j\omega n}
  = H(e^{j\omega})e^{j\omega n}
\end{equation}
where, $ H(e^{j\omega})= \sum_{k=-\infty}^{\infty}h[k]e^{-j\omega k}$ is the
freqency response of the  LTI discrete-time system, and it provides a
frequency-domain description of the system. $H(e^{j\omega})$ is precisely the
discrete-time Fourier transform (DTFT) of the impulse response $h[k]$ of the
system.


\section{Basic FIR digital filter structures}
\label{FIR_digital}

A causal Finite Impulse Response (FIR) filters of order $N $is characterized
by a transfer function\footnote{Frequency response of a filter.} $H(z)$,
\begin{equation}
  H(z)= \sum_{k=0}^{N}h[k]z^{-k},
  \label{eq:transfer-function}
\end{equation}
which is a polynomail in $z^{-1}$ of degree N. They do not have feedback
effect of previous output samples. Comparing it with
eq.~\ref{eq:impulse-response-function},
it can be easily inferred $z^{-1}$ is one
sample delay. In time domain, the input output relationship can be written as,
\begin{equation}
  y[n]= \sum_{k=0}^{N} h[k]x[n-k]
\end{equation}
where $y[n]$ and $x[n]$ are input and output sequences respectively.

\section{Polyphase realization of FIR filters}
\label{polyphase_FIR}

Finite Impulse Response (FIR) filters can be realized based on the polyphase
decomposition of its transfer function which results in a parallel structure. 
For example, consider a causal FIR transfer function $H(z)$ of length 9
\begin{equation}
  H(z) = h[0] + h[1] z^{-1} + h[2] z^{-2} + h[3] z^{-3} + h[4] z^{-4}
  + h[5] z^{-5} + h[6] z^{-6} + h[7] z^{-7} + h[8] z^{-8}
\end{equation}
however, in the time domain the impulse-response of the same filter will be,
\begin{eqnarray}
  y[n] & = & h[0]\,x[n] + h[1]\,x[n-1] + h[2]\,x[n-2] + h[3]\,x[n-3] + h[4]\,x[n-4]
  \nonumber \\
  & & + h[5]\,x[n-5] + h[6]\,x[n-6] + h[7]\,x[n-7] + h[8]\,x[n-8]
\end{eqnarray}
Above transfer function can be expressed as a sum of two terms, one term
containing even-indexed coefficients  and other containing the odd-indexed
coefficients,
\begin{eqnarray}
  H(z)&= &(h[0]+h[2]z^{-2}+h[4]z^{-4}+h[8]z^{-8}){}+
  \nonumber\\
  & & {}+(h[1]z^{-1}+h[3]z^{-3}+h[5]z^{-5}+h[7]z^{-7})
\end{eqnarray}
which can be rearranged in a different way
\begin{eqnarray}
  H(z)&= &(h[0]+h[2]z^{-2}+h[4]z^{-4}+h[8]z^{-8}){}+
  \nonumber\\
  & & {}+z^{-1}(h[1]+h[3]z^{-2}+h[5]z^{-4}+h[7]z^{-6})
\end{eqnarray}

And if we use,
\begin{eqnarray}\label{eq:equation-array}
  E_0(z)=h[0]+h[2]z^{-1}+h[4]z^{-2}+h[6]z^{-3}+h[8]z^{-4}      \\
  E_1(z)=h[1]+h[3]z^{-1}+h[5]z^{-2}+h[7]z^{-3}
\end{eqnarray}
and therefore, we can decompose it in to two branches, 
\begin{equation}
  H(z)=E_0({z^2})+z^{-1}E_1({z^2})
\end{equation}
By grouping the terms differently, we can even decompose H(z) in three branches
as,
\begin{equation}
  H(z)=E_0({z^3})+z^{-1}E_1({z^3})+z^{-2}E_2({z^3})
\end{equation}

Therefore, in general case, an L-branch polyphase decomposition of the transfer
function of order $N$ is of the form
\begin{equation}\label{eq:gen-ppf-decomposition}
  H(z)= \sum_{m=0}^{L-1}z^{-m}E_m(z^L)
\end{equation}
where 
\begin{equation}
  E_m(z) = \sum_{n=0}^{[\frac{(N+1)}{L}]}h[Ln+m]z^{-n},\qquad 0 \leq m \leq L-1 
\end{equation}
with $ h[n]=0$ for $n>N$.

Realization based on this kind of decomposition is called a polyphase
realization. The subfilters $E_m(z^{L})$ in the polyphase realization of an FIR
transfer function are also FIR filters. 


\subsection{Design of uniform filter banks with equal passband widths}
\label{passband_widths}


\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{figures/filter-banks.eps}
  \caption{The bank of M filters $H_k(z)$ with uniformly shifted frequency responses.}
  \label{fig:filter-bank}
\end{figure}


Let $ H_0(z)$ represent a causal low-pass digital (FIR) filter with an impulse
response $ h_0[n]$:

\begin{equation}
 H_0(z)= \sum_{n=0}^{\infty}h_0[n]z^{-n}
\end{equation}
Let us now assume that $ H_0(z)$ has its passband edge $\omega_{P}$ and stopband
edge $\omega_S$  around $\frac{\pi}{M}$, where $M$ is some arbitrary integer, as
indicated in Fig.~\ref{fig:filter-bank}.  Now, consider the transfer function
$H_k(z)$, whose impulse response $h_k[n]$ is defined to be 

\begin{equation}\label{eq:impulse-response}
 h_k[n]= h_0[n]W_M^{-kn},\qquad 0 \leq k \leq M-1 ;
\end{equation}
where $ W_M = e^{\frac {-j2\pi}{M}}$, as defined in Eq.~\ref{eq:DFT-equation},
will be   
\begin{equation}
 H_k(z)= \sum_{n=0}^{\infty}h_k[n]z^{-n} = \sum_{n=0}^{\infty}h_0[n](zW_{M}^{k})^{-n},\qquad 0 \leq k \leq M-1
\end{equation}
i.e., 
\begin{equation}
  H_k(z) = H_0(zW_{M}^{k}),\qquad 0 \leq k \leq M-1 
\end{equation}
with a corresponding frequency response

\begin{equation}\label{eq:frequency-response}
H_k(e^{j\omega}) = H_0(zW_{M}^{k}),\qquad 0 \leq k \leq M-1.
\end{equation}
i.e., the frequency response of $ H_k(z)$ is obtained by shifting the response of
$ H_0(z)$ to the right,  by an amount $ \frac{2\pi k}{M}$. The responses of
$H_1(z), H_2(z), ......, H_{M-1}(z)$ are shown in  Fig.~\ref{fig:filter-bank} are
uniformly shifted versions of the response of the basic prototype filter
$H_0(z)$.

\subsection{Polyphase Implementation of uniform filter bank}
\label{polyphase_implementation}

Fig.~\ref{fig:analysis-filter-banks} represent a uniform filter bank with the $M$
analysis filters $ H_k(z)$ related through Eq.~\ref{eq:frequency-response}. The
impulse response sequences $ h_k[n] $ of the analysis filters are accordingly
related as Eq.~\ref{eq:impulse-response}. Therefore, instead of realizing each
analysis filter as a separate filter, it is possible to develop a computationally
more efficient realization of the above uniform filter bank. Let the low-pass
prototype transfer function $H_0(z)$ be represented in its $M$-band polyphase
form:
\begin{equation}\label{eq:polyphase-form}
  H_0(z)=\sum_{l=0}^{M-1} z^{-l}E_l(z^M),
\end{equation}
where $E_l(z)$ is the \emph l th polyphase component of $H_0(z)$:
\begin{equation}
  E_l(z)=\sum_{n=0}^{\infty}e_l[n]z^{-n},\qquad 0\leq l\leq M-1.
\end{equation}
Substituting \emph z with $zW_M^k$ in Eq.~\ref{eq:polyphase-form}, we arrive at
the M-band polyphase decomposition of $H_k(z)$:
\begin{equation}\label{eq:polyphase-decomposition}
  H_k(z)=\sum_{l=0}^{M-1}z^{-l}W_M^{-kl}E_l(z^MW_M^{kM})=\sum_{l=0}^{M-1}z^{-l}W_M^{-kl}E_l(z^M),\quad k=0,1,.....,M-1,
\end{equation}
writting, $W_k^{kM}=1$. Above equation can be written in matrix form as
\begin{equation}
  H_k(z)=\left[\begin{array}{ccccc}
      1 & W_M^{-k} & W_M^{-2k} & \ldots & W_M^{-(M-1)k}
    \end{array}\right]
  \left[\begin{array}{c}
      E_0(z^M)\\
      z^{-1}E_1(z^M)\\
      z^{-2}E_2(z^M)\\
      \vdots \\
      z^{-(M-1)}E_{M-1}(z^M)
    \end{array}\right]
\end{equation}
for $k=0,1,....M-1$. All there $M$ equations can be combined into a matrix
equation as
\begin{equation}
  \left[\begin{array}{c}
      H_0(z)\\
      H_1(z)\\
      H_2(z)\\
      \vdots\\
      H_{M-1}(z)
    \end{array}\right]
  = \left[\begin{array}{ccccc}
      1 & 1 & 1 & \ldots & 1 \\
      1 & W_M^{-1} & W_M^{-2} & \ldots & W_M^{-(M-1)}\\
      1 & W_M^{-2} & W_M^{-4} & \ldots & W_M^{-2(M-1)}\\
      \vdots & \vdots & \vdots & \vdots & \vdots \\
      1 & W_M^{-(M-1)} & W_M^{-2(M-1)} & \ldots & W_M^{-(M-1)^2}
    \end{array}\right]
  \left[\begin{array}{c}
      E_0(z^M)\\
      z^{-1}E_1(z^M)\\
      z^{-2}E_2(z^M)\\
      \vdots\\
      z^{-(M-1)}E_{M-1}(z^M)
    \end{array}\right]
\end{equation}
which is equivalent to
\begin{equation}\label{eq:analysis-filter-bank}
  \left[\begin{array}{c}
      H_0(z)\\
      H_1(z)\\
      H_2(z)\\
      \vdots\\
      H_{M-1}(z)
    \end{array}\right]
  =M \mathbf D^{-1}
  \left[\begin{array}{c}
      E_0(z^M)\\
      z^{-1}E_1(z^M)\\
      z^{-2}E_2(z^M)\\
      \vdots\\
      z^{-(M-1)}E_{M-1}(z^M)
    \end{array}\right]
\end{equation}
where $\mathbf D$ denotes the DFT matrix given by Eq.~\ref{eq:DFT-matrix} and 
$\mathbf D^{-1}=\frac{1}{M}D_M^*$ (* signifies the complex conjugates).

An efficient implementation of the M-band analysis filter bank based on 
Eq.~\ref{eq:analysis-filter-bank}, is thus shown in 
Fig.~\ref{fig:analysis-filter-banks}, where the prototype lowpass filter $H_0(z)$
has been implemented in a polyphase form. For example, an
$M$-band uniform DFT analysis filter bank  based on an $N$-tap prototype lowpass
FIR filter requires, a total of $\frac{M}{2}log_2M+N $ multipliers, whereas a
direct implementation requires $NM$ multiplications.


\begin{thebibliography}{100}
\frenchspacing
\bibitem{Crochiere} R. E. Crochiere, Lawrence R. Rabiner,\emph{Multirate  Digital Signal Processing}, 
         Prentice-Hall Inc., 1983. 
\bibitem{memo}Jan Stemerdink, \emph{Cascaded polyphase filter banks},
  \textsc{lofar-astron-mem-??}
\bibitem{Mitra} S. K. Mitra, \emph{Digital Signal Processing}, McGraw-Hill,
  2001.
\bibitem{Projectplan} Project Plan LOFAR/CR.
\bibitem{moon} T.K. Moon, W. C. Stirling, \emph{Mathematical Methods and
  Algorithms} Prentice-Hall Inc., 2000.
\end{thebibliography}

\end{document}
